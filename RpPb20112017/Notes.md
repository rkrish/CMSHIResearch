## November 20, 2017
--

The purpose of this script is to produce RpPb and compare with simulations, since the latest results are kind of ok as compared to something that was produced long back with a drooping one at high pT for EPPS16, as compared to EPS09


The required files for producing the RpPb macro is `RpPb.C`, it needs the inputs from the processed from two files, bound and free collision type and free and free collision type. The macro writes an output file, note that the file would be rewritten in case the file is already there. 

