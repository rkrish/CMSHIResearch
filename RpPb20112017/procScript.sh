#!/bin/bash

if [[ 1 == 0 ]]
then

genResult(){

	pTHat=$1
	folder=$2
	fileName=`ls ${folder}/*${pTHat}*.root`
	csValue=`cat ${folder}/Cross-Section.txt | grep ${pTHat} | awk '{print $3}'`
	echo $csValue, $fileName

	pTLimits=(`echo $1 | sed -n 's/\(.*\)To\(.*\)/\1 \2/p'`)
	echo "pTLimits: " ${pTLimits[*]}
	lowPt=${pTLimits[0]}; highPt=${pTLimits[1]}
	#root -b -q "myMacro.C+(\"$fileName\", \"${csValue}\", ${lowPt}, ${highPt})"
}

