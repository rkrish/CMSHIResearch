// C++ Headers
#include <iostream>
#include <string>
#include <map>
#include <thread>

// ROOT Headers
#include <TH1D.h>
#include <TFile.h>
#include <TAxis.h>
#include <TPad.h>
#include <TColor.h>

// Macro Load
//#include "./DrawForWithAndWithoutCuts_v6.C+"
//#include "CommonObjects_v1.C"

using namespace std;

//CommonObjects_v1 cc;
typedef	const char* cchar;

typedef	map<string, map<string, TH1D*>> map2D;
typedef	map2D::iterator map2D_it;

typedef	map<string, map2D> map3D;
typedef	map3D::iterator map3D_it;


/* Rename the map3D Initialized Histograms*/
void renameHist(map3D hist)
{

	for (map3D_it it1 = hist.begin(); it1!= hist.end(); ++it1){
		for (map2D_it it2 = (it1->second).begin(); it2 != (it1->second).end(); ++it2 )
			for (map<string, TH1D*>::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3 ){

				cchar hName = Form("%s%s%s", (it1->first).c_str(), (it2->first).c_str(), (it3->first).c_str());
				(it3->second)->SetName(hName);
				cout << (it3->second)->GetName() << endl;

			}
	}

}//close-renameHist

/* Clear the histograms for a specific range  */
void clearHistRange(TH1D *h1, double pTMin, double pTMax)
{

	double lbinNoMin, lbinNoMax;
	cout << "\n\n Working on the histogram from the function clearHistRange: " << h1->GetName() << endl;
	cout << "Clearing up the bins from (pTMin, pTMax): " << pTMin << ", " << pTMax << endl;

	for (int tBin=0; tBin < h1->GetSize(); tBin++){
		lbinNoMin = h1->GetXaxis()->FindBin(pTMin);
		lbinNoMax = h1->GetXaxis()->FindBin(pTMax);

		if ( (tBin > lbinNoMin) && (tBin < lbinNoMax) ) {h1->SetBinContent(tBin, 0); h1->SetBinError(tBin, 0);}
		//if ( (tBin > lbinNoMin) && (tBin < lbinNoMax) ) cout << tBin << " ";
	}//close-for
		cout << endl;

}//close-clearHistRange

/* Apply Cosmetics to Histograms  */
TH1D* applyCosmetics(TH1D* h1, Color_t mColor, Style_t mStyle, double mSize)
{

	h1->SetMarkerColor(mColor);
	h1->SetMarkerStyle(mStyle);
	h1->SetMarkerSize(mSize);
	h1->SetStats(0);

	return h1;
}//close-applyCosmetics

/* Process Triggered Histograms  */
map3D processTrigHist( TFile *file, map3D hTrig, double nEvents, vector<cchar> triggerLabelsList )
{

	cout << "File opened: " << file->GetName() << (file->IsOpen() ? " yes" : " no")  << endl;
//		cout << "Scaling by the number of events: " << nEvents << endl;
		cout << endl;

	cchar sPath;
	for (cchar triggerLabels: triggerLabelsList){
	for (cchar charge : {"Pos", "Neg"}){

			// Charged Tracks
			sPath = Form("userAnalyzer/WithoutCuts/hWithoutCuts%s%s", charge, triggerLabels);
			hTrig[charge]["WithoutCuts"][triggerLabels] = (TH1D*)file->Get(sPath);
			hTrig[charge]["WithoutCuts"][triggerLabels]->Scale(1/nEvents);

			// Leading Tracks
			sPath = Form("userAnalyzer/WithoutCuts/hWithoutCutsleadingpT%s%s",  charge, triggerLabels);
			hTrig[Form("%s-Leading", charge)]["WithoutCuts"][triggerLabels] = (TH1D*)file->Get(sPath);
			hTrig[Form("%s-Leading", charge)]["WithoutCuts"][triggerLabels]->Scale(1/nEvents);

			// Charged Tracks
			sPath = Form("userAnalyzer/WithCuts/h%s%s", charge, triggerLabels);
			hTrig[charge]["WithCuts"][triggerLabels] = (TH1D*)file->Get(sPath);
			hTrig[charge]["WithCuts"][triggerLabels]->Scale(1/nEvents);

			// Leading Tracks
			sPath = Form("userAnalyzer/WithCuts/hleadingpT%s%s", charge, triggerLabels);
			hTrig[Form("%s-Leading", charge)]["WithCuts"][triggerLabels] = (TH1D*)file->Get(sPath);
			hTrig[Form("%s-Leading", charge)]["WithCuts"][triggerLabels]->Scale(1/nEvents);

		}//close-for: Loop over triggers
	}//close-for: Loop over charges


	return hTrig;

}//close-processHist

map3D processMinBiasHist( TFile *file, map3D hMinBias, double nEvents )
{

	cout << "File opened: " << file->GetName() << (file->IsOpen() ? " yes" : " no")  << endl;
//		cout << "Scaling by the number of events: " << nEvents << endl;
		cout << endl;

	cchar sPath;
	for (cchar charge : {"Pos", "Neg"}){

			// Charged Tracks
			sPath = Form("userAnalyzer/WithoutCuts/hWithoutCuts%spT",  charge);
			hMinBias[charge]["WithoutCuts"]["NoTriggers"] = (TH1D*)file->Get(sPath);;
			hMinBias[charge]["WithoutCuts"]["NoTriggers"]->Scale(1/nEvents);

			// Leading Tracks
			sPath = Form("userAnalyzer/WithoutCuts/hWithoutCuts%sleadingpT", charge);
			hMinBias[Form("%s-Leading", charge)]["WithoutCuts"]["NoTriggers"] = (TH1D*)file->Get(sPath);
			hMinBias[Form("%s-Leading", charge)]["WithoutCuts"]["NoTriggers"]->Scale(1/nEvents);


			// Charged Tracks
			sPath = Form("userAnalyzer/WithCuts/h%spT", charge);
			hMinBias[charge]["WithCuts"]["NoTriggers"] = (TH1D*)file->Get(sPath);
			hMinBias[charge]["WithCuts"]["NoTriggers"]->Scale(1/nEvents);
			// Leading Tracks
			sPath = Form("userAnalyzer/WithCuts/h%sleadingpT", charge);
			hMinBias[Form("%s-Leading", charge)]["WithCuts"]["NoTriggers"] = (TH1D*)file->Get(sPath);
			hMinBias[Form("%s-Leading", charge)]["WithCuts"]["NoTriggers"]->Scale(1/nEvents);

	}//close-for: Loop over charges
	//h1->Scale(1/nEvents);


	return hMinBias;

}//close-processHist

/* Show up what labels were initialized in the 3D Map*/
void map3DLabels(map3D m3D)
{
	for (map3D_it it1 = m3D.begin(); it1!= m3D.end(); ++it1){
		for (map2D_it it2 = (it1->second).begin(); it2 != (it1->second).end(); ++it2 )
		for (map<string, TH1D*>::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3 ){

			cout << "[" << it1->first << "]"
			     << "[" << it2->first << "]"
			     << "[" << it3->first << "]\n";
			}
		cout << "\n\n\n";
	}

}//close-map3DLabels


TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles
		lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		/*  
		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";
		*/

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << " Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << " File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist


TH1D* applyVariableBinning(TH1D *hVarBin)
{

	double varBinArr[] = { 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,
	        0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95,
	        1.1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 3.2, 4.0, 4.8, 5.6, 6.4,
	        7.2, 9.6, 12.0, 14.4, 19.2, 24.0, 28.8, 35.2, 41.6, 48.0, 60.8, 73.6, 86.4, 103.6,
	        120.0, 140, 200 };

	const int nVarBins = sizeof(varBinArr)/sizeof(double)-1;
	cout << "Number of Variable Bins declared: " << nVarBins << endl;

	hVarBin = (TH1D*)hVarBin->Rebin(nVarBins, "hVarBin", varBinArr);
	hVarBin = normalizeVarBinHist(hVarBin, nVarBins);

	return hVarBin;

}//close-applyVariableBinning


/* Draw the Histograms from the input files Pre-Processing */
void drawHist(map3D hTrackTrig, map3D hMinBias_pPb, vector<cchar>triggerLabelsList, cchar charge)
{

	// With and Without Cuts
	TCanvas *c[5];
	TLegend *leg[5];

	cout << "From inside drawHist" << endl;
	int i=0;
			cchar cuts;
			cchar leadingLabel;
			c[0] = new TCanvas("c0", "pPb Canvas", 900, 450);
			leg[0] = new TLegend(0.6, 0.7, 0.9, 0.9);

			leadingLabel = Form("%s-Leading", charge);
			cuts = "WithCuts";
				hMinBias_pPb[charge][cuts]["NoTriggers"]->Draw("same p e1");
				applyCosmetics(hMinBias_pPb[charge][cuts]["NoTriggers"], (kBlack), 20, 1.2);
				leg[0]->AddEntry((hMinBias_pPb[charge][cuts]["NoTriggers"]), Form("%s %s", cuts, charge), "lep");


				hMinBias_pPb[leadingLabel][cuts]["NoTriggers"]->Draw("same p e1");
				applyCosmetics(hMinBias_pPb[leadingLabel][cuts]["NoTriggers"], (kGray), 26, 1.2);
				leg[0]->AddEntry((hMinBias_pPb[leadingLabel][cuts]["NoTriggers"]), Form("Leading %s %s", cuts, charge), "lep");

			cuts = "WithoutCuts";
				hMinBias_pPb[charge][cuts]["NoTriggers"]->Draw("same p e1");
				applyCosmetics(hMinBias_pPb[charge][cuts]["NoTriggers"], (kBlack), 20, 1.2);
				leg[0]->AddEntry((hMinBias_pPb[charge][cuts]["NoTriggers"]), Form("%s %s", cuts, charge), "lep");


				hMinBias_pPb[leadingLabel][cuts]["NoTriggers"]->Draw("same p e1");
				applyCosmetics(hMinBias_pPb[leadingLabel][cuts]["NoTriggers"], (kGray), 26, 1.2);
				leg[0]->AddEntry((hMinBias_pPb[leadingLabel][cuts]["NoTriggers"]), Form("Leading %s %s", cuts, charge), "lep");

			leg[0]->Draw("same p e1");
			gPad->SetLogy();

			i = 1;
			Color_t clr, color[] = {kRed, kBlue, kGreen, kViolet, 32};
			for (cchar trigLabel : triggerLabelsList){
				c[i] = new TCanvas(Form("c%d", i), "pPb Canvas", 900, 450);
				leg[i] = new TLegend(0.6, 0.7, 0.9, 0.9);
				for (cchar cuts : {"WithCuts", "WithoutCuts"}){
			
					c[i]->cd();
					if (cuts == (string)"WithCuts") clr = color[i-1];
					if (cuts == (string)"WithoutCuts") clr = color[i-1]-6;

					hTrackTrig[charge][cuts][trigLabel]->Draw("same p e1");
					applyCosmetics(hTrackTrig[charge][cuts][trigLabel], clr, 20, 1.2);
					leg[i]->AddEntry((hTrackTrig[charge][cuts][trigLabel]), Form("%s %s %s", cuts, charge, trigLabel), "lep");

					hTrackTrig[leadingLabel][cuts][trigLabel]->Draw("same p e1");
					applyCosmetics(hTrackTrig[leadingLabel][cuts][trigLabel], clr+2, 24, 1.2);
					leg[i]->AddEntry((hTrackTrig[leadingLabel][cuts][trigLabel]), Form("Leading %s %s %s", cuts, charge, trigLabel), "lep");


					leg[i]->Draw("same p e1");
					gPad->SetLogy();

				}//close-for : With and WithoutCuts
			i++;
			}//close-for: trigger labels
}//close-drawHist

// Main Program
//void WithAndWithoutCuts_v11(int btnID, CommonObjects_v1 cc){
void WithAndWithoutCuts_v11(CommonObjects_v1 cc){

	// Clears up the histogram ranges
	bool rangeClear = cc.rangeClear;

	int btnID = cc.id;

	//cout << "Run the code by giving : WithAndWithoutCuts_v11(Option) " << endl;
	map3D hTrackTrig, hMinBias_pp, hMinBias_pPb, hRatio;
	map<cchar, TFile*> file;
	vector<cchar> triggerLabelsList;

	file["pPbHighPt"] = new TFile("output_pPbHighPt_0407_01_2016-07-04-114955.root", "r");

	file["pPbUPCMinBias"] = new TFile("output_pPbUPCMinBias_0407_01_2016-07-04-115311.root", "r");

	file["ppFullTrack"] = new TFile("output_FullTrack_22_06_2016-06-22-144545.root", "r");

	file["ppMinBias"] = new TFile("output_MinimumBiasAll_2206_01_2016-06-22-144611.root", "r");

	file["ppHighPtJet80"] = new TFile("output_HighPtJet80_2206_2016-06-27-113518.root", "r");


	for (map<const char*, TFile*>::iterator it = file.begin(); it != file.end(); ++it) {

		cout << "Is the file open? " << "\t"
	     		<< file[it->first]->GetName() << "\t"
		        << ( file[it->first]->IsOpen() ? "yes" : "no" )
	     		<< endl;
	}//close-loop over files for pp & pPb



        triggerLabelsList = {"Track12", "Track20", "Track30"};
	hTrackTrig = processTrigHist(file["pPbHighPt"], hTrackTrig, 222789339, triggerLabelsList);

	triggerLabelsList = {"Track18", "Track24", "Track34", "Track45", "Track53"};
	hTrackTrig = processTrigHist(file["ppFullTrack"], hTrackTrig, 4459855, triggerLabelsList);

 
	hMinBias_pp = processMinBiasHist(file["ppMinBias"], hMinBias_pp, 2536648731);
	hMinBias_pPb = processMinBiasHist(file["pPbUPCMinBias"], hMinBias_pPb, 286814246);

	
	// pPb Histograms Drawing
	if (btnID == tppTrig){
        	triggerLabelsList = {"Track12", "Track20", "Track30"};
		drawHist(hTrackTrig, hMinBias_pPb, triggerLabelsList, "Pos");
	}

	// pp Histograms Drawing
	if (btnID == tpPbTrig){
		triggerLabelsList = {"Track18", "Track24", "Track34", "Track45", "Track53"};
		drawHist(hTrackTrig, hMinBias_pp, triggerLabelsList, "Pos");
	}

	/*  
	hMinBias_pp["Pos"]["WithCuts"]["NoTriggers"]->SetMarkerColor(kRed);
	hMinBias_pp["Pos"]["WithCuts"]["NoTriggers"]->Draw("same");
	hMinBias_pPb["Pos"]["WithCuts"]["NoTriggers"]->SetMarkerColor(kGreen);
	hMinBias_pPb["Pos"]["WithCuts"]["NoTriggers"]->Draw("same");
	*/

	// Spectra Combination 
	map<cchar, double> NValTrack12, NValTrack20, NValTrack30, NValTrack50,

	       NTrack12_14To22, NValMB_14To22, NValMB_0To200,
	       NTrack12_22To32, NTrack20_22To32, 
	       NTrack20_32To200, NTrack30_32To200;
	
	double binNoMax[5], binNoMin[5];

	/*  Get the Normalization factors from leading pT for TrackTriggered (12, 20, 30) */
	for ( cchar charge : {"Pos", "Neg"} ){

	cchar label = Form("%s-Leading", charge);
	cout  << "Calculating Normalization Factors from Leading pT Spectra for the charge : " 
	      << charge 
	      << "with the label hTrackTrig : " << label
	      << endl;

	NValMB_0To200[charge] = hMinBias_pPb[label]["WithCuts"]["NoTriggers"]->Integral();
	       
	// Normalization for Track12
	binNoMin[0] = hTrackTrig[label]["WithCuts"]["Track12"]->FindBin(14);
	binNoMax[0] = hTrackTrig[label]["WithCuts"]["Track12"]->FindBin(22);
	NTrack12_14To22[charge] = hTrackTrig[label]["WithCuts"]["Track12"]->Integral(binNoMin[0], binNoMax[0]);
	NValMB_14To22[charge] = hMinBias_pPb[label]["WithCuts"]["NoTriggers"]->Integral(binNoMin[0], binNoMax[0]);


	// Normalization for Track20
	binNoMin[1] = hTrackTrig[label]["WithCuts"]["Track20"]->FindBin(22);
	binNoMax[1] = hTrackTrig[label]["WithCuts"]["Track20"]->FindBin(32);

	NTrack20_22To32[charge] = hTrackTrig[label]["WithCuts"]["Track20"]->Integral(binNoMin[1], binNoMax[1]);


	binNoMin[1] = hTrackTrig[label]["WithCuts"]["Track12"]->FindBin(22);
	binNoMax[1] = hTrackTrig[label]["WithCuts"]["Track12"]->FindBin(32);
	NTrack12_22To32[charge] = hTrackTrig[label]["WithCuts"]["Track12"]->Integral(binNoMin[1], binNoMax[1]);


	// Normalization for Track30
	binNoMin[2] = hTrackTrig[label]["WithCuts"]["Track30"]->FindBin(32);
	binNoMax[2] = hTrackTrig[label]["WithCuts"]["Track30"]->FindBin(200);
	NTrack30_32To200[charge] = hTrackTrig[label]["WithCuts"]["Track30"]->Integral(binNoMin[2], binNoMax[2]);


	binNoMin[2] = hTrackTrig[label]["WithCuts"]["Track20"]->FindBin(32);
	binNoMax[2] = hTrackTrig[label]["WithCuts"]["Track20"]->FindBin(200);
	NTrack20_32To200[charge] = hTrackTrig[label]["WithCuts"]["Track20"]->Integral(binNoMin[2], binNoMax[2]);


	NValTrack12[charge] = NTrack12_14To22[charge]/NValMB_14To22[charge];
	NValTrack20[charge] = (NValTrack12[charge]*NTrack20_22To32[charge])/NTrack12_22To32[charge];
	NValTrack30[charge] = (NValTrack20[charge]*NTrack30_32To200[charge])/NTrack20_32To200[charge];

	cout  << "\n"
	      << "NValMB_0To200: " << NValMB_0To200[charge] << ", " 
	      << "NValTrack12: " << NValTrack12[charge] << ", " 
	      << "NValTrack20[charge]: " << NValTrack20[charge] << ", " 
	      << "NValTrack30: " << NValTrack30[charge] 
	      << endl ;
	     

	// Clone the Track Triggered Spectra
	triggerLabelsList = {"Track12", "Track20", "Track30"};

		string labelLeadingNorm = Form("%s-Leading-Normalized", charge);
		string labelNorm = Form("%s-Normalized", charge);

		for ( cchar trigLabel : triggerLabelsList ) {

			hTrackTrig[labelLeadingNorm]["WithCuts"][trigLabel] = (TH1D*)hTrackTrig[Form("%s-Leading", charge)]["WithCuts"][trigLabel]->Clone();
			hTrackTrig[labelNorm]["WithCuts"][trigLabel] = (TH1D*)hTrackTrig[charge]["WithCuts"][trigLabel]->Clone();

		}//close-for trigger labels


			// Scaling down leading track pT
			hTrackTrig[labelLeadingNorm]["WithCuts"]["Track12"]->Scale(1/NValTrack12[charge]);
			hTrackTrig[labelLeadingNorm]["WithCuts"]["Track20"]->Scale(1/NValTrack20[charge]);
			hTrackTrig[labelLeadingNorm]["WithCuts"]["Track30"]->Scale(1/NValTrack30[charge]);

			// Scaling down charged track pT
			hTrackTrig[labelNorm]["WithCuts"]["Track12"]->Scale(1/NValTrack12[charge]);
			hTrackTrig[labelNorm]["WithCuts"]["Track20"]->Scale(1/NValTrack20[charge]);
			hTrackTrig[labelNorm]["WithCuts"]["Track30"]->Scale(1/NValTrack30[charge]);

		// Min Bias 
		if (rangeClear){

			clearHistRange(hMinBias_pPb["Pos"]["WithCuts"]["NoTriggers"], 14, 200);
			clearHistRange(hMinBias_pPb["Neg"]["WithCuts"]["NoTriggers"], 14, 200);


			// Clear from the beginning for Track Triggered Spectra
			clearHistRange(hTrackTrig[labelNorm]["WithCuts"]["Track12"], 0, 14);
			clearHistRange(hTrackTrig[labelNorm]["WithCuts"]["Track20"], 0, 21);
			clearHistRange(hTrackTrig[labelNorm]["WithCuts"]["Track30"], 0, 31);

			// Clear beyond the Trigger range
			clearHistRange(hTrackTrig[labelNorm]["WithCuts"]["Track12"], 21, 200);
			clearHistRange(hTrackTrig[labelNorm]["WithCuts"]["Track20"], 31, 200);

		}//close-if

		//string chargeMergeLabel = Form("%s-Merged", charge);
		hTrackTrig[charge]["WithCuts"]["pPbMerged"] = (TH1D*)hMinBias_pPb["Pos"]["WithCuts"]["NoTriggers"]->Clone();
		hTrackTrig[charge]["WithCuts"]["pPbMerged"]->Add(hTrackTrig[labelNorm]["WithCuts"]["Track12"]);
		hTrackTrig[charge]["WithCuts"]["pPbMerged"]->Add(hTrackTrig[labelNorm]["WithCuts"]["Track20"]);
		hTrackTrig[charge]["WithCuts"]["pPbMerged"]->Add(hTrackTrig[labelNorm]["WithCuts"]["Track30"]);

	}//close-for: Loop over charges


	// Spectra Combination 
	map<cchar, double> NValTrack18, NValTrack24, NValTrack34, NValTrack45, NValTrack53,


	       // For Track18
	       NTrack18_20To26, NValMB_20To26, /*NValMB_0To200,*/

	       // For Track24
	       NTrack18_26To36, NTrack24_26To36, 

	       // For Track34
	       NTrack24_36To47, NTrack34_36To47, 

	       // For Track45
	       NTrack34_47To55, NTrack45_47To55,

	       // For Track53
	       NTrack45_55To200, NTrack53_55To200
	       ;

	for ( cchar charge : {"Pos", "Neg"}){

	cchar label = Form("%s-Leading", charge);
	cout  << "Calculating Normalization Factors from Leading pT Spectra for the charge : " 
	      << charge 
	      << "with the label hTrackTrig : " << label
	      << endl;


	// Normalization for Track18
	binNoMin[0] = hTrackTrig[label]["WithCuts"]["Track18"]->FindBin(20);
	binNoMax[0] = hTrackTrig[label]["WithCuts"]["Track18"]->FindBin(26);
	NTrack18_20To26[charge] = hTrackTrig[label]["WithCuts"]["Track18"]->Integral(binNoMin[0], binNoMax[0]);
	NValMB_20To26[charge] = hMinBias_pp[label]["WithCuts"]["NoTriggers"]->Integral(binNoMin[0], binNoMax[0]);


	// Normalization for Track24
	binNoMin[1] = hTrackTrig[label]["WithCuts"]["Track24"]->FindBin(26);
	binNoMax[1] = hTrackTrig[label]["WithCuts"]["Track24"]->FindBin(36);
	NTrack24_26To36[charge] = hTrackTrig[label]["WithCuts"]["Track24"]->Integral(binNoMin[1], binNoMax[1]);

	binNoMin[1] = hTrackTrig[label]["WithCuts"]["Track18"]->FindBin(26);
	binNoMax[1] = hTrackTrig[label]["WithCuts"]["Track18"]->FindBin(36);
	NTrack18_26To36[charge] = hTrackTrig[label]["WithCuts"]["Track18"]->Integral(binNoMin[1], binNoMax[1]);


	// Normalization for Track34
	binNoMin[2] = hTrackTrig[label]["WithCuts"]["Track34"]->FindBin(36);
	binNoMax[2] = hTrackTrig[label]["WithCuts"]["Track34"]->FindBin(47);
	NTrack34_36To47[charge] = hTrackTrig[label]["WithCuts"]["Track34"]->Integral(binNoMin[2], binNoMax[2]);

	binNoMin[2] = hTrackTrig[label]["WithCuts"]["Track24"]->FindBin(36);
	binNoMax[2] = hTrackTrig[label]["WithCuts"]["Track24"]->FindBin(47);
	NTrack24_36To47[charge] = hTrackTrig[label]["WithCuts"]["Track24"]->Integral(binNoMin[2], binNoMax[2]);


	// Normalization for Track45
	binNoMin[3] = hTrackTrig[label]["WithCuts"]["Track45"]->FindBin(47);
	binNoMax[3] = hTrackTrig[label]["WithCuts"]["Track45"]->FindBin(55);
	NTrack45_47To55[charge] = hTrackTrig[label]["WithCuts"]["Track45"]->Integral(binNoMin[3], binNoMax[3]);

	binNoMin[3] = hTrackTrig[label]["WithCuts"]["Track34"]->FindBin(47);
	binNoMax[3] = hTrackTrig[label]["WithCuts"]["Track34"]->FindBin(55);
	NTrack34_47To55[charge] = hTrackTrig[label]["WithCuts"]["Track34"]->Integral(binNoMin[3], binNoMax[3]);


	// Normalization for Track53
	binNoMin[4] = hTrackTrig[label]["WithCuts"]["Track53"]->FindBin(55);
	binNoMax[4] = hTrackTrig[label]["WithCuts"]["Track53"]->FindBin(200);
	NTrack53_55To200[charge] = hTrackTrig[label]["WithCuts"]["Track53"]->Integral(binNoMin[4], binNoMax[4]);

	binNoMin[4] = hTrackTrig[label]["WithCuts"]["Track45"]->FindBin(55);
	binNoMax[4] = hTrackTrig[label]["WithCuts"]["Track45"]->FindBin(200);
	NTrack45_55To200[charge] = hTrackTrig[label]["WithCuts"]["Track45"]->Integral(binNoMin[4], binNoMax[4]);


	NValTrack18[charge] = NTrack18_20To26[charge]/NValMB_20To26[charge];
	NValTrack24[charge] = (NValTrack18[charge]*NTrack24_26To36[charge])/NTrack18_26To36[charge];
	NValTrack34[charge] = (NValTrack24[charge]*NTrack34_36To47[charge])/NTrack24_36To47[charge];
	NValTrack45[charge] = (NValTrack34[charge]*NTrack45_47To55[charge])/NTrack34_47To55[charge];
	NValTrack53[charge] = (NValTrack45[charge]*NTrack53_55To200[charge])/NTrack45_55To200[charge];


	cout  << "\n" << charge << "\t"
	      << "NValMB_0To200: " << NValMB_0To200[charge] << ", " 
	      << "NValTrack18: " << NValTrack18[charge] << ", " 
	      << "NValTrack24: " << NValTrack24[charge] << ", " 
	      << "NValTrack34: " << NValTrack34[charge] << ", "
	      << "NValTrack45: " << NValTrack45[charge] << ", "
	      << "NValTrack53: " << NValTrack53[charge] << ", "
	      << endl ;

	// Clone the Track Triggered Spectra
	triggerLabelsList = {"Track18", "Track24", "Track34", "Track45", "Track53"};

	string labelLeadingNorm = Form("%s-Leading-Normalized", charge);
	string labelNorm = Form("%s-Normalized", charge);


	for ( cchar trigLabel : triggerLabelsList ) {

		hTrackTrig[labelLeadingNorm]["WithCuts"][trigLabel] = (TH1D*)hTrackTrig[Form("%s-Leading", charge)]["WithCuts"][trigLabel]->Clone();
		hTrackTrig[labelNorm]["WithCuts"][trigLabel] = (TH1D*)hTrackTrig[charge]["WithCuts"][trigLabel]->Clone();


	}//close-for trigger labels
	    
		// Scaling down leading track pT
		hTrackTrig[labelLeadingNorm]["WithCuts"]["Track18"]->Scale(1/NValTrack18[charge]);
		hTrackTrig[labelLeadingNorm]["WithCuts"]["Track24"]->Scale(1/NValTrack24[charge]);
		hTrackTrig[labelLeadingNorm]["WithCuts"]["Track34"]->Scale(1/NValTrack34[charge]);
		hTrackTrig[labelLeadingNorm]["WithCuts"]["Track45"]->Scale(1/NValTrack45[charge]);
		hTrackTrig[labelLeadingNorm]["WithCuts"]["Track53"]->Scale(1/NValTrack53[charge]);

		// Scaling down charged track pT
		hTrackTrig[labelNorm]["WithCuts"]["Track18"]->Scale(1/NValTrack18[charge]);
		hTrackTrig[labelNorm]["WithCuts"]["Track24"]->Scale(1/NValTrack24[charge]);
		hTrackTrig[labelNorm]["WithCuts"]["Track34"]->Scale(1/NValTrack34[charge]);
		hTrackTrig[labelNorm]["WithCuts"]["Track45"]->Scale(1/NValTrack45[charge]);
		hTrackTrig[labelNorm]["WithCuts"]["Track53"]->Scale(1/NValTrack53[charge]);


		string chargeNormLabel = Form("%s-Normalized", charge);

		if (rangeClear){
			// Min Bias 
			clearHistRange(hMinBias_pp["Pos"]["WithCuts"]["NoTriggers"], 20, 200);
			clearHistRange(hMinBias_pp["Neg"]["WithCuts"]["NoTriggers"], 20, 200);


			// Clear from the beginning for Track Triggered Spectra
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track18"], 0, 20);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track24"], 0, 26);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track34"], 0, 36);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track45"], 0, 47);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track53"], 0, 55);

			// Clear beyond the Trigger range
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track18"], 26, 200);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track24"], 36, 200);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track34"], 47, 200);
			clearHistRange(hTrackTrig[chargeNormLabel]["WithCuts"]["Track45"], 55, 200);

		}//close-if : Clear Histograms

		hTrackTrig[charge]["WithCuts"]["ppMerged"] = (TH1D*)hMinBias_pp[charge]["WithCuts"]["NoTriggers"]->Clone();

        	triggerLabelsList = {"Track18", "Track24", "Track34", "Track45", "Track53"};
		for (cchar trigLabel : triggerLabelsList){
			hTrackTrig[charge]["WithCuts"]["ppMerged"]->Add(hTrackTrig[chargeNormLabel]["WithCuts"][trigLabel]);
		}//close-for: Loop over Trigger Labels List

	}//close-for : Charge Loop for pp Normalization


	// Apply Variable Binning for Combined Track Triggered Spectrum
	for (cchar label : {"ppMerged", "pPbMerged"}){

		hTrackTrig["Neg"]["WithCuts"][label] = applyVariableBinning(hTrackTrig["Neg"]["WithCuts"][label]);
		hTrackTrig["Pos"]["WithCuts"][label] = applyVariableBinning(hTrackTrig["Pos"]["WithCuts"][label]);

		hRatio["Neg/Pos"]["WithCuts"][label] = (TH1D*)hTrackTrig["Neg"]["WithCuts"][label]->Clone();
		hRatio["Neg/Pos"]["WithCuts"][label]->Divide(hTrackTrig["Pos"]["WithCuts"][label]);


	}//close-for: Loop over pp, pPb Merged


	/* Draw {pp, pPb}Merged for Posive, Negative separately */
	if(btnID == tMergedppAndpPb)
	{

		TCanvas *c[2];
		Color_t color[] = {kRed-4, kBlue-4}; int i=0, j=0;
		for (cchar charge : {"Pos", "Neg"}){
			TLegend *leg = new TLegend(0.8, 0.8, 0.5, 0.9);
			c[j] = new TCanvas(Form("%d", j), "", 900, 450);
			c[j]->cd();
			for (cchar label : {"ppMerged", "pPbMerged"}){

				hTrackTrig[charge]["WithCuts"][label]->Draw("same p e1");
				applyCosmetics(hTrackTrig[charge]["WithCuts"][label], color[i], 20, 1.5);
				leg->AddEntry(hTrackTrig[charge]["WithCuts"][label], Form("%s %s-Trigger Combined", label, charge), "lep");

				leg->Draw("same");
				i++;
			}//close-for:Loop over pp,pPb
			gPad->SetLogy();
			j++; i=0;
		}//close-for:Loop over charged

	}//close-if

	// pp Trigger-Normalized: MB, TrackTriggered Plots
	if(btnID == tSpectra_pp)
	{
		//cchar charge ="Pos", histLabel = "Pos-Normalized";
		
		Style_t mstyle;
		TCanvas *c[2]; int j=0;
		for (cchar charge : {"Pos", "Neg"}){

			c[j] = new TCanvas(Form("%d", j), "", 900, 460);
			c[j]->cd();

			if (charge == (string)"Pos") mstyle = 20;
			if (charge == (string)"Neg") mstyle = 24;

			cchar histLabel = Form("%s-Normalized", charge);
		
			Color_t color[] = {kRed, kBlue, kGreen, kViolet, 32};
			int colorIt=0;

			TLegend *leg[4];
			leg[0] = new TLegend(0.6, 0.7, 0.9, 0.9);

		hMinBias_pp[charge]["WithCuts"]["NoTriggers"]->Draw("same p e1");
		applyCosmetics(hMinBias_pp[charge]["WithCuts"]["NoTriggers"], (kBlack), mstyle, 1.2);
		leg[0]->AddEntry((hMinBias_pp[charge]["WithCuts"]["NoTriggers"]), Form("WithCuts Min Bias %s", histLabel), "lep");

		triggerLabelsList = {"Track18", "Track24", "Track34", "Track45", "Track53"};
		for (cchar trigLabel : triggerLabelsList){

			hTrackTrig[histLabel]["WithCuts"][trigLabel]->Draw("same p e1");

			leg[0]->AddEntry((hTrackTrig[histLabel]["WithCuts"][trigLabel]), Form("WithCuts  %s %s", trigLabel, histLabel), "lep");
			applyCosmetics((hTrackTrig[histLabel]["WithCuts"][trigLabel]), (color[colorIt]-6), mstyle, 1.3);
			hTrackTrig[histLabel]["WithCuts"][trigLabel]->Draw("same p e1");
			colorIt++;


		}//close-for: Loop over trigger labels

		leg[0]->Draw("same p e1");
		gPad->SetLogy();
		j++;

		}//close-for : Loop over charges

	}//close-if

	// pPb Trigger-Normalized: MB, TrackTriggered Plots
	if(btnID == tSpectra_pPb)
	{
		//cchar charge ="Pos", histLabel = "Pos-Normalized";
		
		Style_t mstyle;
		TCanvas *c[2]; int j=0;
		for (cchar charge : {"Pos", "Neg"}){

			c[j] = new TCanvas(Form("%d", j), "", 900, 460);
			c[j]->cd();

			if (charge == (string)"Pos") mstyle = 20;
			if (charge == (string)"Neg") mstyle = 24;

			cchar histLabel = Form("%s-Normalized", charge);
		
			Color_t color[] = {kRed, kBlue, kGreen, kViolet, 32};
			int colorIt=0;

			TLegend *leg[4];
			leg[0] = new TLegend(0.6, 0.7, 0.9, 0.9);

		hMinBias_pPb[charge]["WithCuts"]["NoTriggers"]->Draw("same p e1");
		applyCosmetics(hMinBias_pPb[charge]["WithCuts"]["NoTriggers"], (kBlack), mstyle, 1.2);
		leg[0]->AddEntry((hMinBias_pPb[charge]["WithCuts"]["NoTriggers"]), Form("WithCuts Min Bias %s", histLabel), "lep");

		triggerLabelsList = {"Track12", "Track20", "Track30"};
		for (cchar trigLabel : triggerLabelsList){

			hTrackTrig[histLabel]["WithCuts"][trigLabel]->Draw("same p e1");

			leg[0]->AddEntry((hTrackTrig[histLabel]["WithCuts"][trigLabel]), Form("WithCuts  %s %s", trigLabel, histLabel), "lep");
			applyCosmetics((hTrackTrig[histLabel]["WithCuts"][trigLabel]), (color[colorIt]-6), mstyle, 1.3);
			hTrackTrig[histLabel]["WithCuts"][trigLabel]->Draw("same p e1");
			colorIt++;


		}//close-for: Loop over trigger labels

		leg[0]->Draw("same p e1");
		gPad->SetLogy();
		j++;

		}//close-for : Loop over charges

	}//close-if

	// Neg/Pos : pPb, pp separately 
	if (btnID == tNegByPos)
	{

		cout << "NegByPos" << endl;
		TLegend *leg = new TLegend(0.2, 0.8, 0.5, 0.9);
		Color_t color[] = {kRed-6, kBlue-6}; int i=0;
		for (cchar label : {"ppMerged", "pPbMerged"}){
			hRatio["Neg/Pos"]["WithCuts"][label]->SetStats(0);
			hRatio["Neg/Pos"]["WithCuts"][label]->Draw("same p e1");
			applyCosmetics(hRatio["Neg/Pos"]["WithCuts"][label], color[i], 20, 1.5);
			leg->AddEntry(hRatio["Neg/Pos"]["WithCuts"][label], Form("Ratio of %s Neg/Pos-Trigger Combined", label), "lep");

			leg->Draw("same");
			i++;
		}

	}//close-if
				
	// Calculate RpPb : Pos, Neg separately
	if(btnID == tRpPb)
	{

		TCanvas *c = new TCanvas("c", "", 900, 460);
		TLegend *leg = new TLegend(0.2, 0.8, 0.5, 0.9);
		Color_t color[] = {kRed-6, kBlue-6}; int i=0;
		for (cchar charge : {"Pos", "Neg"}){

			cchar label = Form("RpPb-%s", charge);
			hRatio[label]["WithCuts"]["Merged"] = (TH1D*)hTrackTrig[charge]["WithCuts"]["pPbMerged"]->Clone();
			hRatio[label]["WithCuts"]["Merged"]->Divide(hTrackTrig[charge]["WithCuts"]["ppMerged"]);

			hRatio[label]["WithCuts"]["Merged"]->Scale(1/6.9);
			hRatio[label]["WithCuts"]["Merged"]->Draw("same e1 p");
			applyCosmetics(hRatio[label]["WithCuts"]["Merged"], color[i++], 20, 1.5);
			leg->AddEntry(hRatio[label]["WithCuts"]["Merged"], label, "lep");
			leg->Draw();
			gPad->SetLogx();
		}
	}//close-if

	map3DLabels(hTrackTrig);
	renameHist(hTrackTrig);

}//close-main
