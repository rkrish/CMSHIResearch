#include <iostream>

using namespace std;

void message(){

	cout << "\n\nThis program helps to keep/remove a chosen set of branches in the ROOT file" 
	     << ", thereby helping reduce the file size for a quicker processing time.\n\n" << endl;

}//close:message

//void keepOrRemoveObjects( TFile *infile, TFile *outfile, vector<string> keepObj, vector<string> removeObj ){
void keepOrRemoveObjects(){

	message();

	TFile *infile = new TFile("xVSpt/BoundAndFreeProton_EPPS16_CT14nlo_170To230_2017-08-30-211258.root");

	//TTree *mtree = (TTree*)infile->GetObject("QCDAna/tree2", mtree);
	TTree *mtree = (TTree*)infile->Get("QCDAna/tree2");
	//TTree *ntree = mtree->CloneTree(0); 

	cout << mtree->GetName();
	//mtree->SetDirectory(0);

	TFile *outfile = new TFile("testDeleteMe.root", "recreate");
	outfile->cd();
	mtree->CloneTree()->Write();
	//ntree->GetBranch("hadronPos")->SetFile("testDeleteMe.root");
	//mtree->Write("blah");
	//outfile->Write();
	outfile->Close();

	infile->Close();

	

}//close:main
