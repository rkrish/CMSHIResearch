#!/bin/bash

someText="Whatsup"
for i in {0..5}
do
	echo "Static text argument"
	root -b -q -l "passArgumentCommandLine.C+($i, \"Whatsup\")"

	echo "Text from bash string"
	root -b -q -l "passArgumentCommandLine.C+($i, \"${someText}\")"
done


<<comment1

Use this macro to pass a string and a 
number to the macro from the commandline for
batch processing. 

comment1
