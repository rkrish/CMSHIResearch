// C++ Headers
#include <iostream>

// ROOT Headers
#include <TROOT.h>
#include <TGraph.h>
#include <TH1D.h>

// ROOT Macros
#include "externalMacro.C"


using namespace std;

void  passArgumentCommandLine(int a, const char *c){

	TGraph *g;
	TH1D *h;
	int error=0;

	h = new TH1D("h", "Gauss distribution", 100, -5, 5);
	h->FillRandom("gauss", 1000);

	// Compile and link the symbols from the
	// external file for use in this program
//	gROOT->ProcessLine(".L externalMacro.C++", &error);
//	gROOT->ProcessLine("externalMacro();");

//	if (error) cout << "Failed" << endl;
//	else {cout << "Macro error status: " << error << endl;}

	cout << "Values passed from commandline: " << a << endl;
	cout << "Text from commandline: " << c << endl;
	externalMacro(h, g);
}

