#!/bin/bash

echo "The shared libraries in this directory"
echo "======================================"
ls *.so

echo "======================================"
ls *.pcm
echo "======================================"
ls *.d

if [ $1 == 1 ]
then 
	echo "You chose to remove the files with the extension"
	rm *.so; rm *.pcm; rm *.d

else 
	echo "Please enter a valid option, 1 for removing files"
	echo "./removeJunk.sh 1"

fi
