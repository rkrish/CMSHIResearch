

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
1.97354	  1.09933e-07	  0.098677	  7.60656e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-01-30-100437
0.000182268	  7.7581e-16	  9.1134e-06	  6.39e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-01-30-100437
3.6665e-05	  2.73178e-17	  1.83325e-06	  1.19907e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-01-30-100437
0.174074	  8.64469e-10	  0.0087037	  6.74525e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-01-30-100437
8.2929e-06	  1.55509e-18	  4.14645e-07	  2.86088e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-01-30-100437
2.033e-06	  7.89384e-20	  1.0165e-07	  6.44566e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-01-30-100437
0.046925	  6.43186e-11	  0.00234625	  1.83989e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-01-30-100437
7.1627e-07	  1.15729e-20	  3.58135e-08	  2.46799e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-01-30-100437
0.0069684	  1.33087e-12	  0.00034842	  2.64662e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-01-30-100437
0.00104855	  3.17492e-14	  5.24275e-05	  4.0878e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-01-30-100437
