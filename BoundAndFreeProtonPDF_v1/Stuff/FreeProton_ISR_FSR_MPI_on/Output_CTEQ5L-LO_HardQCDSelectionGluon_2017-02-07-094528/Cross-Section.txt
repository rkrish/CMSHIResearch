

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.476	  1.26431e-05	  1.1238	  0.000815735	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-07-094528
0.00039967	  3.49431e-15	  1.99835e-05	  1.35614e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-07-094528
5.5023e-05	  6.77794e-17	  2.75115e-06	  1.88874e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-07-094528
1.46092	  4.90694e-08	  0.073046	  5.08193e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-07-094528
8.4625e-06	  1.71833e-18	  4.23125e-07	  3.0073e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-07-094528
1.39483e-06	  5.04782e-20	  6.97415e-08	  5.15436e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-07-094528
0.30733	  2.05599e-09	  0.0153665	  1.04024e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-07-094528
3.1829e-07	  2.93256e-21	  1.59145e-08	  1.24236e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-07-094528
0.032455	  2.28446e-11	  0.00162275	  1.09651e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-07-094528
0.0033645	  2.36901e-13	  0.000168225	  1.11662e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-07-094528
