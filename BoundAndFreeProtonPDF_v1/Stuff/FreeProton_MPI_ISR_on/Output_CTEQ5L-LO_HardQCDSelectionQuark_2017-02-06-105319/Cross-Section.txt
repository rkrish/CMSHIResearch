

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
1.97381	  1.09997e-07	  0.0986905	  7.60876e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-06-105319
0.000182297	  7.75574e-16	  9.11485e-06	  6.38903e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-06-105319
3.6657e-05	  2.68981e-17	  1.83285e-06	  1.18983e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-06-105319
0.174033	  8.63509e-10	  0.00870165	  6.7415e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-06-105319
8.291e-06	  1.39213e-18	  4.1455e-07	  2.70684e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-06-105319
2.0335e-06	  7.9049e-20	  1.01675e-07	  6.45017e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-06-105319
0.04693	  6.43544e-11	  0.0023465	  1.8404e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-06-105319
7.1645e-07	  1.15801e-20	  3.58225e-08	  2.46876e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-06-105319
0.0069704	  1.33288e-12	  0.00034852	  2.64862e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-06-105319
0.0010485	  3.19114e-14	  5.2425e-05	  4.09823e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-06-105319
