

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.303	  1.25949e-05	  1.11515	  0.000814181	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-06-105408
0.00043204	  4.27899e-15	  2.1602e-05	  1.5007e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-06-105408
5.6903e-05	  8.3069e-17	  2.84515e-06	  2.09095e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-06-105408
1.53681	  5.82552e-08	  0.0768405	  5.53721e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-06-105408
8.3433e-06	  1.75641e-18	  4.17165e-07	  3.04044e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-06-105408
1.32284e-06	  4.73044e-20	  6.6142e-08	  4.98969e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-06-105408
0.33265	  2.50209e-09	  0.0166325	  1.14756e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-06-105408
2.6774e-07	  2.14639e-21	  1.3387e-08	  1.06286e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-06-105408
0.035894	  2.74998e-11	  0.0017947	  1.20306e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-06-105408
0.003722	  2.93183e-13	  0.0001861	  1.2422e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-06-105408
