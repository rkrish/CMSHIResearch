

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
2.2048	  1.34349e-07	  0.11024	  8.40892e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-06-163116
0.000177254	  7.31204e-16	  8.8627e-06	  6.20358e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-06-163116
3.59e-05	  2.54951e-17	  1.795e-06	  1.15838e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-06-163116
0.183521	  9.43128e-10	  0.00917605	  7.04545e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-06-163116
8.2576e-06	  1.31093e-18	  4.1288e-07	  2.62671e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-06-163116
2.0741e-06	  7.68578e-20	  1.03705e-07	  6.36015e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-06-163116
0.047954	  6.64338e-11	  0.0023977	  1.8699e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-06-163116
7.5854e-07	  1.28018e-20	  3.7927e-08	  2.59573e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-06-163116
0.0069209	  1.26198e-12	  0.000346045	  2.57721e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-06-163116
0.00102383	  2.79715e-14	  5.11915e-05	  3.8369e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-06-163116
