

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.303	  1.25916e-05	  1.11515	  0.000814073	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-03-125534
0.000432	  4.27986e-15	  2.16e-05	  1.50085e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-03-125534
5.6909e-05	  8.24966e-17	  2.84545e-06	  2.08373e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-03-125534
1.53662	  5.82477e-08	  0.076831	  5.53685e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-03-125534
8.3453e-06	  1.75765e-18	  4.17265e-07	  3.04151e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-03-125534
1.32295e-06	  4.73277e-20	  6.61475e-08	  4.99092e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-03-125534
0.33265	  2.5032e-09	  0.0166325	  1.14781e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-03-125534
2.6769e-07	  2.14742e-21	  1.33845e-08	  1.06312e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-03-125534
0.035894	  2.74928e-11	  0.0017947	  1.20291e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-03-125534
0.003722	  2.93038e-13	  0.0001861	  1.2419e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-03-125534
