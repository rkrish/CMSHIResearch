

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
12.9291	  4.788e-06	  0.646455	  0.000501996	  CTEQ5L-LO_HardQCDMix_10To20_2017-02-03-125507
0.00065866	  9.84349e-15	  3.2933e-05	  2.27613e-08	  CTEQ5L-LO_HardQCDMix_120To170_2017-02-03-125507
0.000109908	  2.54078e-16	  5.4954e-06	  3.65685e-09	  CTEQ5L-LO_HardQCDMix_170To230_2017-02-03-125507
1.04676	  3.26416e-08	  0.052338	  4.14485e-05	  CTEQ5L-LO_HardQCDMix_20To30_2017-02-03-125507
2.03e-05	  8.91594e-18	  1.015e-06	  6.85026e-10	  CTEQ5L-LO_HardQCDMix_230To300_2017-02-03-125507
4.0095e-06	  3.37792e-19	  2.00475e-07	  1.33336e-10	  CTEQ5L-LO_HardQCDMix_300To380_2017-02-03-125507
0.26116	  2.04182e-09	  0.013058	  1.03665e-05	  CTEQ5L-LO_HardQCDMix_30To50_2017-02-03-125507
1.05237e-06	  2.78684e-20	  5.26185e-08	  3.82983e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2017-02-03-125507
0.034448	  3.02949e-11	  0.0017224	  1.26272e-06	  CTEQ5L-LO_HardQCDMix_50To80_2017-02-03-125507
0.0044791	  4.73243e-13	  0.000223955	  1.57821e-07	  CTEQ5L-LO_HardQCDMix_80To120_2017-02-03-125507
