

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
37.446	  3.73355e-05	  1.8723	  0.00140179	  CTEQ5L-LO_HardQCDAll_10To20_2017-02-03-125516
0.00128136	  3.7601e-14	  6.4068e-05	  4.44859e-08	  CTEQ5L-LO_HardQCDAll_120To170_2017-02-03-125516
0.00020475	  9.30052e-16	  1.02375e-05	  6.99643e-09	  CTEQ5L-LO_HardQCDAll_170To230_2017-02-03-125516
2.7783	  2.07346e-07	  0.138915	  0.000104465	  CTEQ5L-LO_HardQCDAll_20To30_2017-02-03-125516
3.7151e-05	  3.08877e-17	  1.85755e-06	  1.27502e-09	  CTEQ5L-LO_HardQCDAll_230To300_2017-02-03-125516
7.4081e-06	  1.18775e-18	  3.70405e-07	  2.50026e-10	  CTEQ5L-LO_HardQCDAll_300To380_2017-02-03-125516
0.64529	  1.08793e-08	  0.0322645	  2.39289e-05	  CTEQ5L-LO_HardQCDAll_30To50_2017-02-03-125516
2.0465e-06	  1.04396e-19	  1.02325e-07	  7.41249e-11	  CTEQ5L-LO_HardQCDAll_380To10000_2017-02-03-125516
0.077889	  1.44399e-10	  0.00389445	  2.7568e-06	  CTEQ5L-LO_HardQCDAll_50To80_2017-02-03-125516
0.0093138	  2.0207e-12	  0.00046569	  3.26118e-07	  CTEQ5L-LO_HardQCDAll_80To120_2017-02-03-125516
