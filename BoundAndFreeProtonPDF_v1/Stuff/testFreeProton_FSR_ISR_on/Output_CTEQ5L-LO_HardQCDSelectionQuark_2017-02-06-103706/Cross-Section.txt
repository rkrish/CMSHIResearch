

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
1.97348	  1.09968e-07	  0.098674	  7.60774e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-06-103706
0.0001823	  7.75486e-16	  9.115e-06	  6.38867e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-06-103706
3.6667e-05	  2.69143e-17	  1.83335e-06	  1.19019e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-06-103706
0.17404	  8.63627e-10	  0.008702	  6.74196e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-06-103706
8.2929e-06	  1.34696e-18	  4.14645e-07	  2.66257e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-06-103706
2.0338e-06	  7.90767e-20	  1.0169e-07	  6.4513e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-06-103706
0.046928	  6.43401e-11	  0.0023464	  1.8402e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-06-103706
7.1614e-07	  1.15632e-20	  3.5807e-08	  2.46697e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-06-103706
0.0069698	  1.33138e-12	  0.00034849	  2.64713e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-06-103706
0.00104848	  3.21442e-14	  5.2424e-05	  4.11315e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-06-103706
