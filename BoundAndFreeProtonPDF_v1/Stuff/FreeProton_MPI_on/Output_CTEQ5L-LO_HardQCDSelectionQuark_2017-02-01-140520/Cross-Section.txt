

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
2.2051	  1.34457e-07	  0.110255	  8.41231e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-01-140520
0.000177224	  7.30962e-16	  8.8612e-06	  6.20255e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-01-140520
3.5901e-05	  2.55041e-17	  1.79505e-06	  1.15859e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-01-140520
0.183536	  9.43306e-10	  0.0091768	  7.04611e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-01-140520
8.2582e-06	  1.3118e-18	  4.1291e-07	  2.62758e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-01-140520
2.0747e-06	  7.69036e-20	  1.03735e-07	  6.36204e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-01-140520
0.047959	  6.64375e-11	  0.00239795	  1.86995e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-01-140520
7.5828e-07	  1.27856e-20	  3.7914e-08	  2.59409e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-01-140520
0.0069191	  1.26178e-12	  0.000345955	  2.577e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-01-140520
0.00102396	  2.78901e-14	  5.1198e-05	  3.83132e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-01-140520
