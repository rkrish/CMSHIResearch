

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
13.8831	  5.48996e-06	  0.694155	  0.000537536	  CTEQ5L-LO_HardQCDMix_10To20_2017-02-06-162207
0.00062268	  8.92363e-15	  3.1134e-05	  2.16718e-08	  CTEQ5L-LO_HardQCDMix_120To170_2017-02-06-162207
0.000105536	  2.35319e-16	  5.2768e-06	  3.51926e-09	  CTEQ5L-LO_HardQCDMix_170To230_2017-02-06-162207
1.06297	  3.4004e-08	  0.0531485	  4.23047e-05	  CTEQ5L-LO_HardQCDMix_20To30_2017-02-06-162207
2.0091e-05	  8.63535e-18	  1.00455e-06	  6.7416e-10	  CTEQ5L-LO_HardQCDMix_230To300_2017-02-06-162207
4.1415e-06	  3.57166e-19	  2.07075e-07	  1.37107e-10	  CTEQ5L-LO_HardQCDMix_300To380_2017-02-06-162207
0.25705	  1.96549e-09	  0.0128525	  1.01709e-05	  CTEQ5L-LO_HardQCDMix_30To50_2017-02-06-162207
1.15815e-06	  3.37777e-20	  5.79075e-08	  4.21636e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2017-02-06-162207
0.032987	  2.72799e-11	  0.00164935	  1.19824e-06	  CTEQ5L-LO_HardQCDMix_50To80_2017-02-06-162207
0.0042289	  4.27752e-13	  0.000211445	  1.50044e-07	  CTEQ5L-LO_HardQCDMix_80To120_2017-02-06-162207
