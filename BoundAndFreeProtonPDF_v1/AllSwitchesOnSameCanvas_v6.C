
#include <iostream>
#include <map>
#include <string>


// ROOT Headers
#include <TFile.h>
#include <TH1D.h>
#include <TRegexp.h>
#include <TCanvas.h>
#include <TObject.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TLegend.h>
#include <TKey.h>
#include <TPRegexp.h>
#include <TList.h>

using namespace std;

typedef const char* cchar;

void Message(){
	cout << "This macro is a convenience script where one can get to see the positive, negative, ratio of free, bound proton pdf separately with all process switches on the same canvas" << endl;
}


void processHistograms(vector<cchar> vPath, cchar chargeType){

	map<cchar, TFile*> mTFile;
	map<cchar, TH1D*> mh1;
	map<string, Color_t> mColor; 

	mColor["HardQCDAll"] = kBlack;
	mColor["HardQCDSelectionQuark"] = kRed;
	mColor["HardQCDSelectionGluon"] = kGreen;
	mColor["HardQCDMix"] = kBlue;

	TLegend *leg = new TLegend(0.9, 0.8, 0.7, 0.6);

	//for ( cchar fileName : mfile["boundProtonPDF"] ){
	for ( cchar fileName : vPath ){
		cout << fileName << endl;

		mTFile[fileName] = new TFile(fileName, "read");
		cout << "Is the file open? " 
		     << mTFile[fileName]->GetName() << "\t"
		     << mTFile[fileName]->IsOpen() << endl;

		// Loop over objects in the file
		mTFile[fileName]->cd();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )) {

			cchar histName = key->GetName();

			//Read the histogram objects for +, -, -/+
			TString s0(histName);

			// Match only if there is a word ratio, pos, neg
			TPRegexp rLabel(chargeType);
			int matched = rLabel.Match(s0);
			if (matched >= 2){

				TString label = ((TObjString*)(rLabel.MatchS(s0)->At(1)))->GetString();
				//cout << "Input string: " << inString << endl;
				cout << "Matched label: " << label << endl;

				// Regex for TLegend
				TString sl(fileName);
				TPRegexp rl(".*_(HardQCD[a-zA-Z]+)_.*");
				TString forLeg = ((TObjString*)(rl.MatchS(sl)->At(1)))->GetString();
				mh1[fileName] = (TH1D*)mTFile[fileName]->Get(histName);
				mh1[fileName]->SetMarkerColor(mColor[(string)forLeg]);
				mh1[fileName]->SetLineColor(mColor[(string)forLeg]);
				mh1[fileName]->SetDirectory(0);

				leg->AddEntry(mh1[fileName], forLeg, "lep");
				//mh1[fileName]->Draw("same");
				//leg->Draw("same");
			}
		}//close-while

		//mTFile[fileName]->Close();
		
	// Read the histogram objects for +, -, -/+
	// Show them on Canvas
	}//close:for
}//close:processHistograms(vector<cchar> vPath)



/* Divide Histograms */
TH1D* divideHistograms(TH1D *hNum, TH1D *hDenom, TH1D *hRatio){

	cout << "from dividehistograms" << endl;
	cout << "hnum " << hNum->GetSize() << endl;
	cout << "hdenom " << hDenom->GetSize() << endl;
	cout << "hratio " << hRatio->GetName() << endl;
	
	if (hNum->GetSize() == hDenom->GetSize()){
		hRatio = (TH1D*)hNum->Clone();
		hRatio->Divide(hDenom);
	}

	return hRatio;
}//close-divideHistograms

string psMatchedFile(vector<cchar> vPath, string prSw, string matchedFile){

	TString psMatchFileName;
		for (string file : vPath){

			//TString sPs(mfile["freeProtonPDF"].at(i).c_str());
			TString sPs(file.c_str());
			TPRegexp rPs(Form(".*%s.*", prSw.c_str()));
			bool isAMatch = rPs.MatchB(sPs);

			//cout << "matched? " << isAMatch << endl;

			if(isAMatch) {
				psMatchFileName = ((TObjString*)(rPs.MatchS(sPs)->At(0)))->GetString();
				cout << "matched string: " << psMatchFileName << " " <<  isAMatch << endl;
			}

		}//close:for

	return (string)psMatchFileName;

}//close-psMatchedFile(vector<string> vPath, string prSw, string matchedFile)

void AllSwitchesOnSameCanvas_v6(vector<cchar> vPathFree, vector<cchar> vPathBound, cchar chargeType){

	Message();
	map<cchar, vector<cchar>> mfile;
	map<cchar, TFile*> mTFile;
	map<cchar, map<cchar,TH1D*>> mh1;
	map<string, Color_t> mColor; 

	TLegend *leg = new TLegend(0.9, 0.8, 0.7, 0.6);
	

	mfile["freeProtonPDF"] = vPathFree;
	mfile["boundProtonPDF"] = vPathBound;

	//processHistograms( vPathFree, chargeType);
	//processHistograms( vPathBound, chargeType);

	/* Bound/Free | same process Switch : (all/all, q/q, g/g, mix/mix) | (+, -) separately */
	vector<string> vPrSw = {"HardQCDAll", "HardQCDSelectionQuark", "HardQCDSelectionGluon", "HardQCDMix"};

	
		// Bound, Free separately
		//mfile["freeProtonPDF"];
		// Open the file
		// Read the histograms
		// Match the type of process switch
		// Apply a division operation
		// Apply cosmetics
		//divideHistograms();

	//string match;
	
	mColor["HardQCDAll"] = kBlack;
	mColor["HardQCDSelectionQuark"] = kRed;
	mColor["HardQCDSelectionGluon"] = kGreen;
	mColor["HardQCDMix"] = kBlue;

	// Loop over process switches
	map<string, string> mfreeMatch, mboundMatch;
	TString forLeg;
	for (string ps : vPrSw){

		mfreeMatch[ps] = psMatchedFile(mfile["freeProtonPDF"], ps, mfreeMatch[ps]);
		mboundMatch[ps] = psMatchedFile(mfile["boundProtonPDF"], ps, mboundMatch[ps]);
		
		// Get the file 
		mTFile["freeProtonPDF"] = new TFile(mfreeMatch[ps].c_str(), "read");
		mTFile["boundProtonPDF"] = new TFile(mboundMatch[ps].c_str(), "read");


		// Loop over objects in the file
		mTFile["freeProtonPDF"]->cd();
		{
		cchar fileName = mTFile["freeProtonPDF"]->GetName();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )) {

			cchar histName = key->GetName();

			//Read the histogram objects for +, -, -/+
			TString s0(histName);

			// Match only if there is a word ratio, pos, neg
			TPRegexp rLabel(chargeType);
			int matched = rLabel.Match(s0);
			if (matched >= 2){
				TString label = ((TObjString*)(rLabel.MatchS(s0)->At(1)))->GetString();
				cout << "Matched label: " << label << endl;

				// Regex for TLegend
				TString sl(fileName);
				TPRegexp rl(".*_(HardQCD[a-zA-Z]+)_.*");
				forLeg = ((TObjString*)(rl.MatchS(sl)->At(1)))->GetString();

		
				mh1["freeProtonPDF"][ps.c_str()] = (TH1D*)mTFile["freeProtonPDF"]->Get(histName);

				mh1["freeProtonPDF"][ps.c_str()] = (TH1D*)mTFile["freeProtonPDF"]->Get(histName);
				mh1["freeProtonPDF"][ps.c_str()]->SetMarkerColor(mColor[(string)forLeg]);
				mh1["freeProtonPDF"][ps.c_str()]->SetLineColor(mColor[(string)forLeg]);
				mh1["freeProtonPDF"][ps.c_str()]->SetDirectory(0);

				//leg->AddEntry(mh1["freeProtonPDF"][ps.c_str()], forLeg, "lep");
				//mh1["freeProtonPDf"][ps.c_str()]->Draw("same");
				//leg->Draw("same");

			}//close:check matched
		}//close-while
		}//close:scoping


		// Loop over objects in the file
		mTFile["boundProtonPDF"]->cd();
		{
		cchar fileName = mTFile["boundProtonPDF"]->GetName();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )) {

			cchar histName = key->GetName();

			//Read the histogram objects for +, -, -/+
			TString s0(histName);

			// Match only if there is a word ratio, pos, neg
			TPRegexp rLabel(chargeType);
			int matched = rLabel.Match(s0);
			if (matched >= 2){
				TString label = ((TObjString*)(rLabel.MatchS(s0)->At(1)))->GetString();
				cout << "Matched label: " << label << endl;

				// Regex for TLegend
				TString sl(fileName);
				TPRegexp rl(".*_(HardQCD[a-zA-Z]+)_.*");
				forLeg = ((TObjString*)(rl.MatchS(sl)->At(1)))->GetString();

				mh1["boundProtonPDF"][ps.c_str()] = (TH1D*)mTFile["boundProtonPDF"]->Get(histName);

				mh1["boundProtonPDF"][ps.c_str()] = (TH1D*)mTFile["boundProtonPDF"]->Get(histName);
				mh1["boundProtonPDF"][ps.c_str()]->SetMarkerColor(mColor[(string)forLeg]);
				mh1["boundProtonPDF"][ps.c_str()]->SetMarkerStyle(32);
				mh1["boundProtonPDF"][ps.c_str()]->SetLineColor(mColor[(string)forLeg]);
				mh1["boundProtonPDF"][ps.c_str()]->SetDirectory(0);

				//leg->AddEntry(mh1["boundProtonPDF"][ps.c_str()], forLeg, "lep");
				//mh1["boundProtonPDf"][ps.c_str()]->Draw("same");
				//leg->Draw("same");
		
			}//close:check matched
		}//close-while
		}//close:scoping

		// Divide free by bound per ps for one charge type
		mh1["freeBYbound"][ps.c_str()] = new TH1D();
		mh1["freeBYbound"][ps.c_str()]->SetName("free by bound");

		mh1["freeBYbound"][ps.c_str()] = divideHistograms(mh1["boundProtonPDF"][ps.c_str()], mh1["freeProtonPDF"][ps.c_str()], mh1["freeBYbound"][ps.c_str()]);
		mh1["freeBYbound"][ps.c_str()]->SetMarkerStyle(20);
		leg->AddEntry(mh1["freeBYbound"][ps.c_str()], Form("%s %s", ps.c_str(), chargeType), "lep");
		mh1["freeBYbound"][ps.c_str()]->Draw("same");
		leg->Draw("same");

	}//close:process switch

}//close-main
