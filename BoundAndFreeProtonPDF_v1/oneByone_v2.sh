#!/bin/bash

message(){

	echo "=============================================================="
	echo "This script is used to merge the spectra of different pT Hats \
	      and extract the positive and negative charged particle spectra \
	      and calculating the ratio of negative to positive spectra"
	echo "=============================================================="
	echo ; echo ;
}

message

mainFolder=( \
	BoundAndFreeProton_208_EPPS16LO_CT14lo \
	BoundAndFreeProton_208_EPS09NLO_CT10nlo \
	BoundProton_208_EPS09NLO_CT10nlo \
	FreeProton_208_CT10nlo \
	FreeProton_208_CT14nlo \
	BoundProton_208_EPPS16nlo \
	)


#For Bound and Free Proton PDF Merging pt hats * process sw
pTHatMerge(){
# Loop over (free,bound)
for mf in ${mainFolder[*]}
do
	mainFolder=${mf}
	psFolders=`ls ${mf}`

	echo ${psFolders}
	# Loop over (proc sw)
	for psFile in ${psFolders}
	do
		# execute the algorithm 

		# Loop over folders
		folder=$mainFolder/$psFile

		# CTEQ5L is a placeholder
		./getData.sh -p CTEQ5L-LO ${folder} > PDFStudyData.sh


		# Merge the pT Hat histograms
		./processAllOutputs.sh ${folder}
	done
done
}

pTHatMerge


#For Bound and Free Proton PDF Merging pt hats * process sw
calcRatioAndWriteToFile(){
# Loop over (free,bound)
for mf in ${mainFolder[*]}
do
	mainFolder=${mf}
	psList=`ls ${mf}/${mf}*.root`
	for psFile in ${psList}
	do
		echo "Processing the file: " ${psFile};
		echo "------------------------------------------------"
		root -b -q "Pythia8SpectraStandalone_v5.C+(\"$psFile\")"
		echo "------------------------------------------------"
		echo ; echo ;
		
	done
done
}

calcRatioAndWriteToFile 

