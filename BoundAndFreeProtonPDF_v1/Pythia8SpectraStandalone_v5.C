#pragma once

// C++ Headers
#include <iostream>
#include <vector>
#include <string>
#include <map>

// ROOT Headers
#include <TGraph.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TPRegexp.h>
#include "TClonesArray.h"
#include <TObjString.h>
#include <TString.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TFile.h>

using namespace std;

typedef const char* cchar;
typedef map<string, map<string, TH1D*>> map2Dh1;
typedef map<string, map2Dh1> map3Dh1;
 

/* Normalizing Variable Binned Histograms */
TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles
		lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist

TGraph* histToGraph(TH1D *h, TGraph *g)
{

	int NBINS = h->GetSize();
	double nx[NBINS], ny[NBINS];
	for (int tBin=0; tBin < NBINS ; tBin++){ 

		nx[tBin] = tBin;
		ny[tBin] = h->GetBinContent(tBin);

	}//close-for

	//g = new TGraph(NBINS, nx, h->GetArray() );
	g = new TGraph( NBINS, nx, ny );
	g->SetMarkerSize(h->GetMarkerSize());
	g->SetMarkerStyle(h->GetMarkerStyle());
	g->SetMarkerColor(h->GetMarkerColor());

	return g;

}//close-histToGraph

/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics


/* Show all the initialized map3D Labels */
void map3DLabels(map3Dh1 hTrack)
{

	for(map3Dh1::iterator it = hTrack.begin(); it != hTrack.end(); ++it){
		for(map2Dh1::iterator it1 = (&(it->second))->begin(); it1 != (&(it->second))->end(); ++it1){
			for (map<string, TH1D*>::iterator it2 = (&(it1->second))->begin(); it2 != (&(it1->second))->end(); ++it2){
				cout << "[\"" << it->first << "\"]" ;
				cout << "[\"" << it1->first << "\"]" ;
				cout << "[\"" << it2->first<<"\"]";
				cout << endl;
				}
			}
	}

}//close-map3DLabels


/* Check if the file is available or not */
void fileAvailability(cchar fileName)
{

	TFile *file = new TFile(fileName, "r");;
	if (!file->IsOpen()){
		cout << "\nThis file : " << file->GetName() << endl;
		cout << "is not available...." << endl;
		cout << "Exiting .... " << endl;
		exit(EXIT_FAILURE);
	}

	else {

	cout << "Is the file open? (1: yes, 0:no)\t" 
	     << file->GetName() << "\t"
	     << file->IsOpen() << endl;
	}
}

/* Get positive, negative spectra and calculate the ratio */
map3Dh1 processHist(cchar fileName, map3Dh1 mh1, string pdfName, string procSw)
{

	TFile *file = new TFile(fileName, "r");
	
	//fileAvailability(file) ;

	cchar sPathPos, sPathNeg, sPathAll;
	sPathPos = "QCDAna/pchspectrum";
	sPathNeg = "QCDAna/nchspectrum";
	sPathAll = "QCDAna/chspectrum";


	TH1D *hPos, *hNeg, *hRatio, *hAll;
	hPos = (TH1D*)file->Get(sPathPos)->Clone("hPos");
	hNeg = (TH1D*)file->Get(sPathNeg)->Clone("hNeg");
	hAll = (TH1D*)file->Get(sPathAll)->Clone("hAll");

	//hNeg = (TH1D*)hPos->Clone("hNeg");
	//hNeg->Add(hAll, hPos, 1, -1);

	//hNeg = (TH1D*)hAll->Clone("hNeg");
	//bool h1NegCalc = hNeg->Add(hPos, -1);
	//cout << "Negative histogram calculated from hAll and hPos: " << h1NegCalc << endl;

	hRatio = (TH1D*)hNeg->Clone("hRatio");

	hRatio->Divide(hPos);

	mh1[pdfName][procSw]["Pos"] = (TH1D*)hPos;
	mh1[pdfName][procSw]["Neg"] = (TH1D*)hNeg;
	mh1[pdfName][procSw]["Ratio-NegByPos"] = (TH1D*)hRatio;

	//file->Close();

	return mh1;

}//close-processHist

void InfoAboutMacro()
{

	cout << "\n\n";
	cout << "This macro is used to obtain the Charge Asymmetry for different PDF Sets" << endl;
	cout << "\n\n";

}//close-InfoAboutMacro


char* outputFileName(char *fileName){

	string n[5];

	//fileName="FreeProton_MPI_on/FreeProton_MPI_on_HardQCDSelectionQuark_Combined_2017-02-01-140520.root";
	//fileName="BoundProton_208_EPS09NLO_CT10nlo/BoundProton_208_EPS09NLO_CT10nlo_HardQCDSelectionQuark_Combined_2017-03-20-132257.root";
	TString s0(fileName);
	//TPRegexp r0("([a-zA-Z_]+)/([a-zA-Z_]+)_(HardQCD.*Combined)_([0-9-]+).root");
	TPRegexp r0("(.*)/(.*)_(HardQCD.*Combined)_([0-9-]+).root");
	n[1] = ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString();
	n[2] = ((TObjString*)(r0.MatchS(s0)->At(2)))->GetString();
	n[3] = ((TObjString*)(r0.MatchS(s0)->At(3)))->GetString();
	n[4] = ((TObjString*)(r0.MatchS(s0)->At(4)))->GetString();

	cout << n[1] << "\t"
	     << n[2] << "\t"
	     << n[3] << "\t"
	     << n[4] << "\t"
	     << endl;

	fileName = Form("ProcessedOutput_pdfs-%d-%s_%s_%s.root", 1, n[2].c_str(), n[3].c_str(), n[4].c_str());

	return fileName;
}//close-outputFileName


/** Main Program **/
void Pythia8SpectraStandalone_v5(char *inFileName)
{

	InfoAboutMacro();
	//char *userInput;

	vector<char*> vfileName;
	vfileName.push_back(inFileName);

	double varBinArr[] = { 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,
	        0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95,
	        1.1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 3.2, 4.0, 4.8, 5.6, 6.4,
	        7.2, 9.6, 12.0, 14.4, 19.2, 24.0, 28.8, 35.2, 41.6, 48.0, 60.8, 73.6, 86.4, 103.6,
	        130.0, 170, 200 };

	// Get the number of variables bins declared
	const int nVarBins = sizeof(varBinArr)/sizeof(double)-1;
	cout << "Number of Variable Bins declared: " << nVarBins << endl;

	for(auto file : vfileName) fileAvailability(file);

	vector<string> vpdfList;
	string pdfName, procSw;
	
	TString s0(vfileName[0]);
	TPRegexp r1("_([0-9-]+).root");

	string creationDate;
	creationDate = ((TObjString*)(r1.MatchS(s0)->At(1)))->GetString();

	TPRegexp r2("_([a-zA-Z]+)_(.*)");
	procSw = ((TObjString*)(r2.MatchS(s0)->At(1)))->GetString();

	map3Dh1 mh1;
	for (auto fName : vfileName)
	{

		TString s0(fName);
		TPRegexp r0("([a-zA-Z0-9-()+.]+)_(.*)");
		pdfName = ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString();

		vpdfList.push_back(pdfName); 
		mh1 = processHist( fName, mh1, pdfName, procSw );

	}//close-for: Loop over file names


	if(1)
	{

		TCanvas *cCharge = new TCanvas("cCharge", "", 900, 450);
		TCanvas *cRatio = new TCanvas("cRatio", "", 900, 450);

		TLegend *leg[3]; 
		leg[0] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[1] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[2] = new TLegend(0.6, 0.7, 0.8, 0.9);

		cCharge->cd();
		Color_t colArr[] = {kGreen, kBlue, kRed, kBlack, kCyan, kGreen-5, kRed-6, kGray, kBlue+3, 
				    kGreen-4, kOrange, kSpring+4, kCyan+2};
		for (unsigned int i=0; i < vpdfList.size(); i++){

			pdfName = vpdfList.at(i);	
			applyCosmetics(mh1[pdfName][procSw]["Pos"], kRed, (20+i), 1.5);
			applyCosmetics(mh1[pdfName][procSw]["Neg"], kBlue, (22+i), 1.5);

			leg[1]->AddEntry(mh1[pdfName][procSw]["Pos"], Form("Pos-%s", pdfName.c_str()), "lep");
			leg[2]->AddEntry(mh1[pdfName][procSw]["Neg"], Form("Neg-%s", pdfName.c_str()), "lep");

			mh1[pdfName][procSw]["Pos-VarBin"] = (TH1D*)mh1[pdfName][procSw]["Pos"]->Rebin(nVarBins, Form("%s%s-Pos-VarBin", pdfName.c_str(), procSw.c_str()), varBinArr);
			mh1[pdfName][procSw]["Neg-VarBin"] = (TH1D*)mh1[pdfName][procSw]["Neg"]->Rebin(nVarBins, Form("%s%s-Neg-VarBin", pdfName.c_str(), procSw.c_str()), varBinArr);

			mh1[pdfName][procSw]["Pos-VarBin"]= normalizeVarBinHist(mh1[pdfName][procSw]["Pos-VarBin"], nVarBins);	
			mh1[pdfName][procSw]["Neg-VarBin"]= normalizeVarBinHist(mh1[pdfName][procSw]["Neg-VarBin"], nVarBins);	

			//mh1[pdfName][procSw]["Pos"]->Draw("same CP");
			//mh1[pdfName][procSw]["Neg"]->Draw("same CP");
			
			mh1[pdfName][procSw]["Pos-VarBin"]->Draw("same CP");
			mh1[pdfName][procSw]["Neg-VarBin"]->Draw("same CP");

			leg[1]->Draw("same");
			leg[2]->Draw("same");

		}//close-for: Loop over pdf names 
		gPad->SetLogy();


		cRatio->cd();
			for (unsigned int i=0; i < vpdfList.size(); i++){

				pdfName = vpdfList.at(i);
				applyCosmetics(mh1[pdfName][procSw]["Ratio-NegByPos"], kSpring+4, (20), 1.5);

				cout << "Standard Deviation for : " 
				     << pdfName << "\t"
				     << mh1[pdfName][procSw]["Ratio-NegByPos"]->GetStdDev(1) 
				     << endl;


				mh1[pdfName][procSw]["Ratio-NegByPos"]->Rebin(5);
				mh1[pdfName][procSw]["Ratio-NegByPos"]->Scale(0.2);
				mh1[pdfName][procSw]["Ratio-NegByPos"]->Draw("same P");

				//mg[pdfName] = histToGraph(mh1[pdfName][procSw]["Ratio-NegByPos"], mg[pdfName]);
				//multiG->Add(mg[pdfName]);


				leg[0]->AddEntry(mh1[pdfName][procSw]["Ratio-NegByPos"], pdfName.c_str(), "lep");
				leg[0]->Draw("same");
				gPad->SetGrid();
			}

		//TCanvas *cGraph = new TCanvas("cGraph", "", 900, 450);
		//cGraph->cd();
		//multiG->Draw("alp");
	}

	/* Writing the output to a file */
	TFile *oFile = new TFile(outputFileName(inFileName),  "recreate");
	for (auto pdfName : vpdfList){

		(mh1[pdfName][procSw]["Ratio-NegByPos"])->Write(Form("%s_%s_%s", "Ratio-NegByPos",pdfName.c_str(), procSw.c_str()));
		(mh1[pdfName][procSw]["Pos"])->Write(Form("%s_%s_%s", "Pos", pdfName.c_str(), procSw.c_str()));
		(mh1[pdfName][procSw]["Neg"])->Write(Form("%s_%s_%s", "Neg", pdfName.c_str(), procSw.c_str()));
	}

	oFile->Close();
	
}//close-main

