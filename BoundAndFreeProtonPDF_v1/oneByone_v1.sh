#!/bin/bash

mainFolder=testFreeProton_FSR_ISR_on
psFileList=`ls ${mainFolder}`

for psFile in ${psFileList}
do
	# Loop over folders
	folder=$mainFolder/$psFile
	#echo ${folder}

	# CTEQ5L is a placeholder
	./getData.sh -p CTEQ5L-LO ${folder} > PDFStudyData.sh

	#cat PDFStudyData.sh

	# Merge the pT Hat histograms
	./processAllOutputs.sh ${folder}
done



