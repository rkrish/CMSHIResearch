## June 29, 2017

The purpose of this folder is to generate the plots for xVSpt plots for positive and negative charged particles

1. h2->(xVal, yVal)
2. h2->(xErr, yErr) : Needs to be sorted out, because of the way stuff is populated

Design :
1. Read h2 : xVSpt
2. Per xBin, 10 bins total, pull the pt histograms i.e. h1 { x[1] : pT[{bins}] }, x[2] : pT[{bins}], etc for all 10 bins
3. Do this for bound, free, boundAndFree, and plot such h1 overlapping

### xVSpt_v1.C
Has the core functionality, read files, pick up the pos, neg for x vs pT, to implement the Bound, Free, BoundAndFree



## August 31, 2017
---
xVSpt_v3.C : This script takes an input file, reads a tree, generates a TH2D and draws it, more importantly, it has the options like
`h2->SetMaximum(100);`
`h2->SetMinimum(40);`
h2 is a TH2D, this way, the color palette can be made sensitive to the range the user would like to highlight. 




