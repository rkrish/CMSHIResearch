#include <iostream>

#include <TFile.h>
#include <TH2D.h>
#include <TH1D.h>
#include <math.h>

using namespace std;

/* Main Program */
void projectXAxis(){

	// Get the file
	TFile *file = new TFile("deleteTest/Output_BoundProton_EPPS16_CT14nlo_50to80-10_numEvent50000.root");
	cout << file->IsOpen();
	
	TH2D *h2;
	TH1D *hx[10], *hy[10];

	h2 = (TH2D*)file->Get("QCDAna/xVSpT_Beam_Pos");
	//h1 = h2->ProjectionY();
	cout << h2->GetNbinsX() << endl;
	cout << h2->GetNbinsY() << endl;
	cout << ((TAxis*)h2->GetXaxis())->GetNbins() << endl;
	cout << ((TAxis*)h2->GetYaxis())->GetNbins() << endl;
	cout << ((TAxis*)h2->GetXaxis())->GetXmax() << endl;
	cout << ((TAxis*)h2->GetXaxis())->GetXmin() << endl;

	
	int nXBins, nXLow, nXHigh, nYBins, nYLow, nYHigh;
	nXLow = ((TAxis*)h2->GetXaxis())->GetXmin();
	nXHigh = ((TAxis*)h2->GetXaxis())->GetXmax();
	nXBins = ((TAxis*)h2->GetXaxis())->GetNbins();

	nYLow = ((TAxis*)h2->GetYaxis())->GetXmin();
	nYHigh = ((TAxis*)h2->GetYaxis())->GetXmax();
	nYBins = ((TAxis*)h2->GetYaxis())->GetNbins();

	hx[0] = new TH1D("h1", "per x", nXBins, nXLow, nXHigh);

	// Initialize hy per x-bins
	for (int i=0; i < nXBins; i++) hy[i] = new TH1D(Form("hy[%d]", i), "per y", nYBins, nYLow, nYHigh);

	if (1){
	//for (unsigned int ix=0; ix < h2->GetNbinsX(); ix++){
	for (unsigned int ix=0; ix < (h2->GetXaxis())->GetNbins(); ix++){
		for (unsigned int iy=0; iy < (h2->GetYaxis())->GetNbins(); iy++){
		//for (unsigned int iy=0; iy < h2->GetNbinsY(); iy++){
			double n = h2->GetBinContent(ix, iy);
			double ne = h2->GetBinErrorUp(ix, iy);
			double bcx = ((TAxis*)h2->GetXaxis())->GetBinCenter(ix);
			double bcy = ((TAxis*)h2->GetYaxis())->GetBinCenter(iy);
			//cout << "round(n): " << round(n) << endl;
			//cout << n << ", ";
			if ( round(n) > 0 ) { 
				cout << "ix: " << ix << "\t"
				     << "iy: " << iy << "\t" 
				     << "bcx: " << bcx << "\t" 
				     << "bcy: " << bcy << "\t\t" 
				     << n << ", " << ne << endl;
			}
		}//close:for
	}//close:for
	}


	if (0){
	//for (unsigned int ix=0; ix < h2->GetNbinsX(); ix++){
		int ix=1;
		for (unsigned int iy=0; iy < (h2->GetYaxis())->GetNbins(); iy++){
		//for (unsigned int iy=0; iy < h2->GetNbinsY(); iy++){
			int n = h2->GetBinContent(ix, iy);
			int ne = h2->GetBinErrorUp(ix, iy);
			int bcx = ((TAxis*)h2->GetXaxis())->GetBinCenter(ix);
			int bcy = ((TAxis*)h2->GetYaxis())->GetBinCenter(iy);
			//cout << n << ", ";
			if ( n > 0 ) { 
				cout << "ix: " << ix << "\t"
				     << "iy: " << iy << "\t" 
				     << "bcx: " << bcx << "\t" 
				     << "bcy: " << bcy << "\t\t" 
				     << n << ", " << ne << endl;
			hy[0]->SetBinContent(iy, n);
			hy[0]->SetBinError(iy, ne);
			}
		}//close:for
	}


	if (0){
	//for (unsigned int ix=0; ix < h2->GetNbinsX(); ix++){
		int ix=2;
		for (unsigned int iy=0; iy < (h2->GetYaxis())->GetNbins(); iy++){
		//for (unsigned int iy=0; iy < h2->GetNbinsY(); iy++){
			int n = h2->GetBinContent(ix, iy);
			int ne = h2->GetBinErrorUp(ix, iy);
			double bcx = ((TAxis*)h2->GetXaxis())->GetBinCenter(ix);
			double bcy = ((TAxis*)h2->GetYaxis())->GetBinCenter(iy);
			//cout << n << ", ";
			if ( n > 0 ) { 
				cout << "ix: " << ix << "\t"
				     << "iy: " << iy << "\t" 
				     << "bcx: " << bcx << "\t" 
				     << "bcy: " << bcy << "\t\t" 
				     << n << ", " << ne << endl;
			hy[1]->SetBinContent(iy, n);
			hy[1]->SetBinError(iy, ne);
			}
		}//close:for
	}

	hy[0]->Draw("same p");
	hy[1]->Draw("same p");
	//h1->Draw("same");
	//h2->Draw("surf3");


}//close:main
