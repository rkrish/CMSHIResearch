#!/bin/bash

rm *.so; rm *.pcm; rm *.d;

folderList=(\
	BoundAndFreeProton_EPPS16_CT14nlo_2017-08-30-211258 \
	BoundAndFreeProton_EPS09_CT10nlo_2017-08-30-211234 \
	FreeProton_CT10nlo_2017-08-30-211321 \
	FreeProton_CT14nlo_2017-08-30-211343 \
)

#folderList=(BoundAndFreeProton_EPPS16_CT14nlo_2017-08-30-211258)

for folder in ${folderList[@]}
do

	fileList=`ls $folder/*.root | sed 's/^/"/g' | sed 's/$/"/g' | tr "\n" "," | sed 's/^/{/g' | sed 's/,$/}/g'`
	#root -b -q "xVSpt.C+(${fileList}, \"${folder}\")"
	echo ; echo ;

done

unset folderList
folderList=`ls -d GenOutput/*`

for folder in ${folderList[@]}
do

	#echo $fileList
	fileList=`ls $folder/*.root | sed 's/^/"/g' | sed 's/$/"/g' | tr "\n" "," | sed 's/^/{/g' | sed 's/,$/}/g'`
	echo $fileList
	#root -b -q "xVSpt.C+(${fileList}, \"${folder}\")"
	echo ; echo ;

done


rm *.so; rm *.pcm; rm *.d;
