#include <iostream>
#include <string>

#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TProfile.h>
#include <TString.h>
#include <TPRegexp.h>
#include <TCanvas.h>
#include <TLegend.h>

using namespace std;

typedef const char* cchar;

TProfile* applyCosmetics(TProfile* hp, Color_t mCol, int mStyle, float mSize )
{
	        hp->SetLineColor(mCol);
		hp->SetMarkerStyle(mStyle);
	        hp->SetMarkerColor(mCol);
		hp->SetMarkerSize(mSize);
		hp->SetStats(0);
        
		return hp;

}//close-applyCosmetics

void xVSpt_test(){

	map<string, TFile*> mfile;
	map<string, TTree*> mtree;
	map<string, map<string, TProfile*>> mprof;
	map<string, string> ml;

	Color_t col[] = {kRed, kGreen, kBlue, kRed+3, kGreen-2, kBlue-3, kRed-3, kBlue+5, kOrange, kBlue-1}; int i=0;

	mfile["EPPS16+CT14nlo"] = new TFile("GenOutput/BoundAndFree_EPPS16_CT14nlo_temp/BoundAndFree_EPPS16_CT14nlo_numEvent100000.root");
	mfile["EPS09+CT10nlo"] = new TFile("GenOutput/BoundProton_EPS09_CT10nlo_temp/BoundProton_EPS09_CT10nlo_numEvent100000.root");
	mfile["EPPS16"] = new TFile("GenOutput/BoundProton_EPPS16_CT14nlo_temp/BoundProton_EPPS16_CT14nlo_numEvent100000.root");
	mfile["EPS09"] = new TFile("GenOutput/BoundProton_EPS09_CT10nlo_temp/BoundProton_EPS09_CT10nlo_numEvent100000.root");
	mfile["CT10nlo"] = new TFile("GenOutput/FreeProton_CT10nlo_temp/FreeProton_CT10nlo_numEvent100000.root");
	mfile["CT14nlo"] = new TFile("GenOutput/FreeProton_CT14nlo_temp/FreeProton_CT14nlo_numEvent100000.root");

	string mk;

	ml = {{"x1_Pos","x1:hadronPos"}, {"x1_Neg","x1:hadronNeg"}, {"x2_Pos", "x2:hadronPos"}, {"x2_Neg", "x2:hadronNeg"}};
	map<string, string>::iterator mit;
	for (map<string, TFile*>::iterator mf = mfile.begin(); mf != mfile.end(); ++mf){
		mtree[mf->first] = (TTree*)mfile[mf->first]->Get("QCDAna/tree");
		i=0;
		//cout << mf->first << "\t";
		for ( mit = ml.begin() ; mit != ml.end(); ++mit){

			mk = mit->first;
			const char* label = Form("mprof%s_%s", mf->first.c_str(), mk.c_str());
			//cout << mf->first << " " << mk << " " << label << endl;
			mprof[mf->first][mk] = new TProfile(label, "", 10, 0, 100, -5, 5);
			mtree[mf->first]->Project(label, mit->second.c_str());
			applyCosmetics(mprof[mf->first][mk], col[i], 20, 2); i++;

		}//close:for
	}//close:for





	mk = "x1_Pos";
	mprof["EPPS16+CT14nlo"][mk]->Draw("p e0 same");

	mk = "x1_Pos";
	mprof["CT14nlo"][mk]->Draw("p e0 same");

}//close:main
