#!/bin/bash


# Input files for MinBias
inFileArray=(
output_MinimumBias1_2016-02-26.root \
output_MinimumBias2_2016-02-26.root \
output_MinimumBias3_2016-02-26.root \
output_MinimumBias4_2016-02-26.root \
output_MinimumBias5_2016-02-26.root \
output_MinimumBias6_2016-02-26.root \
output_MinimumBias7_2016-02-26.root \
output_MinimumBias8_2016-02-26.root \
output_MinimumBias9_2016-02-26.root \
output_MinimumBias10_2016-02-26.root \
output_MinimumBias11_2016-02-26.root \
output_MinimumBias12_2016-02-26.root \
output_MinimumBias13_2016-02-26.root \
output_MinimumBias14_2016-02-26.root \
output_MinimumBias15_2016-02-26.root \
output_MinimumBias16_2016-02-26.root \
output_MinimumBias17_2016-02-26.root \
output_MinimumBias18_2016-02-26.root \
output_MinimumBias19_2016-02-26.root \
output_MinimumBias20_2016-02-26.root \
)

# Number of events in the array sequence
nEvents=(
126809757 126998875 126853017 127250025 127169537 \
127256729 127256692 127239988 127222974 127220628 \
126325160 127207059 125206184 126522737 126753153
)

for i in {0..5}
do
	echo "======================================"
	echo "Generate the file for Invariant Yields"
	echo "======================================"


	# Input File from the array
	inFile=${inFileArray[$i]}
	outFile="VariableBinnedResultspp$i.root"

	echo ${outFile} ${inFile}
#	root -b -q -l "ChargedParticlesVariableBinning_v2.C+(\"${inFile}\", \"${outfile}\", ${nEvents})"
	root -b -q -l "readFile.C+(\"${inFile}\", \"${outFile}\", ${nEvents[$i]})"


done


<<comment1

Use this macro to pass a string and a 
number to the macro from the commandline for
batch processing. 

comment1
