#include <iostream>

using namespace std;

void readFile(const char *inFile, const char *outFile, const double nEvents){

	cout << "Information passed from the commandline" << endl;
	cout << "=================================================" << endl;
	cout << "Input file name from the program: " << inFile << endl;
	cout << "Ouput file name from the program: " << outFile << endl;
	cout << "Number of events " << nEvents << endl;
	cout << "=================================================" << endl;

	cout << "Writing to the file" << endl;
	cout << "Closing the file" << endl;

	cout << "Saving the file in png format" << endl;
}
