#!/bin/bash

case $1 in 

	1)
		path=/home/janjamrk/CMSSW_7_4_0/src/CMSHIResearch/Pythia8Analysis/test/Pythia8Study_v8
		scp -r janjamrk@login.accre.vanderbilt.edu:${path}/GenOutput/*.root .
		;;

	2)
		path=/home/janjamrk/CMSSW_7_4_0/src/CMSHIResearch/Pythia8Analysis/src
		scp -r janjamrk@login.accre.vanderbilt.edu:${path}/*.cc v1
		;;

esac


