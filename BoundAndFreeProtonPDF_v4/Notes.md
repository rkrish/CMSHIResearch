### April 10, 2017
---

#### ``` oneByone_v2.sh ```:
This script has information related to pulling up a folder from ACCRE, combining pT Hats per folder, there's lot of nesting folder and stuff in it. But basically, it does pTHat combination, ```Pythia8SpectraStandalone_v8.C```, pulls the required information i.e. required histograms and applies cosmetics and does variable binning and writes to an output file that has the file name ```Processed-1-pdfs*root```, this is the file that is finally pulled out into another macro and applied additional cosmetics to generate the plots. 

### April 11, 2017
---

#### ``` Pythia8SpectraStandalone_v8.C ```:
No issues, it very well normalizes the histograms comparing with v7, which doesn't normalize the histograms. Note that the process of normalization requires the number of events to be hardcoded into the macro. If the histogram already contains the number of events, then its better it could be extracted and used without hard coding it.  This macro should be used with ```oneByone_v2.sh```


#### ``` oneByone_v2.sh ```:
[Bug] Some bug with this file, when the array is initalized, it processes everything, but doesn't copy the first object in the array. 
Not quite using this, the idea is to normalize the histograms with the total number of events. However, there's no output in the end. Requires some debugging. Normalization may be required for plots that go into the thesis. So, should keep an eye on this to be done and keep it in the to do list. This macro should be used with ```oneByone_v2.sh```
