
void errorBar(){

	TH1D *h = new TH1D("h", "", 10, 1, 5);

	for (int i=0; i <10; i++) h->Fill(2);

	h->SetBinError(1, 5.2);
	h->SetMarkerColor(kBlue);
	h->SetMarkerStyle(20);
	h->SetMarkerSize(2);

	h->Draw("e1");

}
