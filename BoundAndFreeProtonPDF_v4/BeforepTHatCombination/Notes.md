### ```April 18, 2017```

#### ```BeforepTHatCombination```

This folder contains the macros to check how the pTHat histograms look like before the pTHat Combination.

### ```April 19, 2017```

The files ```processRatioBefore.sh``` requires ```ratioBefore_v1.C``` are dependent on each other. The macro _v1.C takes the input files in the format as given in the shell script and draws the ptHat ranges after dividing the bound and free type specified in the shell script. Basically whats happening here is the shell script takes the input files for bound, free or whatever of the type and this path is read by the macro, which picks up all the pTHat range files and divides them matching the ptHat range and draws them on canvas. 

This is done as a comparison to see, before pTHat combination how the EPS based pdf sets are doing compared to CT based free proton pdfs. 

