
#!/bin/bash

pathA="BoundAndFreeProton_208_EPS09NLO_CT10nlo/Output_CTEQ5L-LO_HardQCDAll_2017-03-29-104604"
pathB="FreeProton_208_CT10nlo/Output_CTEQ5L-LO_HardQCDAll_2017-04-07-055552"
pdfTypeA="EPS09+CT10nlo"
pdfTypeB="CT10nlo"
histObjName="QCDAna/chspectrum"

# histObjName="QCDAna/jetspectrum"

# Get the date information
dtA=`sed -n 's/.*_\([0-9-]\+\)$/\1/p' <<< $pathA`
dtB=`sed -n 's/.*_\([0-9-]\+\)$/\1/p' <<< $pathB`

echo "date : $dtA, $dtB"

# This macro will plot all the pTHats onto a single canvas before reweighing
root -l "ratioBefore_v1.C+(\"$pathA\", \"$pathB\", \"$pdfTypeA\", \"$pdfTypeB\", \"$dtA\", \"$dtB\", \"$histObjName\")"

