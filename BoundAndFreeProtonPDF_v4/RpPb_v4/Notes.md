### April 7, 2017

#### ``` plotRpPb_v5.C : ``` 

Has the implementation for Data to Monte Carlo for Ratio of Negative to Positive charged particles. There's an if() condition to the end of the program, when turned on will plot the information. Make sure, the required input files are present, it requires 6 input files, EPS09, EPPS16, CT10nlo, CT14nlo, EPPS16+CT14nlo, EPS09+CT10nlo.

A small correction is required here, where the information for CT10nlo is not right in the code i.e. the file put is not right, the jobs are currently running and once they are done, should be tested here. 


### April 9, 2017
####``` plotRpPb_v6.C : ```

Has the RpPb for positive, negative, all from data and comparision from EPPS16+CT14nlo and EPS09+CT10nlo i.e basically the bound pdf collided with free pdf for the proton. It is a clone of plotRpPb_v5.C

Also has Neg/Pos ratio for each of the pdf sets. Needs some refactoring i.e. create a **map<string, array>** data structure so that the cosmetics can be applied easily in a loop
