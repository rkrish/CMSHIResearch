// C++ Headers
#include <iostream>
#include <string>
#include <array>

// ROOT Headers
#include <TCanvas.h>
#include <TString.h>
#include <TH1D.h>
#include <TFile.h>
#include <TGraph.h>
#include <TLatex.h>
#include <TObjArray.h>

using namespace std;


/* Global pad options for the canvas */
TCanvas* padOptions(TCanvas *c, signed int numPads)
{

	// Loop over all Pads
	for (int i=1; i <= numPads; i++){
		c->cd(i);
		gPad->SetGrid();
		gPad->SetLogy();
		gPad->SetLogx();
	}	

	return c;
	
}//close-padOptions



/* Cosmetics for TGraph */
TGraph* graphCosmetics(TGraph *g, int mstyle, int mcolor, double msize)
{

	g->SetMarkerStyle(mstyle);
	g->SetMarkerColor(mcolor);
	g->SetMarkerSize(msize);
	return g;
} // close-graphCosmetics



/* Calculate Invariant Yield */
TGraph* calcInvYield(TH1D *h, TGraph *g, int nEvents, int pTMin, int pTMax)
{

	static int nBins = h->GetSize();
	cout << "Number of bins calcInvYield " << nBins << endl;
	double pT, dpT, dN; 
	double InvYield[nBins], pTBins[nBins];
	for (int tBin=0; tBin < nBins; tBin++)
	{
		// Inv Yield for Positively Charged Particles
		pT = h->GetBinCenter(tBin);	
		dpT = h->GetBinWidth(tBin);	
		dN = h->GetBinContent(tBin);

		// Initialize to 0, incase of any garbage
		InvYield[tBin]=0; pTBins[tBin]=pT;
	
		// Calculate Invariant Yield only for the range specified
		// by pTMin and pTMax window
//		if (pT > pTMin && pT < pTMax) InvYield[tBin] = dN /(dpT*pT*nEvents);
		if (pT > pTMin && pT < pTMax) InvYield[tBin] = dN /(dpT*pT);
		else InvYield[tBin] = 0;
		
		cout << tBin << ": "<< pTBins[tBin] << "\t"
		     << ": " << InvYield[tBin] << "\n";
		


	} // Close for loop

	g = new TGraph(nBins, pTBins, InvYield);

//	for (int i=0; i<nBins; i++) cout << g->GetX()[i] << ": "<< g->GetY()[i] << "\n";

	cout << "============= calcInvYield : Executed successfully =============" << endl;
		return g;
}//close-calcInvYield



/* Normalize Variable Binned Histograms */
TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles
		lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
	//	lbinError = hVarPosPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
	//	hVarPosPt->SetBinError(tBin, lbinError/lbinWidth);

		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";

	}

	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	return hVarPt;
}//close - normalize variable binned histograms



/* Calculate Invariant Yield Ratio */
TGraph* calcInvYieldRatio(TGraph *g1, TGraph *g2, TGraph *g2Byg1)
{
	int maxBins = g1->GetN();
	double kg2Byg1[maxBins], kPos[maxBins], kNeg[maxBins],
	       pTBins[maxBins];
	bool kValueCheck;
	for (int tBin=0; tBin < maxBins; tBin++)
	{
		
		// Read the points from the array
		kPos[tBin] = g1->GetY()[tBin];
		kNeg[tBin] = g2->GetY()[tBin];
		pTBins[tBin] = g1->GetX()[tBin];

		// Check if the denominator = 0
		kValueCheck = ( kPos[tBin] != 0 );
		if ( kValueCheck ) kg2Byg1[tBin] = kNeg[tBin] / kPos[tBin];
		/*
		cout << kg2Byg1[tBin] << ","
		     << kPos[tBin] << "\t"
		     << kNeg[tBin] << "\n";
		     */

	}
	

	g2Byg1 = new TGraph(maxBins, pTBins, kg2Byg1);

	cout << "============= calcInvariantYieldRatio : Executed successfully =============" << endl;
	return g2Byg1;
}



/* Main program */
void InvYieldVariableBinning_v2(const char *inFile, string collisionSystem)
{


	// Data for event normalization from DAS
	double nEvents_pPb = 36872579, 
	       nEvents_pp=2536648731
		       ;
//	       nEvents_pp=127220628;

	cout << "====================================================" << endl;
	cout << "Collision System chosen: " << collisionSystem << endl;
	cout << "====================================================" << endl;


	double nEvents; 
	double pTMax, pTMin;

	// TGraph, THist axis limit variable declaration
	double gYLow, gYHigh,
	       gXLow, gXHigh,
	       hXLow, hXHigh,
	       hYLow, hYHigh;

	const char *outputFileName;

	// X-axis range for TGraph Inv Yield
	gXLow=1; gXHigh=200;

	// X-axis & Y-axis range for Histograms
	hXLow=1; hXHigh=200;
	hYLow=1e-11; hYHigh=5;

	// Range upto which Inv Yield is calculated
	pTMin=0.4; pTMax=200; 

	// Pass variables to the program based on 
	// the collision system
	if (collisionSystem == "pp"){
	 nEvents = nEvents_pPb; 
	 gYLow=1e-9; gYHigh=1e+2; 
	 outputFileName = "output_ppInvariantYieldForSelectedpTRange.root";

	}

	else if (collisionSystem == "pPb"){
	 nEvents = nEvents_pp;
	 gYLow=1e-11; gYHigh=1e+3; 
	 outputFileName = "output_pPbInvariantYieldForSelectedpTRange.root";
	}

	else {cout << "Invalid Option, please choose either pp or pPb" << endl; exit(1);}


	// Initialize TCanvas objects
	TCanvas *cRaw = new TCanvas ("cRaw", "", 900, 500);
	cRaw->Divide(3, 1);

	TCanvas *cInvYield = new TCanvas("cInvYield", "", 900, 500);
	cInvYield->Divide(3, 1);

	TCanvas *cVarInvYield = new TCanvas("cVarInvYield", "", 900, 500);
	cVarInvYield->Divide(3, 1);


	// Reading from the input file
	TFile *inputFile = new TFile(inFile);

	cout << "=============================================" << endl;
	cout << "Are the files open ? " << "\n"
	     << inFile << "\t" << inputFile->IsOpen() << "\n";
	cout << "=============================================" << endl;

	// Initiate histograms
	TH1D *hPosPt, *hNegPt;

	// Inv Yield Histograms
	TH1D *hPosInvYield, *hNegInvYield, *hNegByPosInvYield;

	// New Histograms for selected pT Range
	TH1D *hnewPosInvYield, *hnewNegInvYield,
	     *hnewPosPt, *hnewNegPt;

	// Path to the histograms in the file
	const char *sPosPt, *sNegPt;
 	sPosPt = "/userAnalyzer/htrackPosPt";
        sNegPt = "/userAnalyzer/htrackNegPt";

	// Set gPad options globally
	padOptions(cRaw, 3);
	padOptions(cInvYield, 3);
	padOptions(cVarInvYield, 3);

	// Read Pos and Neg pT Spectra from Data
	hPosPt = (TH1D*)inputFile->Get(sPosPt);
	hNegPt = (TH1D*)inputFile->Get(sNegPt);

	// Normalize the input histograms with num Events
	hPosPt->Scale(1/nEvents);
	hNegPt->Scale(1/nEvents);

	// Number of Bins in histograms for
	// Positive and Negative from Data
	double maxNPosBins = hPosPt->GetSize();
	double maxNNegBins = hNegPt->GetSize();

	// Check if the num of bins for pos and neg are same
	if ( maxNPosBins == maxNNegBins ) cout << "The number of bins in Positive and Negative distributions are identical" << endl;

	// Loop over all the TGraph bins
	// to calculate the ratio
	double kNeg[100], kPos[100],
	       kInvYieldRatio[100], 
	       pTBins[100];

	// Initialize TGraphs for Invariant Yield
	TGraph *graphPosInvYield=0, *graphNegInvYield=0, 
	       *graphNegByPosInvYield=0;

	// Invoke the function to calculate Invariant Yield
	graphPosInvYield = calcInvYield(hPosPt, graphPosInvYield, nEvents, pTMin, pTMax);
	graphNegInvYield = calcInvYield(hNegPt, graphNegInvYield, nEvents, pTMin, pTMax);

	// Calculate the Invariant Yield Ratio
	graphNegByPosInvYield = calcInvYieldRatio(graphPosInvYield, graphNegInvYield, graphNegByPosInvYield);

        /******************** VARIABLE BINNING ***********************/

	// Initialize hist and graph for variable binning
	TH1D *hVarPosPt, *hVarNegPt;
	TGraph *gVarPosPt, *gVarNegPt, *gBinWidth;

	// Information for variable binning
	double varBinArr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 50, 80, 120, 200};

	// Get the number of variables bins declared
	int nVarBins = sizeof(varBinArr)/sizeof(double)-1;

	// Rebinning from the original histograms
	hVarPosPt = (TH1D*)hPosPt->Rebin(nVarBins, "hVarPosPt", varBinArr);
	hVarNegPt = (TH1D*)hNegPt->Rebin(nVarBins, "hVarNegPt", varBinArr);

	// Normalizing Rebinned Histograms and rewriting back to themselves
	hVarPosPt = normalizeVarBinHist(hVarPosPt, nVarBins);	
	hVarNegPt = normalizeVarBinHist(hVarNegPt, nVarBins);	

	// Initialize TGraphs for Invariant Yield after Rebinning
	TGraph *gVarPosInvYield=0, *gVarNegInvYield=0,
	       *gVarInvYieldRatio=0;

	// Calculate Invariant Yield and then the Ratio
	gVarPosInvYield = calcInvYield(hVarPosPt, gVarPosInvYield, nEvents, pTMin, pTMax);
	gVarNegInvYield = calcInvYield(hVarNegPt, gVarNegInvYield, nEvents, pTMin, pTMax);

	// Checking for data in TGraph inv Yield with variable binning
		cout << "=============CHECKING FOR INV YIELD AFTER VARIABLE BINNING==================" << endl;
	for (int i=0; i < gVarPosInvYield->GetN(); i++){
		cout << "( bin, pT, Inv Yield )" << "\t"
			<< gVarPosInvYield->GetX()[i] << ", "
			<< gVarPosInvYield->GetY()[i] << endl;
	}

	// Now the Inv Yield Ratio
	gVarInvYieldRatio = calcInvYieldRatio(gVarPosInvYield, gVarNegInvYield, gVarInvYieldRatio);

	/***********************  Drawing on the Canvas *************************/
	// Variable Binned Histogram Apply Histogram Cosmetics and Ranges  
	hPosPt->SetAxisRange(hYLow, hYHigh, "Y");
	hVarPosPt->SetAxisRange(hYLow, hYHigh, "Y");
	hVarPosPt->SetLineColor(kRed);

	hNegPt->SetAxisRange(hYLow, hYHigh, "Y");
	hVarNegPt->SetAxisRange(hYLow, hYHigh, "Y");
	hVarNegPt->SetLineColor(kRed);

	TH1D *hVarNegPtClone = (TH1D*)hVarNegPt->Clone();
	hVarNegPtClone->Divide(hVarPosPt);
	hVarNegPt->SetLineColor(kRed);


	// Histograms from Data, Ratio plots for histograms
	hPosPt->SetTitle("Positively Charged Particles Normalized Hist pT Distribution");
	hPosPt->GetYaxis()->SetTitle("Counts per 200MeV/c");
	hPosPt->GetXaxis()->SetTitle("p_{T} (GeV/c)");
	hPosPt->SetAxisRange(hXLow, hXHigh, "X");

	hNegPt->SetTitle("Negatively Charged Particles Normalized Hist pT Distribution");
	hNegPt->GetYaxis()->SetTitle("Counts per 200MeV/c");
	hNegPt->GetXaxis()->SetTitle("p_{T} (GeV/c)");
	hNegPt->SetAxisRange(hXLow, hXHigh, "X");

	TH1D *hNegPtClone;
	hNegPtClone = (TH1D*)hNegPt->Clone();

	hNegPtClone->Divide(hPosPt);
	hNegPtClone->SetTitle("Neg By Pos Normalized Hist Ratio");
	hNegPtClone->GetYaxis()->SetTitle("#frac{Neg}{Pos} (GeV/c)");
	hNegPtClone->GetXaxis()->SetTitle("p_{T} (GeV/c)");
	hNegPtClone->SetAxisRange(hXLow, hXHigh, "X");
	hNegPtClone->SetAxisRange(0, 100, "X");
//	gPad->SetLogy(0);


	/************* graphInvYield Axis Properties **************/
	double gTitleOffset = 1.6;
	const char *gYAxisTitle;
	gYAxisTitle = "#frac{1}{N_{evt}}#frac{d^{2}N}{p_{t}dp_{T}}";

	// Drawing on to the gPads
	graphPosInvYield->GetYaxis()->SetRangeUser(gYLow, gYHigh);
//	graphPosInvYield->GetYaxis()->SetLimits(1e-11, 1e+1);
//	graphPosInvYield->GetXaxis()->SetRangeUser(gXLow, gXHigh);
	graphPosInvYield->GetYaxis()->SetTitle(gYAxisTitle);
	graphPosInvYield->GetYaxis()->SetTitleOffset(gTitleOffset);
	graphPosInvYield->GetYaxis()->CenterTitle(true);
	graphPosInvYield->GetXaxis()->SetTitle("p_{T}(GeV/c)");

	graphNegInvYield->GetYaxis()->SetRangeUser(gYLow, gYHigh);
	graphNegInvYield->GetXaxis()->SetRangeUser(gXLow, gXHigh);
	graphNegInvYield->GetYaxis()->SetTitleOffset(gTitleOffset);
	graphNegInvYield->GetYaxis()->CenterTitle(true);
	graphNegInvYield->GetYaxis()->SetTitle(gYAxisTitle);
	graphNegInvYield->GetXaxis()->SetTitle("p_{T}(GeV/c)");

	graphNegByPosInvYield->GetYaxis()->SetRangeUser(0, 3);
	graphNegByPosInvYield->GetXaxis()->SetRangeUser(gXLow, gXHigh);
	graphNegByPosInvYield->GetYaxis()->SetTitle("Inv Yield Neg By Pos");
	graphNegByPosInvYield->GetYaxis()->CenterTitle(true);
	graphNegByPosInvYield->GetYaxis()->SetTitleOffset(gTitleOffset);
	graphNegByPosInvYield->GetXaxis()->SetTitle("p_{T}(GeV/c)");

	/********************************/
	/********** Cosmetics **********/
	/* graph Inv Yield Raw Cosmetics */
	graphCosmetics(graphPosInvYield, 20, kOrange+10, 0.7);
	graphCosmetics(graphNegInvYield, 28, kBlue+4, 0.7);
	graphCosmetics(graphNegByPosInvYield, 28, kYellow+4, 0.7);

	/* graph Inv Yield Rebinned Cosmetics */
	graphCosmetics(gVarPosInvYield, 20, kOrange+10, 0.7);
	graphCosmetics(gVarNegInvYield, 28, kBlue+4, 0.7);
	graphCosmetics(gVarInvYieldRatio, 28, kYellow+4, 0.7);



	/**************** Multiplexing onto the canvases **************/

	/* TGraphs, Invariant Yield */

	cInvYield->cd(1); gVarPosInvYield->Draw("ACP");
	cInvYield->cd(2); gVarNegInvYield->Draw("ACP");
	cInvYield->cd(3); gVarInvYieldRatio->Draw("ACP");

	/* TGraphs, Invariant Yield Variable Binned */
	cVarInvYield->cd(1);
	hPosPt->SetMarkerStyle(20);
	hPosPt->SetMarkerSize(1.2);
//		hPosPt->Draw("P");
		hPosPt->Draw();
	hVarPosPt->SetMarkerStyle(20);
	hVarPosPt->SetMarkerSize(1.2);
	hVarPosPt->SetMarkerColor(kRed);
		hVarPosPt->Draw("h p same");

	cVarInvYield->cd(2);
		hNegPt->Draw();
	hVarNegPt->SetMarkerStyle(20);
	hVarNegPt->SetMarkerSize(1.2);
	hVarNegPt->SetMarkerColor(kRed);
		hVarNegPt->Draw("h p same");

	cVarInvYield->cd(3);
		gPad->SetLogy(0);
		gPad->SetLogx(0);
		hNegPtClone->Draw();
	hVarNegPtClone->SetMarkerStyle(20);
	hVarNegPtClone->SetMarkerSize(1.2);
	hVarNegPtClone->SetMarkerColor(kRed);
		hVarNegPtClone->Draw("h p same");

	/* Histograms from data */
//	cRaw->cd(1); hPosPt->Draw();
//	cRaw->cd(2); hNegPt->Draw();
//	cRaw->cd(3); hNegPtClone->Draw();
//
	

	cRaw->SetTitle("GraphPosInvYield");
	cVarInvYield->SetTitle("Variable Binned Invariant Yield");
	cInvYield->SetTitle("Normalized Events based Invariant Yield, default uniform binning");

	cRaw->cd(1); graphPosInvYield->Draw("ACP");
	cRaw->cd(2); graphNegInvYield->Draw("ACP");
	cRaw->cd(3); 
	gPad->SetLogy(0);
	gPad->SetLogx(0);
	graphNegByPosInvYield->Draw("ACP");

	// Label the histograms and graphs before they go into the output root file 
	hPosPt->SetName("hPosPt");
	hNegPt->SetName("hPosPt");
	hNegPtClone->SetName("hNegByPosPt");

	
	hVarPosPt->SetName("hVarBinPosPt");
	hVarNegPt->SetName("hVarBinPosPt");
	hVarNegPtClone->SetName("hVarBinNegByPosPt");


	gVarPosInvYield->SetName("gVarPosInvYield");
	gVarNegInvYield->SetName("gVarNegInvYield");
	gVarInvYieldRatio->SetName("gVarInvYieldRatio");


	graphPosInvYield->SetName("gPosInvYield");
	graphNegInvYield->SetName("gNegInvYield");
	graphNegByPosInvYield->SetName("gNegByPosInvYield");

	// Writing the name to the file 
	graphPosInvYield->Write("gPosInvYield");
	graphNegInvYield->Write("gNegInvYield");
	graphNegByPosInvYield->Write("gNegByPosInvYield");


	gVarPosInvYield->Write("gVarPosInvYield");
	gVarNegInvYield->Write("gVarNegInvYield");
	gVarInvYieldRatio->Write("gVarInvYieldRatio");



	// Writing Histograms and TGraphs to an output file
	TFile *outputFile = new TFile(outputFileName, "recreate");
	TObjArray hList(0);

	// Adding hist and graphs to the output file
	// Adding the hist/tgraph from input
	hList.Add(hPosPt);
	hList.Add(hNegPt);
	hList.Add(hNegPtClone);
	hList.Add(graphPosInvYield);
	hList.Add(graphNegInvYield);
	hList.Add(graphNegByPosInvYield);

	// Adding the variable binned hist/tgraph calculated
	hList.Add(hVarPosPt);
	hList.Add(hVarNegPt);
	hList.Add(hVarNegPtClone);

	hList.Add(gVarPosInvYield);
	hList.Add(gVarNegInvYield);
	hList.Add(gVarInvYieldRatio);

	hList.Write();

	outputFile->Close();

	// Close the input file
//	cout << "Closing the input file: " << inFile << endl;

}//close-main
