# April 20, 2018

This folder contains a set of root macros, sh, root files. The root files are based on pythia simulations and they are already processed. 
`negBypos.C` is the main macro that compares the data vs mc, neg/pos ratios. It includes results from EPS09, EPPS16, CT14nlo, and CT10nlo. All possibilities are included i.e. bb, ff, bf, b:bound, f:free proton. 

`pTHatCombination` folder was necessary because it contains the required macros that does the *pT hat combination* from the simulated results. It is highly automated and not worth breaking it apart.

`PbPb2015_MinBias.root` this file is obtained from MinBias collisions, based on the single trigger used. The running of the jobs was very smooth. For HardProbes dataset, things are not that smooth. It requires breaking apart into File, instead of Lumi inside the crab script. 

## To Do:
* After the HardProbes root file is available, it is necessary to do the spectra combination for PbPb and compare with bound+bound collision system. Macro needs to be writen for this purpose. 

# April 24, 2018
`spectraCombine.C`: The spectra combination is probably not needed, because, triggered spectra is populated only when the trigger is actually applied, with the necessary pT cut included by default. If, the complete spectra is populated without the pT cut, then it is likely, such a spectra combination is to be imposed. 


# April 29, 2018
`rmBinRange.C`: This is a test code to check, if the histogram bins are cleared only for a chosen range. It is an improvement and generalization for the function *removePoints* in the macro `spectraCombin.C`, as required for doing the spectra summation.

`PbPb2016_MinBias-1.root`: In this file, the pseudorapidity coverage of the leading charged particle spectra is restrained to |eta| < 1


# April 30, 2018
`testSpectraCombine.C`: Checking if the normalization factors are correctly obtained, i.e. pulled from leading track spectra and applied to track spectra.

# May 1, 2018

`spectraCombin.C`: Spectra combined histograms for pos, neg, neg/pos after variable binning and writing the result to an external root file is completed. Additionally, `drawHist.C` macro is invoked in this, the idea is to separate the drawing of the histograms. The code for drawing clutters around and the functionality is often not easy to see. 

`drawHist.C`: Requires to be implemented. 

`negBypos4.C`: Is a clone from fully functional `negBypos3.C`, the idea is to separate the information from the code i.e. PbPb is now available as a separate file, which is enough i.e. no variable binning is necessary for just PbPb in the same code. 

### Update-1
`negByPos4.C`: Writes a root file `negByPos4.root` file, which has the final result as a canvas object, and the individual histograms for pp, pPb, PbPb, and the bb,bf,ff histograms. Note that, the pPb:Neg/Pos is constructed from data points.

`negByPos5.C`: Is a clone of negByPos4.C. The information from the file *pPbNegByPos.root* obtained from the RpPb result was plotted, instead of what was calculated from arrays as it was done in the original RpPb. The macros used to generate the root file is located at */home/bokadia/CMS/CMSHIResearch/Pythia8Macros/CMSHIResearch/ThesisPlots/RpPb/RpPb/v7.C*
