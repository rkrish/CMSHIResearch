
void test(){

	/*
	TFile *f1, *f2;
	f1 = new TFile("Spectra_April29_ppMinus_trk.root");
	f2  = new TFile("Spectra_April29_ppPlus_trk.root");


	TH1D *hp, *hn, *hd;
	//hp = (TH1D*)f1->Get("ppTrackSpectrum");
	//hn = (TH1D*)f2->Get("ppTrackSpectrum");
	hp = (TH1D*)f1->Get("RpPb");
	hn = (TH1D*)f2->Get("RpPb");

	hd = (TH1D*)hn->Clone();
	//hd->Divide(hp);
	hp->Draw("same");
	hn->Draw("same");
	

	TFile *f3;
	f3 = new TFile("output.root");
	hn = (TH1D*)f3->Get("h_RpPbpt_TrackTriggered_JustForNeg_ForRatio");
	hp = (TH1D*)f3->Get("h_RpPbpt_TrackTriggered_JustForPos_ForRatio");

	hd = (TH1D*)hn->Clone();
	hd->Draw("same");
	*/

	TFile *f;
	f = new TFile("PbPb2016_MinBias.root");


	TH1D *hp, *hn, *hd;
	hp = (TH1D*)f->Get("RPbPb/hpTPosMinBias");
	hn = (TH1D*)f->Get("RPbPb/hpTNegMinBias");

	hd = (TH1D*)hn->Clone();
	hd->Divide(hp);
	//hp->Draw("same");
	hd->Draw("same");
	

	
}
