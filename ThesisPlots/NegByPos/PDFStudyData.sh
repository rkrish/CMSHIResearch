#!/bin/bash
fileS1=(
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_10To20_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_20To30_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_30To50_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_50To80_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_80To120_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_120To170_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_170To230_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_230To300_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_300To380_2017-06-30-155936.root\",\
\"FreeProton_CT14nlo_2017-06-30-155936/FreeProton_CT14nlo_380To10000_2017-06-30-155936.root\")

Val1=(
39.043,\
2.7443,\
0.62149,\
0.073569,\
0.0087706,\
0.00122304,\
0.00020056,\
3.7851e-05,\
7.942e-06,\
2.3642e-06 )

nFiles=1
