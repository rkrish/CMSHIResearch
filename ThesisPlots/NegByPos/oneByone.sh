#!/bin/bash

message(){

	echo "=============================================================="
	echo "This script is used to merge the spectra of different pT Hats \
	      and extract the positive and negative charged particle spectra \
	      and calculating the ratio of negative to positive spectra"
	echo "=============================================================="
	echo ; echo ;
}

message

funcA(){
mainFolder=(\
BoundAndFreeProton_EPPS16_CT14nlo_2017-06-10-212303 \
BoundAndFreeProton_EPS09_CT10nlo_2017-06-10-212241 \
BoundProton_EPPS16_CT14nlo_2017-06-10-214359 \
BoundProton_EPS09_CT10nlo_2017-06-10-214807 \
FreeProton_CT10nlo_2017-06-10-214302 \
FreeProton_CT14nlo_2017-06-10-214324 \
)
}

echo ${mainFolder[@]}

#funcB(){
mainFolder=(BoundProton_EPPS16_CT14nlo_2017-06-30-155959)
mainFolder=(FreeProton_CT14nlo_2017-06-30-155936)
#}


# If there's a file with *Combined.root inside the sub-sub folder
# delete it, because the contents inside it may also be hadded

delCombinedIfExists(){
	for mf in ${mainFolder[@]}
	do
		mainFolder=${mf}

		# Deleting the files in the main folder
		rm ${mf}/*Combined*.root

	done
}




# Fetch the folder from ACCRE
getFromACCRE(){
	for mf in ${mainFolder[@]}
	do
		scp -r janjamrk@login.accre.vanderbilt.edu:/home/janjamrk/CMSSW_7_4_0/src/CMSHIResearch/Pythia8Analysis/test/Pythia8Study_v8/$mf .
	done
}


#For Bound and Free Proton PDF Merging pt hats * process sw
pTHatMerge(){
# Loop over (free,bound)
for mf in ${mainFolder[@]}
do
	mainFolder=${mf}
	rootFiles=`ls ${mf}/*.root`

	echo ${rootFiles}
	echo ; echo ;

	./getData.sh -m ${mf} > PDFStudyData.sh
	./processAllOutputs.sh ${mf}

done
}

# Copy the Combined files into the current directory
copyToCurrDir(){
	for mf in ${mainFolder[@]}
	do
		cp ${mf}/*Combined*.root .
	done
}

#getFromACCRE
delCombinedIfExists
pTHatMerge
#copyToCurrDir

# Clear up the compiled junk
#~/removeJunk.sh

# Call this finally to remove the combined stuff
