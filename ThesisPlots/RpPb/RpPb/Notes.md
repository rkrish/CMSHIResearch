# November 20, 2017
-

RpPb_EPPS16_CT14nlo.root : This file is from `/home/bokadia/CMS/CMSHIResearch/Pythia8Macros/CMSHIResearch/RpPb20112017`


## May 2, 2018
`plotRpPb_v7.C`: The neg to pos ratio is calculated via the RpPbNeg/RpPbPos, since the denominators is the average of pos and neg for pp. A small piece of code is added, which writes to a file named *otemp.root* to export the Neg/Pos data for final thesis plot. 
