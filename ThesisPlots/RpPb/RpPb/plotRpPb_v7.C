
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <TFile.h>
#include <TH1D.h>
#include <TKey.h>

using namespace std;

typedef const char* cchar;
typedef map<string, TH1D*> map1D;
typedef map<string, map1D> map2D;
typedef map<string, map2D> map3D;
typedef map<string, map<string, TFile*>> mfile2D;


void Info(){

	cout << "\n\n";
	cout << "====================================================\n";
	cout << "To compare free and bound proton PDF spectra" << endl; 
	cout << "====================================================\n";
	cout << "\n\n";

	cout << "-------------------- FINALIZED ------------------- " << endl;
}


/* Normalizing Variable Binned Histograms */
TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist



/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics

void map3DLabels(map3D hTrack){

	for(map3D::iterator it = hTrack.begin(); it != hTrack.end(); ++it){
		for(map2D::iterator it1 = (&(it->second))->begin(); it1 != (&(it->second))->end(); ++it1){
			for (map<string, TH1D*>::iterator it2 = (&(it1->second))->begin(); it2 != (&(it1->second))->end(); ++it2){
				cout << "[\"" << it->first << "\"]" ;
				cout << "[\"" << it1->first << "\"]" ;
				cout << "[\"" << it2->first<<"\"]";
				cout << endl;
				}
			}
	}

}//close:map3DLabels

void writeToFile(map3D mh){

	TFile *outfile = new TFile("Output_RpPb.root", "recreate");
	outfile->cd();
	for(map3D::iterator it = mh.begin(); it != mh.end(); ++it){
		for(map2D::iterator it1 = (&(it->second))->begin(); it1 != (&(it->second))->end(); ++it1){
			for (map<string, TH1D*>::iterator it2 = (&(it1->second))->begin(); it2 != (&(it1->second))->end(); ++it2){
				cout << "[\"" << it->first << "\"]" ;
				cout << "[\"" << it1->first << "\"]" ;
				cout << "[\"" << it2->first<<"\"]";
				cout << endl;

				cchar label = Form("%s_%s", it->first.c_str(), it1->first.c_str());
				mh[it->first][it1->first][it2->first]->Write(label);
				}
			}
	}
				mh["RpPb"]["Data"]["Pos"]->Write("RpPb_Data_Pos");
				mh["RpPb"]["Data"]["Neg"]->Write("RpPb_Data_Neg");
				mh["RpPb"]["Data"]["All"]->Write("RpPb_Data_All");
				outfile->Close();

}//close:writeToFile

/* Bin and Scale */

void binAndScale(TH1D *h){
	
	//return h;
}//close-binAndScale


/* Divide Histograms */
TH1D* divideHistograms(TH1D *hNum, TH1D *hDenom, TH1D *hRatio){

	cout << "from dividehistograms" << endl;
	cout << "hnum " << hNum->GetSize() << endl;
	cout << "hdenom " << hDenom->GetSize() << endl;
	//cout << "hratio " << hRatio->GetName() << endl;
	
	if (hNum->GetSize() == hDenom->GetSize()){
		hRatio = (TH1D*)hNum->Clone();
		hRatio->Divide(hDenom);
	}

	return hRatio;
}//close-divideHistograms

void plotRpPb_v7(){

	TCanvas *c1 = new TCanvas("c1", "", 900, 460);
	c1->cd();
	double RpPb[] = { 0.5946,0.6211,0.6552,0.6984,0.7219,0.7515,0.7809,0.825,0.866,0.901,0.925,0.965,0.984,1.023,1.052,1.056,1.048,1.054,1.031,1.023,1.036,1.054,1.072,1.142,1.189,1.259,1.308,1.342,1.382,1.407,1.363,1.381,1.316 };

	double pT[] = { 0.55,0.6499999999999999,0.75,0.8500000000000001,0.95,1.05,1.15,1.2999999999999998,1.5,1.7000000000000002,1.9,2.1,2.3,2.8,3.6,4.4,5.199999999999999,6.0,6.800000000000001,8.4,10.8,13.2,16.8,21.6,26.4,32.0,38.400000000000006,44.8,54.4,67.19999999999999,80.0,95.0,112.19999999999999};

	double pTLow[] ={0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.4,1.6,1.8,2.0,2.2,2.4,3.2,4.0,4.8,5.6,6.4,7.2,9.6,12.0,14.4,19.2,24.0,28.8,35.2,41.6,48.0,60.8,73.6,86.4,103.6};

	double pTHigh[] = {0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.4,1.6,1.8,2.0,2.2,2.4,3.2,4.0,4.8,5.6,6.4,7.2,9.6,12.0,14.4,19.2,24.0,28.8,35.2,41.6,48.0,60.8,73.6,86.4,103.6,120.8};


	double posByTotal[] ={
		0,0,1.00013,1.00137,1.00309,1.00325,1.00254,1.00316,1.00333,1.00348,1.00314,1.00226,1.00308,1.00307,1.00229,1.00267,1.00413,1.00426,1.00126,1.0065,1.00778,1.00358,1.00991,1.01691,1.0128,1.01828,1.02419,1.03022,1.03813,1.04602,1.04732,1.06715,1.08251,1.08136,1.07902,0,0,0,0,0,0};

	double posByTotalError[] = {
		0,0,0.0064462,0.00644186,0.00645573,0.00648653,0.00646008,0.00646723,0.00647648,0.00461955,0.00463204,0.00463437,0.00466025,0.00471088,0.00475173,0.00257442,0.00286868,0.00351342,0.00461212,0.00622359,0.00836949,0.00773639,0.0148693,0.0235338,0.00255656,0.0040545,0.00283015,0.00358881,0.00477947,0.0072462,0.0084989,0.0157359,0.026572,0.0374659,0.0610866,0,0,0,0,0,0
	};


	double binCenters[] = {
-2.24615,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.3,1.5,1.7,1.9,2.1,2.3,2.8,3.6,4.4,5.2,6,6.8,8.4,10.8,13.2,16.8,21.6,26.4,32,38.4,44.8,54.4,67.2,80,95,112.2,129.4,146.6,163.8,181,198.2,209.446
	};

   	Double_t xAxis[] = {0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 3.2, 4, 4.8, 5.6, 6.4, 7.2, 9.6, 12, 14.4, 19.2, 24, 28.8, 35.2, 41.6, 48, 60.8, 73.6, 86.4, 103.6, 120.8, 138, 155.2, 172.4, 189.6, 206.8}; 

  	double negByTotal[] = {
   0,0,0.999868,0.998643,0.99691,0.996755,0.997482,0.99683,0.996663,0.996517,0.996871,0.997733,0.997001,0.996993,0.997719,0.997349,0.995812,0.995637,0.998689,0.993349,0.99335,0.995258,0.991959,0.989592,0.987262,0.982146,0.978278,0.972487,0.96632,0.958424,0.957125,0.9373,0.921934,0.923089,0.925431,0,0,0,0,0,0};

  	double negByTotalError[] = {
	   0,0,0.00644448,0.00642436,0.00641602,0.00644465,0.00642755,0.00642652,0.00643358,0.00458764,0.00460324,0.00461363,0.00463241,0.00468285,0.00473055,0.00256135,0.00284735,0.00348879,0.00460327,0.0061645,0.0082869,0.0076943,0.0146942,0.023101,0.00251027,0.00395538,0.00273983,0.00344884,0.00455157,0.00683131,0.00799765,0.0144062,0.0238773,0.033686,0.0551587,0,0,0,0,0,0};

 	double RpPbAllCharged[] = {
	 0, 0, 0.592333, 0.618923, 0.652903, 0.695847, 0.71971, 0.74941, 0.778412, 0.822826, 0.863904, 0.897675, 0.922188, 0.961605, 0.981496, 1.02038, 1.05156, 1.05699, 1.0553, 1.06117, 1.04446, 1.04622, 1.05284, 1.05329, 1.07992, 1.14839, 1.19581, 1.25037, 1.30723, 1.33301, 1.36376, 1.38281, 1.37096, 1.35855, 1.31669, 0, 0, 0, 0, 0, 0
 	};

	double RpPbAllChargedError[] = {
		0, 0, 0.00269632, 0.00281135, 0.00296611, 0.00317461, 0.0032708, 0.00340554, 0.00353942, 0.00266517, 0.00280057, 0.00290509, 0.00298613, 0.00313049, 0.00320158, 0.00176435, 0.00186224, 0.0020095, 0.00229445, 0.00278222, 0.00342438, 0.0030122, 0.00559788, 0.00864685, 0.00132764, 0.00207564, 0.00161071, 0.00201306, 0.00262234, 0.00397115, 0.00471411, 0.00871959, 0.0144036, 0.0201591, 0.0323614, 0, 0, 0, 0, 0, 0
	};


	TLegend *leg = new TLegend(0.9, 0.6, 0.7, 0.9);
	const char *opt = "same hist p9";
	opt = "e1 same";

 	 /* Constructing the histograms for RpPb positive and negative */
  	 TH1D *hPosByTotal = new TH1D("h_RpPbpt_TrackTriggered_JustForPos_ForRatio","hPartPt_0_14_minbias_trkCorr_trigCorr",39, xAxis);
  	 TH1D *hNegByTotal = new TH1D("h_RpPbpt_TrackTriggered_JustForNeg_ForRatio","hPartPt_0_14_minbias_trkCorr_trigCorr",39, xAxis);

  	 TH1D *hRpPbPos = new TH1D("hPosRpPb" ,"hPos RpPb",39, xAxis);
  	 TH1D *hRpPbNeg = new TH1D("hNegRpPb" ,"hNeg RpPb",39, xAxis);
  	 TH1D *hRpPbAllCharged = new TH1D("h_RpPb", "RpPb", 39, xAxis);


	 // Populating the values into RpPb
	 for (int tBin=0; tBin < 39; tBin++){

		 hPosByTotal->SetBinContent(tBin, posByTotal[tBin]);
		 hPosByTotal->SetBinError(tBin, posByTotalError[tBin]);

		 hNegByTotal->SetBinContent(tBin, negByTotal[tBin]);
		 hNegByTotal->SetBinError(tBin, negByTotalError[tBin]);

		 hRpPbAllCharged->SetBinContent(tBin, RpPbAllCharged[tBin]);
		 hRpPbAllCharged->SetBinError(tBin, RpPbAllChargedError[tBin]);

	 }//close:for
 

	 /* RpPb, positive & negative */
	 hRpPbPos->Multiply(hPosByTotal, hRpPbAllCharged, 1, 1);
	 hRpPbNeg->Multiply(hNegByTotal, hRpPbAllCharged, 1, 1);

	 applyCosmetics(hRpPbPos, kRed, 20, 2);
	 applyCosmetics(hRpPbNeg, kBlue, 20, 2);
	 applyCosmetics(hRpPbAllCharged, kBlack, 20, 2);

	// EPPS16_CT14nlo MC RpPb
	map<string, TFile*> mfile;
	map<string, TH1D*> mh1;
	mfile["EPPS16_CT14nlo"] = new TFile("RpPb_EPPS16_CT14nlo.root");
	mh1["RpPbPos_EPPS16_CT14nlo"] = (TH1D*)mfile["EPPS16_CT14nlo"]->Get("RpPbPos_EPPS16_CT14nlo");
	mh1["RpPbNeg_EPPS16_CT14nlo"] = (TH1D*)mfile["EPPS16_CT14nlo"]->Get("RpPbNeg_EPPS16_CT14nlo");
	mh1["RpPbAll_EPPS16_CT14nlo"] = (TH1D*)mfile["EPPS16_CT14nlo"]->Get("RpPbAll_EPPS16_CT14nlo");


	applyCosmetics(mh1["RpPbPos_EPPS16_CT14nlo"], kRed, 22, 2);
	applyCosmetics(mh1["RpPbNeg_EPPS16_CT14nlo"], kBlue, 22, 2);
	applyCosmetics(mh1["RpPbAll_EPPS16_CT14nlo"], kBlack, 22, 2);


	mh1["RpPbPos_EPPS16_CT14nlo"]->Draw(opt);
	mh1["RpPbNeg_EPPS16_CT14nlo"]->Draw(opt);
	mh1["RpPbAll_EPPS16_CT14nlo"]->Draw(opt);



	 if(1)
	 {
		 leg->AddEntry(hRpPbPos, "Pos_Data", "lep");
		 leg->AddEntry(hRpPbNeg, "Neg_Data", "lep");
		 leg->AddEntry(hRpPbAllCharged, "All_Data", "lep");

		 hRpPbPos->Draw(opt);
		 hRpPbNeg->Draw(opt);
		 hRpPbAllCharged->Draw(opt);

	 	 hRpPbNeg->SetStats(0);
		 hRpPbPos->SetStats(0);
		 hRpPbAllCharged->SetStats(0);
		 //gStyle->SetErrorX(0.001);
		 gPad->SetLogx();
		 gPad->SetGrid();

	 }


	 if(1){
		 TCanvas *c2 = new TCanvas("c2", "", 900, 460);

		 TH1D *hNegByPos = (TH1D*) hRpPbNeg->Clone();
		 hNegByPos = divideHistograms(hRpPbNeg, hRpPbPos, hNegByPos);
		 hNegByPos->Draw();

		 TFile *of = new TFile("otemp.root", "recreate");
		 hNegByPos->Write("hpPbNegByPos");
		 of->Close();


	 }

}//close:main

