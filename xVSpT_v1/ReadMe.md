## 6 June, 2018

[HardQCDAll, HardQCDQuark, HardQCDGluon]: directories
Each directory has pTHatCombined_CollisionType.root files
Plotting: x{1,2} vs pT{+,-} hadrons per Collision Type

`procResults.sh`: Script that has the combined pT hat files based on type of collision. This information is passed to `xVSpT_dev.C`

`xVSpT_dev.C`: i/p, [Path to processType, label, outputfile], o/p, written to same directory.  Unpacks the root files per directory, generates TProfile based on process switch and writes the output to the same directory for that specific histograms.
Eg: 
	o/p fileName: HardQCDQuark_BoundAndFreeProton_EPPS16_CT14nlo.root
	i/p folders: HardQCDAll
