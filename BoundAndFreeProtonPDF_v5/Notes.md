### April 10, 2017
---

#### ``` oneByone_v2.sh ```:
This script has information related to pulling up a folder from ACCRE, combining pT Hats per folder, there's lot of nesting folder and stuff in it. But basically, it does pTHat combination, ```Pythia8SpectraStandalone_v8.C```, pulls the required information i.e. required histograms and applies cosmetics and does variable binning and writes to an output file that has the file name ```Processed-1-pdfs*root```, this is the file that is finally pulled out into another macro and applied additional cosmetics to generate the plots. 

### April 11, 2017
---

#### ``` Pythia8SpectraStandalone_v8.C ```:
No issues, it very well normalizes the histograms comparing with v7, which doesn't normalize the histograms. Note that the process of normalization requires the number of events to be hardcoded into the macro. If the histogram already contains the number of events, then its better it could be extracted and used without hard coding it.  This macro should be used with ```oneByone_v2.sh```


#### ``` oneByone_v2.sh ```:
[Bug] Some bug with this file, when the array is initalized, it processes everything, but doesn't copy the first object in the array. 
Not quite using this, the idea is to normalize the histograms with the total number of events. However, there's no output in the end. Requires some debugging. Normalization may be required for plots that go into the thesis. So, should keep an eye on this to be done and keep it in the to do list. This macro should be used with ```oneByone_v2.sh```

### May 07, 2017
Made some crucial changes to the file ```getData_v1.sh```, the switch -m has the required updated, note that some more changes are pending, where the variable blah needs to be given externally as before, this will ensure there's a cross-check with what's inside the Cross-Section.txt file and the info given outside or perhaps match the date. 

```oneByOne_v3.sh``` also needs to be modified, only the function *pTHatMerge()*, because of the reason cited in the para before.

### May 08, 2017
```partonSpectraByType.C```, this macro plots the spectra of u, d quarks produced in a Pythia 8 based collision, where bound pdf is used for one beam and free pdf is used for another. There's also the same type of pdf used for both beams. It gives the plots for negative/positive for bound, free, bound and free pdf based collisions as a function of u, d quarks. Gluons, Photons or heavier quarks are available in the root files, it just needs to pull in by the macro for plotting. It will also take the ratio of bound/bound, free/free, boundAndFree/boundAndFree i.e. basically the same type of pdf, but comparing the yield of negative quark type of positive quark type. In the previous case, it was bound by free, now it is neg/pos for *quark type*. A lot of enhancements are required in this macro, these results are not stellar, they are expected and they look similar to the hadron ratios, when a process switch is compared.

At the time of writing this macro, the information to process the macro is contained in these folders:
BoundAndFreeProton_EPPS16_CT14nlo_2017-05-06-215546
BoundAndFreeProton_EPS09_CT10nlo_2017-05-06-215524
BoundProton_EPPS16_CT14nlo_2017-05-06-215748
BoundProton_EPS09_CT10nlo_2017-05-06-215811
FreeProton_CT10nlo_2017-05-06-215702
FreeProton_CT14nlo_2017-05-06-215725

The use of combined pt hat root files is made for this macro. It should be noted that some processing macros were modified inorder to accomplish this i.e. ```oneByOne_v3.sh```, further modifications may be necessary.



### Core
---
**C07052017-01** : Write a new method inside ```oneByOne_v3.sh``` to extract the parton level histograms and calculate their ratio.

### Feature
---
**F08052017-01** : Based on folder name boundAndFree, get inside and pick up the *Combined.root file automatically, instead of putting by hand
