

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
37.698	  3.58504e-05	  1.8849	  0.00137363	  BoundAndFreeProton_EPPS16_CT14nlo_10To20_2017-05-06-215546
0.00125389	  3.62033e-14	  6.26945e-05	  4.36513e-08	  BoundAndFreeProton_EPPS16_CT14nlo_120To170_2017-05-06-215546
0.00020327	  8.92636e-16	  1.01635e-05	  6.85426e-09	  BoundAndFreeProton_EPPS16_CT14nlo_170To230_2017-05-06-215546
2.7327	  1.90877e-07	  0.136635	  0.00010023	  BoundAndFreeProton_EPPS16_CT14nlo_20To30_2017-05-06-215546
3.7553e-05	  3.08513e-17	  1.87765e-06	  1.27427e-09	  BoundAndFreeProton_EPPS16_CT14nlo_230To300_2017-05-06-215546
7.6317e-06	  1.2477e-18	  3.81585e-07	  2.56259e-10	  BoundAndFreeProton_EPPS16_CT14nlo_300To380_2017-05-06-215546
0.62859	  1.00944e-08	  0.0314295	  2.30496e-05	  BoundAndFreeProton_EPPS16_CT14nlo_30To50_2017-05-06-215546
2.1494e-06	  1.1583e-19	  1.0747e-07	  7.8079e-11	  BoundAndFreeProton_EPPS16_CT14nlo_380To10000_2017-05-06-215546
0.075402	  1.34748e-10	  0.0037701	  2.66308e-06	  BoundAndFreeProton_EPPS16_CT14nlo_50To80_2017-05-06-215546
0.0090299	  1.89174e-12	  0.000451495	  3.1554e-07	  BoundAndFreeProton_EPPS16_CT14nlo_80To120_2017-05-06-215546
