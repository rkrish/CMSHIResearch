#!/bin/bash

fileExt=( pcm so d )
echo "Do you want to delete?"
echo "The following extensions will be deleted" ${fileExt[@]}
echo "============================="
echo "Deleting the files:"

for i in ${fileExt[@]}
do 
	ls -l *.$i
	rm *.$i
done
