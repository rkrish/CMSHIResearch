#include <iostream>
#include <fstream>
#include <string>

#include <TPRegexp.h>
#include <TString.h>

using namespace std;


typedef const char* cchar;

/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics

void displayInfo(cchar obj){


  //if (obj == "cat") cout << "its a cat ! " << endl;

  string line;
  ifstream myfile ("infoData.txt");
  if (myfile.is_open())
  {
    while ( getline (myfile, line) )
    {

      //cout << line << '\n';

      TString s0(line);
      TPRegexp r0("^([0-9]+):(\\s.*)");

      //cout << s0.EqualTo(obj) << "\t" << r0.Match(s0) << endl;

      if(r0.Match(s0)){
	//cout << "Yes, match found !" << endl;
	// take the matched pattern and replace the number with nothing
      	//cout << ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString() << endl;
	//cout << "Matched value: " << ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString().EqualTo(obj) << endl;
	//if(s0.CompareTo(obj)){
        if(((TObjString*)(r0.MatchS(s0)->At(1)))->GetString().EqualTo(obj)){
	cout << "\n\n\n";
	cout << "===============================\n";
	cout << "Information on what's plotted:\n";
	cout << "===============================\n";

      	cout << ((TObjString*)(r0.MatchS(s0)->At(2)))->GetString() << endl;
	cout << "\n\n\n";
	}
	//cout << s0.CompareTo(obj) << endl;
      }

    }
    myfile.close();
  }

  else cout << "Unable to open file"; 

}//close:displayInfo

class proc{

	public:
		proc(){}
		proc(cchar fName1, cchar fName2, cchar obj);

		proc(cchar fName, cchar, TH1D*);
		proc(cchar, cchar, cchar, int);
		bool calcRatio(TH1D*, TH1D*);
		TH1D *hRatio, *hSum;
		TH1D* normalizeVarBin(TH1D*, double);
		TH1D* addh1(TH1D*, TH1D*);

	private:
		TFile *file, *file1, *file2;
		TH1D *hNum, *hDenom, *hVarNum, *hVarDenom;
		map<string, TH1D*> mh1;
		int opt;

};

/* Calculate fName1/[fName2(Pos+Negl)/2], TH1D is calculate by invoking
 * a different constructor  */
proc::proc(cchar fName, cchar obj, TH1D* hIn){


	Double_t xAxis[] = {0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 3.2, 4, 4.8, 5.6, 
		            6.4, 7.2, 9.6, 12, 14.4, 19.2, 24, 28.8, 35.2, 41.6, 48, 60.8, 73.6, 86.4, 103.6, 120.8}; 

	
	int nBins_xAxis = sizeof(xAxis)/sizeof(Double_t)-1;

	file = new TFile(fName);
	mh1[obj] = (TH1D*)file->Get(obj);

	cchar label;
	label = Form("%s_varBin", obj);

	mh1[label] = (TH1D*)mh1[obj]->Rebin(nBins_xAxis, label, xAxis);
	mh1[label] = normalizeVarBin(mh1[label], nBins_xAxis);

	calcRatio(mh1[label], hIn);

}//close


TH1D* addh1(TH1D* h1, TH1D* h2){

	hSum = new TH1D();
	return hSum;
}//close:

/* To process on different objects from same file */
proc::proc(cchar fName, cchar obj1, cchar obj2, int opt){


	Double_t xAxis[] = {0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 3.2, 4, 4.8, 5.6, 
		            6.4, 7.2, 9.6, 12, 14.4, 19.2, 24, 28.8, 35.2, 41.6, 48, 60.8, 73.6, 86.4, 103.6, 120.8}; 

	int nBins_xAxis = sizeof(xAxis)/sizeof(Double_t)-1;

	switch (opt){
	case 1:
		cout << "Inside a different constructor: " << endl;

		file = new TFile(fName);	

		mh1[obj1] = (TH1D*)file->Get(obj1);
		mh1[obj2] = (TH1D*)file->Get(obj2);


		cchar label1, label2;

		label1 = Form("%s_varBin", obj1);
		cout << label1 << endl;
		mh1[label1] = (TH1D*)mh1[obj1]->Rebin(nBins_xAxis, label1, xAxis);
		mh1[label1] = normalizeVarBin(mh1[label1], nBins_xAxis);

		label2 = Form("%s_varBin", obj2);
		mh1[label2] = (TH1D*)mh1[obj2]->Rebin(nBins_xAxis, label2, xAxis);
		mh1[label2] = normalizeVarBin(mh1[label2], nBins_xAxis);

		calcRatio(mh1[label1], mh1[label2]);
	break;


	/* Calculate the (pos+neg)*0.5 */
	case 2:

		cout << "calculating the sum, not-normalized bins" << endl;

		file = new TFile(fName);	

		mh1[obj1] = (TH1D*)file->Get(obj1);
		mh1[obj2] = (TH1D*)file->Get(obj2);

		mh1["sum"] = (TH1D*)mh1[obj1]->Clone("forSum");
		mh1["sum"]->Add(mh1[obj2]);

		cchar label;
		mh1["sum"] = (TH1D*)mh1["sum"]->Rebin(nBins_xAxis, label, xAxis);
		mh1["sum"] = normalizeVarBin(mh1["sum"], nBins_xAxis);
		mh1["sum"]->Scale(0.5);

		hSum = mh1["sum"];
		hSum->SetName(Form("[%s+%s]*0.5", obj1, obj2));

	break;
	}//close:switch


}//close:


/* To process on same object from different files */
proc::proc(cchar fName1, cchar fName2, cchar obj){

   	Double_t xAxis[] = {0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 3.2, 4, 4.8, 5.6, 
		            6.4, 7.2, 9.6, 12, 14.4, 19.2, 24, 28.8, 35.2, 41.6, 48, 60.8, 73.6, 86.4, 103.6, 120.8}; 

        int nBins_xAxis = sizeof(xAxis)/sizeof(Double_t)-1;

	file1 = new TFile(fName1);
	file2 = new TFile(fName2);

	hNum = (TH1D*)file1->Get(obj);
	hDenom = (TH1D*)file2->Get(obj);

	hVarNum = (TH1D*)hNum->Rebin(nBins_xAxis, "h1Norm", xAxis);
	hVarDenom = (TH1D*)hDenom->Rebin(nBins_xAxis, "h2Denom", xAxis);

	/* Apply the condition initial histogram bin size >> nBins_xAxis */

	if (nBins_xAxis < hNum->GetSize() && nBins_xAxis < hDenom->GetSize()){
		hVarNum = normalizeVarBin(hVarNum, nBins_xAxis);
		hVarDenom = normalizeVarBin(hVarDenom, nBins_xAxis);

		calcRatio(hVarNum, hVarDenom);
	}

}//close

bool proc::calcRatio(TH1D* hNum, TH1D* hDenom){

	cout << "from dividehistograms" << endl;
	cout << "hnum " << hNum->GetSize() << endl;
	cout << "hdenom " << hDenom->GetSize() << endl;
	//cout << "hratio " << hRatio->GetName() << endl;
	
	if (hNum->GetSize() == hDenom->GetSize()){
		hRatio = (TH1D*)hNum->Clone();
		hRatio->Divide(hDenom);
		hRatio->SetStats(0);
		return 1;
	}

	else {
		cout << "the numerator and denominators dont match or doesn't have same bin size" << endl;
		return 0;
	}


}//close:calcRatio


/* Normalizing Variable Binned Histograms */
TH1D* proc::normalizeVarBin (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		lbinWidth = hVarPt->GetBinWidth(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		cout << "hVar (binContent, binError) : "  << "\t"
		     << lbinContent << "\t"
		     << lbinError << "\t" 
		     << lbinWidth << "\t" 
		     << "\n";

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist


/* Main Program */
void RpPb(){


	TLegend *lg = new TLegend(0.9, 0.8, 0.6, 0.6);
	TH1D *hR;
	map<string, map<string, cchar>> mPath;

	/* RpPb : Bound/Free [PDFSpecXSec, FreePDFXSec]*/
        mPath["bf"]["PDFSpecificXSec"] = "Files_20-09-2017-123100/PDFSpecificXSec/pTSum_BoundAndFreeProton_EPPS16_CT14nlo_2017-09-11-142702.root";
	mPath["ff"]["PDFSpecificXSec"] = "Files_20-09-2017-123100/PDFSpecificXSec/pTSum_FreeProton_CT14nlo_2017-09-08-131815.root";

        mPath["bf"]["FreePDFXSec"] = "Files_20-09-2017-123100/FreePDFXSec/pTSum_BoundAndFreeProton_EPPS16_CT14nlo_2017-09-11-142702.root";
	mPath["ff"]["FreePDFXSec"] = "Files_20-09-2017-123100/FreePDFXSec/pTSum_FreeProton_CT14nlo_2017-09-08-131815.root";


	proc *pObj;

	int optn;
	optn = 3;
	cin >> optn ;
	switch (optn){
	
		case 1:{

			pObj = new proc(mPath["bf"]["PDFSpecificXSec"], mPath["bf"]["FreePDFXSec"], "hPos");
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Pos:#sigma_{PDFSpecific}/#sigma_{FreePDF}  ", "lep");
			applyCosmetics(hR, kRed, 20, 2);
			hR->SetDirectory(0);
			hR->Draw("same");

			pObj = new proc(mPath["bf"]["PDFSpecificXSec"], mPath["bf"]["FreePDFXSec"], "hNeg");
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Neg:#sigma_{PDFSpecific}/#sigma_{FreePDF}  ", "lep");
			applyCosmetics(hR, kBlue, 20, 1);
			hR->Draw("same");

			lg->Draw("same");

			displayInfo("301");
		}
		break;

		case 2:{

			/* Neg/Pos (PDFSpecXSec, FreePDFXSec)*/
			pObj = new proc(mPath["bf"]["PDFSpecificXSec"], "hNeg", "hPos", 1);;
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Neg/Pos [pPb:EPPS16+CT14nlo]:#sigma_{PDFSpecific}  ", "lep");
			applyCosmetics(pObj->hRatio, kRed, 20, 2);
			hR->Draw("same");


			pObj = new proc(mPath["bf"]["FreePDFXSec"], "hNeg", "hPos", 1);;
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Neg/Pos [pp:CT14nlo]:#sigma_{FreePDF}  ", "lep");
			applyCosmetics(pObj->hRatio, kBlue, 20, 1);
			hR->Draw("same");
			lg->Draw("same");


		}break;


		case 3 :{

			/* Neg/Pos (PDFSpecXSec, FreePDFXSec)*/
			pObj = new proc(mPath["ff"]["PDFSpecificXSec"], "hNeg", "hPos", 1);;
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Neg/Pos [pPb:EPPS16+CT14nlo]:#sigma_{PDFSpecific}  ", "lep");
			applyCosmetics(pObj->hRatio, kRed, 20, 2);
			hR->Draw("same");


			pObj = new proc(mPath["ff"]["FreePDFXSec"], "hNeg", "hPos", 1);;
			hR = pObj->hRatio;
			lg->AddEntry(hR, "Neg/Pos [pp:CT14nlo]:#sigma_{FreePDF}  ", "lep");
			applyCosmetics(pObj->hRatio, kBlue, 20, 1);
			hR->Draw("same");
			lg->Draw();

		}break;

		case 4 : {


			// Calculate the sum : (Pos+Neg)*0.5
			proc *pObjSum = new proc(mPath["ff"]["PDFSpecificXSec"], "hNeg", "hPos", 2);;

			// Take the ratio for Pos/2*(Pos+Neg)
			proc *pObjPos = new proc(mPath["bf"]["PDFSpecificXSec"], "hPos", pObjSum->hSum);;
			applyCosmetics(pObjPos->hRatio, kRed, 20, 2);
			lg->AddEntry(pObjPos->hRatio, "2*Pos/(Pos+Neg):#sigma_{PDFSpecific}", "lep");
			pObjPos->hRatio->Draw("e1 same");

			// Take the ratio for Neg/2*(Pos+Neg)
			proc *pObjNeg = new proc(mPath["bf"]["PDFSpecificXSec"], "hNeg", pObjSum->hSum);;
			hR = pObjNeg->hRatio;
			applyCosmetics(hR, kBlue, 20, 2);
			lg->AddEntry(hR, "2*Neg/(Pos+Neg):#sigma_{PDFSpecific}", "lep");
			hR->Draw("e1 same");


			/* For all the charged particles */
			proc *pObjSum_ff = new proc(mPath["ff"]["PDFSpecificXSec"], "hNeg", "hPos", 2);;
			proc *pObj = new proc();
			proc *pObjSum_bf  = new proc(mPath["bf"]["PDFSpecificXSec"], "hNeg", "hPos", 2);;
			pObj->calcRatio(pObjSum_bf->hSum, pObjSum_ff->hSum);
			applyCosmetics(pObj->hRatio, kBlack, 20, 2);
			lg->AddEntry(pObj->hRatio ,"2*All/(Pos+Neg):#sigma_{FreePDF}", "lep");
			pObj->hRatio->Draw("e1 same");

			lg->Draw("same");

			gPad->SetGrid();
			gPad->SetLogx();

			displayInfo("303");

		}
		break;


		case 5 : {

			// Calculate the sum : (Pos+Neg)*0.5
			proc *pObjSum = new proc(mPath["ff"]["FreePDFXSec"], "hNeg", "hPos", 2);;

			// Take the ratio for Pos/2*(Pos+Neg)
			proc *pObjPos = new proc(mPath["bf"]["FreePDFXSec"], "hPos", pObjSum->hSum);;
			applyCosmetics(pObjPos->hRatio, kRed, 20, 2);
			lg->AddEntry(pObjPos->hRatio, "2*Pos/(Pos+Neg):#sigma_{FreePDF}", "lep");
			pObjPos->hRatio->Draw("e1 same");
			pObjPos->hRatio->SetDirectory(0);

			// Take the ratio for Neg/2*(Pos+Neg)
			proc *pObjNeg = new proc(mPath["bf"]["FreePDFXSec"], "hNeg", pObjSum->hSum);;
			hR = pObjNeg->hRatio;
			applyCosmetics(hR, kBlue, 20, 2);
			lg->AddEntry(hR, "2*Neg/(Pos+Neg):#sigma_{FreePDF}", "lep");
			hR->Draw("e1 same");
			hR->SetDirectory(0);


			/* For all the charged particles */
			proc *pObjSum_ff = new proc(mPath["ff"]["FreePDFXSec"], "hNeg", "hPos", 2);;
			proc *pObj = new proc();
			proc *pObjSum_bf  = new proc(mPath["bf"]["FreePDFXSec"], "hNeg", "hPos", 2);;
			pObj->calcRatio(pObjSum_bf->hSum, pObjSum_ff->hSum);
			applyCosmetics(pObj->hRatio, kBlack, 20, 2);
			lg->AddEntry(pObj->hRatio ,"2*All/(Pos+Neg):#sigma_{FreePDF}", "lep");
			pObj->hRatio->Draw("e1 same");


			lg->Draw("same");

			gPad->SetGrid();
			gPad->SetLogx();

			displayInfo("302");

		}//close:case
		break;


		case 6 : {

			// Calculate the sum : (Pos+Neg)*0.5
			map<string, proc*> pObj;
			pObj["Sum_FreePDFXSec"] = new proc(mPath["ff"]["FreePDFXSec"], "hNeg", "hPos", 2);;
			pObj["Sum_PDFSpecificXSec"] = new proc(mPath["ff"]["PDFSpecificXSec"], "hNeg", "hPos", 2);;

			applyCosmetics(pObj["Sum_FreePDFXSec"]->hSum, kBlue, 20, 1);
			applyCosmetics(pObj["Sum_PDFSpecificXSec"]->hSum, kRed, 20, 2);

			pObj["Sum_PDFSpecificXSec"]->hSum->Draw("e1 same");
			pObj["Sum_FreePDFXSec"]->hSum->Draw("e1 same");


			gPad->SetLogy();
			gPad->SetGrid();

		}//close:case
		break;


		case 7:{


			map<string, proc*> pObjSum, pObjAll/*RpPb*/, pObjPos/*RpPb*/, pObjNeg/*RpPb*/;
			proc *pObj = new proc();

			cchar label; 

			label = "PDFSpecificXSec";

			// Calculate the sum : (Pos+Neg)*0.5
			pObjSum[label] = new proc(mPath["ff"][label], "hNeg", "hPos", 2);;


			// Take the ratio for Pos/0.5*(Pos+Neg)
			pObjPos[label] = new proc(mPath["bf"][label], "hPos", pObjSum[label]->hSum);;

			// Take the ratio for Neg/0.5*(Pos+Neg)
			pObjNeg[label] = new proc(mPath["bf"][label], "hNeg", pObjSum[label]->hSum);;



			label = "FreePDFXSec";

			// Calculate the sum : (Pos+Neg)*0.5
			pObjSum[label] = new proc(mPath["ff"][label], "hNeg", "hPos", 2);;

			// Take the ratio for Pos/0.5*(Pos+Neg)
			pObjPos[label] = new proc(mPath["bf"][label], "hPos", pObjSum[label]->hSum);;

			// Take the ratio for Neg/0.5*(Pos+Neg)
			pObjNeg[label] = new proc(mPath["bf"][label], "hNeg", pObjSum[label]->hSum);;


			/*
			label = "PDFSpecificXSec";
			pObjSum[label] = new proc(mPath["bf"][label], "hNeg", "hPos", 2);;
			pObj->calcRatio(pObjSum["PDFSpecificXSec"]->hSum, pObjSum["FreePDFXSec"]->hSum);
			pObj->hRatio->Draw("same");
			*/


			// Ratio of RpPb(+) = RpPb (PDFSpecific)/ RpPb (FreePDF)
			pObj->calcRatio(pObjPos["PDFSpecificXSec"]->hRatio, pObjPos["FreePDFXSec"]->hRatio);
			lg->AddEntry(pObj->hRatio, "RpPb(+):#sigma_{PDFSpecific}/#sigma_{FreePDF}", "lep");
			applyCosmetics(pObj->hRatio, kRed, 20, 2);
			pObj->hRatio->Draw("e1 same");


			// Ratio of RpPb(-) = RpPb (PDFSpecific)/ RpPb (FreePDF)
			pObj->calcRatio(pObjNeg["PDFSpecificXSec"]->hRatio, pObjNeg["FreePDFXSec"]->hRatio);
			lg->AddEntry(pObj->hRatio, "RpPb(-):#sigma_{PDFSpecific}/#sigma_{FreePDF}", "lep");
			applyCosmetics(pObj->hRatio, kBlue, 20, 1);
			pObj->hRatio->Draw("e1 same");

			TLine *line = new TLine(-3, 0, 8, 0);
			line->Draw("same");

			lg->Draw("same");

			gPad->SetLogy(0);
			gPad->SetGrid();

			displayInfo("500");

		}//close:case
	        break;

		default:
		break;


	}//close:switch


	//displayInfo("392");
	
	
}//close:main
