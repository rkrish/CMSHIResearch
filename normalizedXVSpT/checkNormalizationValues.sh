#!/bin/bash

f1=`ls -d Files_20-09-2017-123100/PDFSpecificXSec/*`
f2=`ls -d Files_20-09-2017-123100/FreePDFXSec/*`

rm checkRes.txt
oFile=checkRes.txt
for d in $f1
do

	echo $d >> $oFile
	dirL=`ls -d $d/* | sort -V `
	stat=`echo $?`
	if [[ "$stat" -eq "0" ]]
	then
		for file in $dirL; do
			root -b -q -l "xSecCheckTitle.C+(\"$file\")" | grep "^[0-9]" >> $oFile
		done
		echo ; echo ; 
		echo "===============================================================================" >> $oFile
	fi
	echo ; echo ;
done

rm *.d *.pcm *.so
