#!/bin/bash

if [[ 1 == 0 ]]
then

genResult(){

	pTHat=$1
	folder=$2
	fileName=`ls ${folder}/*${pTHat}*.root`
	csValue=`cat ${folder}/Cross-Section.txt | grep ${pTHat} | awk '{print $3}'`
	echo $csValue, $fileName

	pTLimits=(`echo $1 | sed -n 's/\(.*\)To\(.*\)/\1 \2/p'`)
	echo "pTLimits: " ${pTLimits[*]}
	lowPt=${pTLimits[0]}; highPt=${pTLimits[1]}
	root -b -q "myMacro.C+(\"$fileName\", \"${csValue}\", ${lowPt}, ${highPt})"
}

procRes(){

	folder=$1

	# Check if the folder is already present and delete it
	ls -d Result_$folder/
	if [[ $? == 0 ]];then rm -rf Result_$folder; fi

	# Create the folder
	mkdir Result_$folder
	searchString=`sed -n 's/\(.*\)_\([0-9-]\+\)/Result_\1*\2.root/p' <<< $folder`
	resStr=`sed -n 's/\(.*\)_\([0-9-]\+\)/Result_*\2.root/p' <<< $folder`
	ls -l $searchString; echo ; echo ;
	hadd -f Result_pTHatCombined_${folder}.root $searchString

	# Move the generated results into the folder
	mv $resStr Result_$folder

}

pTList=( 10To20 20To30 30To50 50To80 80To120 120To170 170To230 230To300 300To380 380To10000 )
#pTList=(10To20)

folder=BoundAndFreeProton_EPS09_CT10nlo_2017-08-10-124507
folder=FreeProton_CT10nlo_2017-08-10-124550
folder=BoundAndFreeProton_EPPS16_CT14nlo_2017-08-10-124529
folder=FreeProton_CT14nlo_2017-08-10-124613
for pTHat in ${pTList[@]}
do
	genResult $pTHat $folder
	echo ;
done

procRes $folder
fi


folder=BoundAndFreeProton_EPPS16_CT14nlo_2017-08-10-124529
pTHat=10To20
pTHat=20To30
pTHat=30To50
pTHat=50To80
pTHat=80To120
pTHat=120To170
pTHat=170To230
pTHat=230To300
pTHat=300To380
pTHat=380To10000
pTHat=10To20
pTHat=20To30
Q2Limits=`echo $1 | sed -n 's/\(.*\)To\(.*\)/\1, \2/p' <<< $pTHat`
m1=`sed -n "s/\(.*\)_\([0-9-]\+\)/Result_\1_${pTHat}_\2.root/p" <<< $folder`
filePath=Result_$folder/$m1
echo "filePath: " $filePath
#root -l "showResults.C+(\"$filePath\", $Q2Limits)"

# pTHat Combined
echo ; echo ;
filePath=Result_$folder/Result_pTHatCombined_$folder.root
echo "filePath: " $filePath
root -l "showResults.C+(\"$filePath\", 10, 10000)"


f1=FreeProton_CT10nlo_2017-08-10-124550
f2=BoundAndFreeProton_EPS09_CT10nlo_2017-08-10-124507
f3=BoundAndFreeProton_EPPS16_CT14nlo_2017-08-10-124529
f4=FreeProton_CT14nlo_2017-08-10-124613

pTHat=20To30
m1=`sed -n "s/\(.*\)_\([0-9-]\+\)/Result_\1_${pTHat}_\2/p" <<< $f1`
m2=`sed -n "s/\(.*\)_\([0-9-]\+\)/Result_\1_${pTHat}_\2/p" <<< $f2`
m3=`sed -n "s/\(.*\)_\([0-9-]\+\)/Result_\1_${pTHat}_\2/p" <<< $f3`
m4=`sed -n "s/\(.*\)_\([0-9-]\+\)/Result_\1_${pTHat}_\2/p" <<< $f4`
echo $m1

legLabel="pTHatCombined"

funcb(){
root -l "perPtHatAllCollisionSystems.C+({\
	\"Result_$f1/Result_pTHatCombined_$f1.root\", \
	\"Result_$f2/Result_pTHatCombined_$f2.root\", \
	\"Result_$f3/Result_pTHatCombined_$f3.root\", \
	\"Result_$f4/Result_pTHatCombined_$f4.root\", \
	}, \"${legLabel}\")"


root -l "perPtHatAllCollisionSystems.C+({\
	\"Result_$f1/$m1.root\", \
	\"Result_$f2/$m2.root\", \
	\"Result_$f3/$m3.root\", \
	}, \"${legLabel}\")"


root -l "perPtHatAllCollisionSystems.C+({\
	\"Result_$f1/$m1.root\", \
	}, \"${legLabel}\")"

}


rm *.so; rm *.pcm; rm *.d
