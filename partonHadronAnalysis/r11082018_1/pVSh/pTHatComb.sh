#!/bin/bash

getFiles(){

	path=/home/janjamrk/CMSSW_7_4_0/src/CMSHIResearch/Pythia8Analysis/test/Pythia8Study_v8

	scp -r janjamrk@login7.accre.vanderbilt.edu:${path}/HardQCDAll .
	scp -r janjamrk@login7.accre.vanderbilt.edu:${path}/HardQCDQuark .
	scp -r janjamrk@login7.accre.vanderbilt.edu:${path}/HardQCDGluon .

}

getFiles

haddAllpTHats(){

	# HardQCDAll, HardQCDGluon, HardQCDQuark
	ifolder=$1

	rm ${ifolder}/pTHatCombined_*.root

	lf=`ls $ifolder`
	for folder in ${lf[@]}
	do
		echo "folder "$folder

		path=$ifolder/$folder	
		echo "path" $path
		echo ;
		hadd -f ${ifolder}/pTHatCombined_${folder}.root ${path}/*.root
		#echo $folder | sed 's/Output_//g; s/[0-9]\+To[0-9]\+_//g'
	done
}

#if [[ "$1" == "2" ]];then
haddAllpTHats HardQCDAll
haddAllpTHats HardQCDGluon
haddAllpTHats HardQCDQuark
#fi
