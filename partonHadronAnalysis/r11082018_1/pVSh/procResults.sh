#!/bin/bash

# extract date
edate(){

	file=`ls $1`
	echo ${file} | sed -n 's/.*_\(2018.*\).root/\1/p'

}

proc1(){

	# HardQCDAll
	# ----------
	date=`edate HardQCDAll/pTHatCombined_BoundAndFreeProton*.root`
	fpath=HardQCDAll/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDAll.root
	tag=hqa_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDAll/pTHatCombined_BoundProton*.root`
	fpath=HardQCDAll/pTHatCombined_BoundProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDAll.root
	tag=hqa_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDAll/pTHatCombined_FreeProton*.root`
	fpath=HardQCDAll/pTHatCombined_FreeProton_CT14nlo_${date}.root
	ofname=Result_HardQCDAll.root
	tag=hqa_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"



	# HardQCDQuark
	# ------------
	date=`edate HardQCDQuark/pTHatCombined_BoundAndFreeProton*.root`
	fpath=HardQCDQuark/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDQuark/pTHatCombined_BoundProton*.root`
	fpath=HardQCDQuark/pTHatCombined_BoundProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDQuark/pTHatCombined_FreeProton*.root`
	fpath=HardQCDQuark/pTHatCombined_FreeProton_CT14nlo_${date}.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"



	# HardQCDGluon
	# ------------
	date=`edate HardQCDGluon/pTHatCombined_BoundAndFreeProton*.root`
	fpath=HardQCDGluon/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDGluon/pTHatCombined_BoundProton*.root`
	fpath=HardQCDGluon/pTHatCombined_BoundProton_EPPS16_CT14nlo_${date}.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	date=`edate HardQCDGluon/pTHatCombined_FreeProton*.root`
	fpath=HardQCDGluon/pTHatCombined_FreeProton_CT14nlo_${date}.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

}

if [[ "$1" == 1 ]]; then  proc1; fi


# x-axis:partons, y-axis:hadrons
proc2(){

	label=hVSp
	nfName=Result_HardQCDAll_${label}.root
	dfName=Result_HardQCDAll_${label}.root
	num=h2_un_ph_hqa_bfn
	den=h2_un_nh_hqa_bfn
	xaxis="pT-partons (GeV/c)"

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=hadronsVSpartons.root

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\", \"${xaxis}\")"

}

if [[ "$1" == 2 ]]; then  proc2; fi


# x-axis:partons, y-axis:hadrons
proc21(){

	label=hVSp
	nfName=Result_${3}_${label}.root
	dfName=Result_${3}_${label}.root
	num=$1
	den=$2

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=hadronsVSpartons.root
	xaxis="pT-partons (GeV/c)"

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\", \"${xaxis}\")"


}

execproc21(){

	opts=( un up dn dp g upt dpt )
	lpdf=( bfn bbn ffn )
	for pdf in ${lpdf[@]}
	do

		ctype=HardQCDAll
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqa_${pdf}
			den=h2_${pp}_nh_hqa_${pdf}
			proc21 ${num} ${den} ${ctype}
		done

		ctype=HardQCDQuark
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqq_${pdf}
			den=h2_${pp}_nh_hqq_${pdf}
			proc21 ${num} ${den} ${ctype}
		done


		ctype=HardQCDGluon
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqg_${pdf}
			den=h2_${pp}_nh_hqg_${pdf}
			proc21 ${num} ${den} ${ctype}
		done
	done

}

if [[ "$1" == 21 ]]; then  execproc21; fi


# x-axis:hadrons, y-axis:partons
proc31(){

	label=pVSh
	nfName=Result_${3}_${label}.root
	dfName=Result_${3}_${label}.root
	num=$1
	den=$2
	xaxis="pT-hadrons (GeV/c)"

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=partonsVShadrons.root

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\", \"${xaxis}\")"

}


execproc31(){

	opts=( un up dn dp g upt dpt )
	lpdf=( bfn bbn ffn )
	for pdf in ${lpdf[@]}
	do
		ctype=HardQCDAll
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqa_${pdf}
			den=h2_${pp}_nh_hqa_${pdf}
			proc31 ${num} ${den} ${ctype}
		done

		ctype=HardQCDQuark
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqq_${pdf}
			den=h2_${pp}_nh_hqq_${pdf}
			proc31 ${num} ${den} ${ctype}
		done


		ctype=HardQCDGluon
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqg_${pdf}
			den=h2_${pp}_nh_hqg_${pdf}
			proc31 ${num} ${den} ${ctype}
		done
	done

}


if [[ "$1" == 31 ]]; then  execproc31; fi



# x-axis:hadrons, y-axis:partons
proc3(){

	label=pVSh
	nfName=Result_HardQCDQuark_${label}.root
	dfName=Result_HardQCDAll_${label}.root
	num=h2_upt_nh_hqq_bfn
	den=h2_upt_nh_hqa_bfn
	xaxis="pT-hadrons (GeV/c)"

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=partonsVShadrons.root


	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\", \"${xaxis}\")"

}

if [[ "$1" == 3 ]]; then  proc3; fi

addLabel(){

	# options to use:hVSp, pVSh, x-axis: partons will have range 0-500, hadrons will have range 0-120
	label=$1 
	cp Result_HardQCDQuark.root Result_HardQCDQuark_${label}.root
	cp Result_HardQCDGluon.root Result_HardQCDGluon_${label}.root
	cp Result_HardQCDAll.root Result_HardQCDAll_${label}.root

}

if [[ "$1" == "a" ]]; then  addLabel $2; fi

h2Draw(){

	hqa=`ls HardQCDAll/pTHatCombined*`
	hqq=`ls HardQCDQuark/pTHatCombined*`
	hqg=`ls HardQCDGluon/pTHatCombined*`
	for fpath in ${hqa[@]} ${hqq[@]} ${hqg[@]} 
	do
		label=`echo ${fpath} | sed 's/\//_/g' | sed 's/pTHatCombined\|[0-9-]\|.root//g; s/_$//g' `
		echo $label, $fpath
		root -b -q "h2partonsVShadrons.C+(\"${fpath}\", \"${label}\")"
	done
}

if [[ "$1" == 4 ]]; then  h2Draw; fi


# calculate the ratios of the projected histograms
proc4(){

	rm ratios_outfile.root

	s1="h2_dpt_nh_hqa_bfn"
	s2="h2_g_nh_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_dpt_ph_hqa_bfn"
	s2="h2_g_ph_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_ph_hqa_bfn"
	s2="h2_g_ph_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_nh_hqa_bfn"
	s2="h2_g_nh_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_nh_hqa_bfn"
	s2="h2_dpt_nh_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_ph_hqa_bfn"
	s2="h2_dpt_ph_hqa_bfn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_ph_hqa_bbn"
	s2="h2_dpt_ph_hqa_bbn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_nh_hqa_bbn"
	s2="h2_dpt_nh_hqa_bbn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"


	s1="h2_upt_ph_hqa_ffn"
	s2="h2_dpt_ph_hqa_ffn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

	s1="h2_upt_nh_hqa_ffn"
	s2="h2_dpt_nh_hqa_ffn"
	root -b -q "h1Ratios.C+(\"${s1}\", \"${s2}\")"

}
proc4

if [[ "$1" == "m" ]]; then

	echo ;
	echo "-----------------------------------------------------------"
	echo "1: project the 2d histograms for HardQCD{Quark, Gluon, All}"
	echo "21: ratio of ph to nh per collision per pdf set"
	echo "a: to add label $0 a hVSp"
	echo "-----------------------------------------------------------"
	echo ;

fi

rm *.pcm *.so *.d &> /dev/null
