
## July 28, 2018

prj3.C, procResults.sh, check2DHist.C, calcRatio.C
Everything is done from procResults.sh, just provide options as given in the script, some are in batch mode. 

prj3.C: writes to a image directory

## July 25, 2018

prj3.C : script that converts the TH2D to TProfile and doing variable binning
procResults.sh : run this script to get things done
Sample root file from HardQCDAll is provided

is an updated version from prj2.C


## July 25, 2018

prj1.C : prototype code for partons pT and hadrons pT projection into a profile histogram and applying variable binning. 

## July 24, 2018

prj.C : Projecting 2D histograms

doSum.C: Profile histograms, 2D histograms are generated and written to the file test.root

sample output_numEvent1000.root, is required and needs to be changed in the macros
