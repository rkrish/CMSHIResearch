#!/bin/bash

# extract date
f1(){

	#HardQCDAll/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-08-08-212444.root
	n1=pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-08-08-212444.root
	mbf=BoundAndFreeProton		
	mm=`sed 's/BoundAndFreeProton/1/g' <<< ${mbf}`
	echo $mm
	if [[ ${mm} == 1 ]];then echo "yes";fi
	n1=pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-08-08-212444.root
	echo $n1 | grep "BoundAndFreeProton" | sed -n 's/.*_\(2018.*\).root/\1/p'

}
#f1


# extract date
f2(){
	#file=`ls HardQCDAll/pTHatCombined_BoundAndFreeProton*.root`
	file=`ls $1*.root`
	echo $file | grep "BoundAndFreeProton" | sed -n 's/.*_\(2018.*\).root/\1/p'
}
#f2 HardQCDAll/pTHatCombined_BoundAndFreeProton
