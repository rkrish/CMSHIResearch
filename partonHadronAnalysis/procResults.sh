#!/bin/bash

proc1(){

	# HardQCDAll
	# ----------
	fpath=HardQCDAll/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-07-25-103724.root
	ofname=Result_HardQCDAll.root
	tag=hqa_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDAll/pTHatCombined_BoundProton_EPPS16_CT14nlo_2018-07-25-104903.root
	ofname=Result_HardQCDAll.root
	tag=hqa_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDAll/pTHatCombined_FreeProton_CT14nlo_2018-07-25-104425.root
	ofname=Result_HardQCDAll.root
	tag=hqa_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"



	# HardQCDQuark
	# ------------
	fpath=HardQCDQuark/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-07-25-123136.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDQuark/pTHatCombined_BoundProton_EPPS16_CT14nlo_2018-07-25-123304.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDQuark/pTHatCombined_FreeProton_CT14nlo_2018-07-25-123241.root
	ofname=Result_HardQCDQuark.root
	tag=hqq_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"



	# HardQCDGluon
	# ------------
	fpath=HardQCDGluon/pTHatCombined_BoundAndFreeProton_EPPS16_CT14nlo_2018-07-25-112641.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_bfn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDGluon/pTHatCombined_BoundProton_EPPS16_CT14nlo_2018-07-25-112820.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_bbn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

	fpath=HardQCDGluon/pTHatCombined_FreeProton_CT14nlo_2018-07-25-112703.root
	ofname=Result_HardQCDGluon.root
	tag=hqg_ffn
	root -b -q "prj5.C+(\"${fpath}\", \"${ofname}\", \"${tag}\")"

}

if [[ "$1" == 1 ]]; then  proc1; fi


# x-axis:partons, y-axis:hadrons
proc2(){

	label=hVSp
	nfName=Result_HardQCDAll_${label}.root
	dfName=Result_HardQCDAll_${label}.root
	num=h2_un_ph_hqa_bfn
	den=h2_un_nh_hqa_bfn

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=hadronsVSpartons.root

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\")"

}

if [[ "$1" == 2 ]]; then  proc2; fi


# x-axis:partons, y-axis:hadrons
proc21(){

	label=hVSp
	nfName=Result_${3}_${label}.root
	dfName=Result_${3}_${label}.root
	num=$1
	den=$2

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=hadronsVSpartons.root

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\")"

}

execproc21(){

	opts=( un up dn dp g upt dpt )
	for ctype in HardQCDAll HardQCDQuark HardQCDAll
	do
		for pp in ${opts[@]}
		do
			num=h2_${pp}_ph_hqa_bfn
			den=h2_${pp}_nh_hqa_bfn
			proc21 ${num} ${den} ${ctype}
		done
	done


}
if [[ "$1" == 21 ]]; then  execproc21; fi


# x-axis:hadrons, y-axis:partons
proc3(){

	label=pVSh
	nfName=Result_HardQCDQuark_${label}.root
	dfName=Result_HardQCDAll_${label}.root
	num=h2_up_ph_hqq_bfn
	den=h2_up_ph_hqa_bfn

	snum=`sed 's/h2_//g' <<< ${num}`
	sden=`sed 's/h2_//g' <<< ${den}`
	oName=${label}_${snum}_BY_${sden}
	wf=1 # 1: write to file, 0: don't write to file
	ofName=partonsVShadrons.root

	root -b -q "calcRatio.C+(\"${nfName}\", \"${dfName}\", \"${num}\", \"${den}\", \"${oName}\", ${wf}, \"${ofName}\")"

}

if [[ "$1" == 3 ]]; then  proc3; fi

addLabel(){

	# options to use:hVSp, pVSh, x-axis: partons will have range 0-500, hadrons will have range 0-120
	label=$1 
	cp Result_HardQCDQuark.root Result_HardQCDQuark_${label}.root
	cp Result_HardQCDGluon.root Result_HardQCDGluon_${label}.root
	cp Result_HardQCDAll.root Result_HardQCDAll_${label}.root

}

#if [[ "$1" == 4 ]]; then  addLabel $2; fi


rm *.pcm *.so *.d
