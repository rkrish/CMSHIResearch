#!/bin/bash


haddAllpTHats(){

	# HardQCDAll, HardQCDGluon, HardQCDQuark
	ifolder=$1

	rm ${ifolder}/pTHatCombined_*.root

	lf=`ls $ifolder`
	for folder in ${lf[@]}
	do
		echo "folder "$folder

		path=$ifolder/$folder	
		echo "path" $path
		echo ;
		hadd -f ${ifolder}/pTHatCombined_${folder}.root ${path}/*.root
		#echo $folder | sed 's/Output_//g; s/[0-9]\+To[0-9]\+_//g'
	done
}

# HardQCDGluon HardQCDAll HardQCDQuark
haddAllpTHats HardQCDAll
haddAllpTHats HardQCDGluon
haddAllpTHats HardQCDQuark
