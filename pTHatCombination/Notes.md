### June 14, 2017

The guiOptions.C [buttons, etc] -> forGUIPerPDFSet.C [processing]. Now the way this is set up is that the canvas object belongs to the main_macro, which is ok, works for now. It keeps getting recreated whenever the macro is called. A better way is to have the TCanvas object from the **guiOptions.C**, and initialize it and delete it whenever the button is clicked. 

```perPDFSet.C : ```
This is the main macro, it runs in standalone mode. Just pass the files, cross-section values and plots them with cosmetics applied, [before normalization, after normalization, combined, Neg/Pos ] added.


```perPDFSet_v5.C : ```
Writing the output of pthats, xsec*pthats, pos, neg, all into a separate file sans application of cosmetics

```perPDFSet_v6.C : ```
Application of cosmetics moved into the for loop, its an optimization



### June 29, 2017
```plotPtHatPerPDF.C```
i/p -> root files[+, -, all][before/after xsec weighted] after hadding from perPDFSet_v5.C
o/p -> Bound/Free[+, - ][before/after xsec weighted], per pTHat
perPDFSet_v5, is just weighting the cross-sections, nothing more to it. This macro is going to divide those processed histograms before and after cross-section weighting.


### June 30, 2017
perPtHatRatios.C
The purpose of this macro is to plot the ratio of bound/free per pTHat, in the end we have 4 histograms per pT Hat, bound/free, [EPS09, EPPS16, EPS09_CT10nlo, EPPS16_CT14nlo], one can choose the type of histograms to take the ratio.
It requires the input files to be in the parent directory, 4 root files

### July 5, 2017
## perPtHatRatios.C
A small change, since pp, ppbar have also to be plotted, this macro will do the job, but the inputs come from the shell script `choosePDFSet.sh`

### July 6, 2016
## perPtHatRatios_v1.C
A clone of the file perPtHatRatios.C, just to preserve it.

## comparePPandPPbar.C
This macro is used to compare the different collisions systems, it requires the results from forDiffCollisionType.C, this is just a copy of the standalone program, perPtHatRatios.C, basically reads the chosen histogram given a single collision system. The information is just organized this way, if not it becomes a mess. The first macro picks up the histograms from the second macro, otherwise, this macro doesn't have any purpose per se. 

f1:comparePPandPPbar, f2:forDiffCollisionType
f2->(bNb{2 types}, fnf{2 types}, bNf{2 types})->b/f [specify (pos, neg, neg/pos)]
f2(histName, "some string", pp, ppbar, pbarp)<-f1
