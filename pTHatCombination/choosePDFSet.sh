#!/bin/bash

works2(){

dirPath=FreeProton_CT10nlo_2017-06-10-214302
dirPath=BoundProton_EPS09_CT10nlo_2017-06-10-214807
dirPath=FreeProton_CT14nlo_2017-06-10-214324
dirPath=BoundAndFreeProton_EPS09_CT10nlo_2017-06-10-212241
dirPath=BoundProton_EPPS16_CT14nlo_2017-06-10-214359
dirPath=BoundAndFreeProton_EPPS16_CT14nlo_2017-06-10-212303

# Read the Cross-Section.txt for csValue, fileName and populate into a bash variable;
infiles=`cat ${dirPath}/Cross-Section.txt | grep [0-9].* | awk -v var="${dirPath}" '{printf "\n\"%s/%s.root\",", var, $5}' | tr "\n" " " | sed 's/,$/}/g' | sed 's/^/{/g'`
echo ; echo ;
csValues=`cat ${dirPath}/Cross-Section.txt | grep [0-9].* | awk '{printf "\n%s,", $3}' | tr "\n" " " | sed 's/,$/}/g' | sed 's/^/{/g'`
dirName=`sed -n 's/\(.*\)_[0-9-]\+/\1/p' <<< ${dirPath}`
#echo ${dirName}
echo ;

#echo $infiles
#root -b -q "perPDFSet.C+(${infiles}, ${csValues})";
root -l "perPDFSet.C+(${infiles}, ${csValues})";
}

#works2

# **** Generate the outputfile based for pos, neg, before and after xsec weighting **** #
works3(){

	dirPathList=( \
	FreeProton_CT10nlo_2017-06-10-214302 \
	BoundProton_EPS09_CT10nlo_2017-06-10-214807 \
	FreeProton_CT14nlo_2017-06-10-214324 \
	BoundAndFreeProton_EPPS16_CT14nlo_2017-06-10-212303 \
	BoundAndFreeProton_EPS09_CT10nlo_2017-06-10-212241 \
	BoundProton_EPPS16_CT14nlo_2017-06-10-214359 \
	)


	dirPathList=( \
		FreeAndBoundProton_EPS09_CT10nlo_2017-07-05-155029 \
		FreeAndBoundProton_EPPS16_CT14nlo_2017-07-05-155132 \
		FreeCT14AndFreeCT10_2017-07-05-161623 \
		FreeCT10AndFreeCT14_2017-07-05-161648 \
	)


	dirPathList=( \

		BoundAndFreeProton_EPPS16_CT14nlo_2017-06-30-155850 \
		BoundAndFreeProton_EPS09_CT10nlo_2017-06-30-155825 \
		BoundProton_EPPS16_CT14nlo_2017-06-30-155959 \
		BoundProton_EPS09_CT10nlo_2017-06-30-160022 \
		FreeProton_CT10nlo_2017-06-30-155912 \
		FreeProton_CT14nlo_2017-06-10-214324 \
	)



	for dirPath in ${dirPathList[@]}
	do

	# Read the Cross-Section.txt for csValue, fileName and populate into a bash variable;
	infiles=`cat ${dirPath}/Cross-Section.txt | grep [0-9].* | awk -v var="${dirPath}" '{printf "\n\"%s/%s.root\",", var, $5}' | tr "\n" " " | sed 's/,$/}/g' | sed 's/^/{/g'`
	echo ; echo ;
	csValues=`cat ${dirPath}/Cross-Section.txt | grep [0-9].* | awk '{printf "\n%s,", $3}' | tr "\n" " " | sed 's/,$/}/g' | sed 's/^/{/g'`
	dirName=`sed -n 's/\(.*\)_[0-9-]\+/\1/p' <<< ${dirPath}`
	echo ;

		root -b -q "beforeAndAfterXSecNormalized.C+(${infiles}, ${csValues}, \"${dirName}\")"
	done
}

# Writes the histograms, before and after cross-section weighted into 
# a root file, but note that there wont be any identification information 
#works3


source ~/removeJunk.sh
clearJunk
clear

# This list is to pass via the shell script


fileName="{\"BoundProton_EPPS16_CT14nlo_pp.root\", \"BoundProton_EPS09_CT10nlo_pp.root\",
\"BoundAndFreeProton_EPPS16_CT14nlo_pp.root\", \"BoundAndFreeProton_EPS09_CT10nlo_pp.root\",\"FreeProton_CT14nlo_pp.root\", \"FreeProton_CT10nlo_pp.root\"}"


#fileList=({ "a\", \"b\" })
#files=`echo ${fileList[@]}`

files="{\"BoundAndFreeProton_EPPS16_CT14nlo_2017-06-30-155850.root\",\"BoundAndFreeProton_EPS09_CT10nlo_2017-06-30-155825.root\",\"BoundProton_EPPS16_CT14nlo_2017-06-30-155959.root\",\"BoundProton_EPS09_CT10nlo_2017-06-30-160022.root\",\"FreeProton_CT10nlo_2017-06-30-155912.root\",\"FreeProton_CT14nlo_2017-06-10-214324.root\"}"

# Look for the hist name in the root files given above
list=(10To20 20To30 30To50 50To80 80To120 120To170 170To230 230To300 300To380 380To10000)
histName=Neg_XSecWeighted_${list[8]} 
coltype="proton:(CT10, CT14) | anti-proton:(EPS09, EPPS16)" #or the other option would be ppbar
#root -l "perPtHatRatios.C+(\"${histName}\", \"$coltype\", ${files})" 


## Use this line to create info for files array in the
## lines above
#awk '{print $9}' | sed 's/^/\\"/g' | sed 's/$/\\"/g' | tr ls -ltrh | grep "17:39" | awk '{print $9}' | sed 's/^/\\"/g' | sed 's/$/\\"/g' | tr "\n" ","



ppFiles="{\"BoundAndFreeProton_EPPS16_CT14nlo_2017-06-30-155850.root\",\"BoundAndFreeProton_EPS09_CT10nlo_2017-06-30-155825.root\",\"BoundProton_EPPS16_CT14nlo_2017-06-30-155959.root\",\"BoundProton_EPS09_CT10nlo_2017-06-30-160022.root\",\"FreeProton_CT10nlo_2017-06-30-155912.root\",\"FreeProton_CT14nlo_2017-06-10-214324.root\"}"

ppbarFiles="{\"BoundAndFreeProton_EPPS16_CT14nlo_2017-06-30-155850.root\",\"BoundAndFreeProton_EPS09_CT10nlo_2017-06-30-155825.root\",\"BoundProton_EPPS16_CT14nlo_2017-06-30-155959.root\",\"BoundProton_EPS09_CT10nlo_2017-06-30-160022.root\",\"FreeProton_CT10nlo_2017-06-30-155912.root\",\"FreeProton_CT14nlo_2017-06-10-214324.root\"}"

pbarpFiles="{\"BoundAndFreeProton_EPPS16_CT14nlo_2017-06-30-155850.root\",\"BoundAndFreeProton_EPS09_CT10nlo_2017-06-30-155825.root\",\"BoundProton_EPPS16_CT14nlo_2017-06-30-155959.root\",\"BoundProton_EPS09_CT10nlo_2017-06-30-160022.root\",\"FreeProton_CT10nlo_2017-06-30-155912.root\",\"FreeProton_CT14nlo_2017-06-10-214324.root\"}"


# Look for the hist name in the root files given above
list=(10To20 20To30 30To50 50To80 80To120 120To170 170To230 230To300 300To380 380To10000)
histName=Neg_XSecWeighted_${list[8]} 
coltype="proton:(CT10, CT14) | anti-proton:(EPS09, EPPS16)" #or the other option would be ppbar
#root -l "perPtHatRatios.C+(\"${histName}\", \"$coltype\", ${files})" 

root -l "comparePPandPPbar.C+(\"${histName}\", \"${coltype}\", ${ppFiles}, ${ppbarFiles}, ${pbarpFiles})"
