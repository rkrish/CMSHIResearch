#include <iostream>
#include <map>
#include <vector>

#include <TPRegexp.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TH1D.h>
#include <TFile.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TLegendEntry.h>
#include <TColor.h>

using namespace std;


/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics

/* Main Program */
void perPDFSet_v6(vector<string> infiles, vector<double> csValues, const char* dirPath){

	map<string, map<string, TH1D*>> mh1;
	map<string, TFile*> mfile;
	map<string, const char*> sPath;

	sPath["Pos"] = "QCDAna/pchspectrum";
	sPath["Neg"] = "QCDAna/nchspectrum";
	sPath["All"] = "QCDAna/chspectrum";

	// Color Array
	Color_t color[] = {kBlue, kRed, kGreen, kBlack, kBlue-5, kRed-3, kGreen+3, kOrange+3, kViolet, kYellow+3, kGreen-2};

	if ( infiles.size() != csValues.size() ) { cout << "number of files: " << infiles.size() << "not equal to " << csValues.size()  << endl; exit(EXIT_FAILURE); }

	vector<string> vptHat;

	//TFile *outfile;
	// Loop over the files
	for (unsigned int i=0, colIt=0; i < infiles.size(); i++, colIt++){

		cout << csValues.at(i) << ": " << infiles.at(i)
			<< endl;
	
		TString s(infiles.at(i));
		TPRegexp r("(.*)/(.*)_([0-9]+To[0-9]+)_(.*)");
		TObjArray *ms =  r.MatchS(s);

		// Put a condition if there's a match
		string ptHat = (const char*)((TObjString*)ms->At(3))->GetString();

		cout 
			<< "\n0: " << (const char*)((TObjString*)ms->At(0))->GetString()
			<< "\n1: " << (const char*)((TObjString*)ms->At(1))->GetString()
			<< "\n2: " << (const char*)((TObjString*)ms->At(2))->GetString()
			<< "\n3: " << (const char*)((TObjString*)ms->At(3))->GetString()
			<< endl;


		vptHat.push_back(ptHat);
		//cout << ptHat << endl;

		// Pull the histograms from root files
		mfile[ptHat] = new TFile(infiles.at(i).c_str());

		char *label;
		for ( string charge : {"Pos", "Neg", "All"}){

			// ptHats before weighting
			mh1[ptHat][charge] = (TH1D*)mfile[ptHat]->Get(sPath[charge]);
			applyCosmetics(mh1[ptHat][charge], color[colIt], 20, 1.8); 

			// ptHats Weighted by cross-section
			label = Form("%s-weighted", ptHat.c_str());
			mh1[label][charge] = (TH1D*)mh1[ptHat][charge]->Clone();
			mh1[label][charge]->Sumw2();
			mh1[label][charge]->Scale(csValues.at(i));
			applyCosmetics(mh1[label][charge], color[colIt], 20, 1.8); 

			mh1[label][charge]->Scale(csValues.at(i));
			applyCosmetics(mh1[label][charge], color[colIt], 20, 1.8); 

			// Add all the normalized ptHats
			mh1["All-ptHats"][charge] = (TH1D*)mh1[label][charge]->Clone();
			mh1["All-ptHats"][charge]->Add(mh1[label][charge], 1);
		}


			mh1[ptHat]["Neg/Pos"] = (TH1D*)mh1[ptHat]["Neg"]->Clone();
			mh1[ptHat]["Neg/Pos"]->Divide(mh1[ptHat]["Pos"]);

		// Normalization formula
		//hin[i]->Scale(xsec.at(i)/nevent[i]);
		//hin[i]->Sumw2();

		// Add all the normalized ptHats
		//mh1["All-ptHats"]["Pos"] = (TH1D*)mh1[label]["Pos"]->Clone();
		//mh1["All-ptHats"]["Pos"]->Add(mh1[label]["Pos"], 1);

		/*
		mh1["All-ptHats"]["Neg"] = (TH1D*)mh1[label]["Neg"]->Clone();
		mh1["All-ptHats"]["Neg"]->Add(mh1[label]["Neg"], 1);

		mh1["All-ptHats"]["All"] = (TH1D*)mh1[label]["All"]->Clone();
		mh1["All-ptHats"]["All"]->Add(mh1[label]["All"], 1);
		*/

	}//close:for 

	// Writing objects to file
	TFile *outfile = new TFile(Form("%s.root", dirPath), "recreate");
	outfile->cd();

	for (string ptHat : vptHat){

		mh1[ptHat]["Pos"]->Write(Form("Pos_%s", ptHat.c_str()));
		mh1[ptHat]["Neg"]->Write(Form("Neg_%s", ptHat.c_str()));
		mh1[ptHat]["All"]->Write(Form("All_%s", ptHat.c_str()));

		char *label = Form("%s-weighted", ptHat.c_str());
		mh1[label]["Pos"]->Write(Form("Pos_XSecWeighted_%s", ptHat.c_str()));
		mh1[label]["Neg"]->Write(Form("Neg_XSecWeighted_%s", ptHat.c_str()));
		mh1[label]["All"]->Write(Form("All_XSecWeighted_%s", ptHat.c_str()));

	}//close:for

		mh1["All-ptHats"]["Pos"]->Write("Pos_All-ptHats");
		mh1["All-ptHats"]["Neg"]->Write("Neg_All-ptHats");
		mh1["All-ptHats"]["All"]->Write("All_All-ptHats");

	outfile->Close();
	



	if (0){
		TCanvas *chist = new TCanvas("chist", "Histograms", 900, 460);
		TLegend *leg = new TLegend(0.9, 0.7, 0.6, 0.8);

		applyCosmetics(mh1["All-ptHats"]["Pos"], kRed, 20, 1.8);
		leg->AddEntry(mh1["All-ptHats"]["Pos"], "Positive", "lep");
		mh1["All-ptHats"]["Pos"]->Draw("e1 same");

		applyCosmetics(mh1["All-ptHats"]["Neg"], kGreen+2, 20, 1.8);
		leg->AddEntry(mh1["All-ptHats"]["Neg"], "Negative", "lep");
		mh1["All-ptHats"]["Neg"]->Draw("e1 same");

		applyCosmetics(mh1["All-ptHats"]["All"], kBlue+1, 20, 1.8);
		//leg->AddEntry(mh1["All-ptHats"]["All"], "All Charged", "lep");
		//mh1["All-ptHats"]["All"]->Draw("p same");

		gPad->SetGrid();
		leg->Draw("same");

	}//close:if

	// After ptHat normalization
	if(0){

		TCanvas *cleg = new TCanvas("cleg", "Legend", 500, 230);
		TCanvas *chist = new TCanvas("chist", "Histograms", 900, 460);
		TLegend *leg = new TLegend(0.9, 0.7, 0.6, 0.8);

		// Before ptHat normalization
		int i=0; double dx=0.07, size=0.8;

		string type = "Pos";
		for (string ptHat : vptHat){

			char *label = Form("%s-weighted", ptHat.c_str());

			size+=dx;
			applyCosmetics(mh1[label][type], color[i], 20, 1.8); i++;
			chist->cd();
			mh1[label][type]->Draw("e1 same");

			gPad->SetGrid();
			leg->AddEntry(mh1[label][type], Form("%s, %s", label, type.c_str()), "lep");
			cleg->cd();
			leg->Draw("same");

		}//close:for
	}//close:if


	// Before ptHat normalization
	if (0){

		TCanvas *cleg = new TCanvas("cleg", "Legend", 500, 230);
		TCanvas *chist = new TCanvas("chist", "Histograms", 900, 460);
		TLegend *leg = new TLegend(0.9, 0.7, 0.6, 0.8);

		int i=0;
		string type = "Pos";
		for (string ptHat : vptHat){

			applyCosmetics(mh1[ptHat][type], color[i], 20, 1.8); i++;
			chist->cd();
			mh1[ptHat][type]->Draw("e1 same");

			cleg->cd();
			gPad->SetGrid();
			TLegendEntry *lE;
			lE = leg->AddEntry(mh1[ptHat][type], Form("%s, %s", ptHat.c_str(), type.c_str()), "lep");
			lE->SetMarkerSize(4);
			//((TAttMarker*)lE->GetObject())->Modify();
			//lE->PaintPrimitives();
			//leg->EditEntryAttFill();
			//lE->Print();
			//lE->GetObject();
			//cleg->Modified(); 
			//cleg->Update();
			leg->Draw("same");

		}//close:for
	}

	// Neg/Pos after combined
	if (0){

		mh1["NegByPos"]["Combined"] = (TH1D*)mh1["All-ptHats"]["Neg"]->Clone();
		mh1["NegByPos"]["Combined"]->Divide(mh1["All-ptHats"]["Pos"]);
		applyCosmetics(mh1["NegByPos"]["Combined"], kBlack, 20, 1.8);
		//gPad->SetLogy();
		mh1["NegByPos"]["Combined"]->Draw("p");
		gPad->SetGrid();
	}//close:if

	//string ptHat = "170To230";
	//mh1[ptHat]["Neg/Pos"]->Draw("e1 same");
	// Neg/Pos per pTHat interval
	if (0){

		char *label;
		int i=0;
		for (string ptHat : vptHat){

			//label = Form("%s-weighted", ptHat.c_str());
			applyCosmetics(mh1[ptHat]["Neg/Pos"], color[i], 20, 1.8); i++;
			mh1[ptHat]["Neg/Pos"]->Draw("e1 same");
			
		}//close:for

	}//close:if

	// Write to file 
	// Loop over the pHats: before/after


}//close:main
