#include <iostream>

using namespace std;

void objOnCanvasToFile_v0(){

	TH1D *h1 = new TH1D("h1", "", 1000, -10, 10);
	TH1D *h2 = new TH1D("h2", "", 1000, -10, 10);
	h1->FillRandom("gaus", 1000);
	h2->FillRandom("landau", 1000);

	h1->Draw("same");
	h2->Draw("same");
}//close-main
