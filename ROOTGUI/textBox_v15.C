// C++ Headers
#include <iostream>
#include <vector>
#include <map>
#include <string>


// ROOT GUI Headers
#include <TApplication.h>
//#include <TGCompositeFrame.h>
#include <TGClient.h>
#include <TGButton.h>
//#include <TGTextButton.h>
#include <TGTextEntry.h>
#include <TGWindow.h>
#include <TDirectory.h>
#include <TQObject.h>

// ROOT Headers
#include <TFile.h>
#include <TH1D.h>
#include <TRegexp.h>
#include <TCanvas.h>
#include <TObject.h>
#include <TLegend.h>
#include <TKey.h>
#include <TPRegexp.h>
#include <TList.h>

typedef const char* cchar;

using namespace std;

class histOptionsWindow : public TGMainFrame{

	public:
		histOptionsWindow(const TGWindow *p, UInt_t w, UInt_t h){

			SetWindowName("Change Histogram Options");
			SetWMSizeHints(350, 300, 350, 150, 1, 1);
			MapSubwindows();
			MapWindow();

		}
};

/* GUICore - Main GUI Class definition */
class GUICore : public TGMainFrame{

	private :
		enum {clear, getHist, divide, color, markerSize, markerStyle};
		GUICore *guiMain;

	public :
		// Methods
		GUICore(const TGWindow *p, UInt_t w, UInt_t h);
		GUICore(const TGWindow *p, UInt_t w, UInt_t h, GUICore*){};
		GUICore(const TGWindow *p, UInt_t w, UInt_t h, TH1D*);
		GUICore(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names, GUICore *g);
		void drawWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names);

		void histOptions(const TGWindow *p, UInt_t w, UInt_t h, GUICore*);
		cchar ReadFile();
		bool FileExists(cchar);
		void createHistListWindow(cchar);
		void Divide();
		void Divide(TH1D *hNum, TH1D *hDenom);
		void ClearCanvas();
		void UpdateCanvas(TObject *obj);
		void UpdateCanvas(cchar, cchar, TCanvas*);
		void ChangeHistColor();
		void UpdateCanvas();
		TLegend *leg, *legRatio;

		/* func to clear up if its > 2 */
		void toggle(cchar, cchar);
		cchar windowName;
		void Checking();
		void Exit();
		void overrideClicked();
		TObject *obj;
		TH1D *h1;
		TFile *file;
		void calcRatio2(vector<cchar>, vector<cchar>);
		void drawOnCanvas(TCanvas*, TH1D*);
		void SaveToFile();

		// Variables
		TCanvas *canvas, *cratio, *cLeg;
		cchar hNameForRatio;
		TGTextEntry *txtBox, *saveToFile;
		TH1D *hRatio;
		map<cchar, TGTextButton*> btn;
		map<cchar, GUICore*> mhWindow;
		map<cchar, TGCheckButton*> mcgbtn;
		map<cchar, TH1D*> mh1;
		map<cchar, TGNumberEntry*> mNumberEntry;

		vector<cchar> currHistInfo, prevHistInfo;
		int count;
		void writeToFile(TCanvas*, cchar);
		map<cchar, TH1D*> mhRatio;
		void EventInfo(Int_t event, Int_t px, Int_t py, TObject *sel);
		void ChangeHistOptions(TVirtualPad*, TObject*, Int_t);
		void ChangeHistProperties(TH1D*);

		TH1D *hClick;
		TColorWheel *colorWheel;
		


};//close-textEntry



void GUICore::histOptions(const TGWindow *p, UInt_t w, UInt_t h, GUICore*){}


GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h, TH1D* hist){
//GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h, GUICore* g){

	cout << "============================================================" << endl;
	cout << "GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h, TH1D* hist){" << endl;
	cout << "============================================================" << endl;

	cout << "hist change options" << endl;

	//hClick->SetMarkerStyle(20);
	//this->guiMain->canvas->cd();
	//hist->SetMarkerStyle(20);
	//hClick->SetMarkerStyle(20);
	//this->guiMain->canvas->Modified();
	//this->guiMain->canvas->Update();

	//cout << "windowname: " << this->guiMain->windowName << endl;

	this->hClick = hist;

	TGVerticalFrame *vframe = new TGVerticalFrame(this);
	AddFrame(vframe);


	TGCompositeFrame *hl1 = new TGCompositeFrame(this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth );


	TGLabel *labelMarkerSize = new TGLabel(hl1, "Marker Size");
	hl1->AddFrame(labelMarkerSize);
	

	mNumberEntry["color"] = new TGNumberEntry(hl1);
	mNumberEntry["color"]->SetState(kTRUE);
	//mNumberEntry["color"]->Connect("ValueSet(Long_t)", "GUICore", this, "ChangeHistColor(Long_t)")
   	hl1->AddFrame(mNumberEntry["color"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	vframe->AddFrame(hl1);

	mNumberEntry["markerSize"] = new TGNumberEntry(vframe, markerSize);
   	vframe->AddFrame(mNumberEntry["markerSize"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

	/*

	TGCompositeFrame *hframeLabel1 = new TGCompositeFrame(this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth );

	TGLabel *labelMarkerSize = new TGLabel(hframeLabel1, "Marker Size");
	hframeLabel1->AddFrame(labelMarkerSize);

	TGLabel *labelMarkerStyle = new TGLabel(hframeLabel1, "Marker Style");
	hframeLabel1->AddFrame(labelMarkerStyle);

	TGLabel *labelMarkerColor = new TGLabel(vframe, "Marker Color");
	vframe->AddFrame(labelMarkerColor);

	vframe->AddFrame(hframeLabel1);
	*/

	mNumberEntry["markerStyle"] = new TGNumberEntry(vframe, markerStyle);
   	vframe->AddFrame(mNumberEntry["markerStyle"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));


	TGCompositeFrame *hframe = new TGCompositeFrame(this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth );

	btn["apply"] = new TGTextButton(hframe, "&Apply");
   	hframe->AddFrame(btn["apply"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["apply"]->Connect("Clicked()", "GUICore", this, "ChangeHistColor()");

	btn["exit"] = new TGTextButton(hframe, "&Exit");
   	hframe->AddFrame(btn["exit"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["exit"]->Connect("Clicked()", "GUICore", this, "Exit()");
	cout << "hist name: " << hist->GetName() << endl;

	btn["colorWheel"] = new TGTextButton(hframe, "&Color Wheel");
   	hframe->AddFrame(btn["colorWheel"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

	btn["colorWheel"]->Connect("Clicked()", "GUICore", colorWheel, "Draw()");
	

	vframe->AddFrame(hframe);
	/*
	btn["apply"] = new TGTextButton(hframe, "&Apply");
   	hframe->AddFrame(btn["apply"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["apply"]->Connect("Clicked()", "GUICore", this, "ChangeHistColor()");

	btn["exit"] = new TGTextButton(this, "&Exit");
   	hframe->AddFrame(btn["exit"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["exit"]->Connect("Clicked()", "GUICore", this, "Exit()");
	cout << "hist name: " << hist->GetName() << endl;

	btn["colorWheel"] = new TGTextButton(this, "&Color Wheel");
   	hframe->AddFrame(btn["colorWheel"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

	btn["colorWheel"]->Connect("Clicked()", "GUICore", colorWheel, "Draw()");
	*/


	//mTxtEntry["exit"]->Connect("Clicked()", "GUICore", this, "Exit()");

	SetWindowName("Change Histogram Options");
	SetWMSizeHints(350, 300, 350, 150, 1, 1);
	MapSubwindows();
	MapWindow();

	cout << "\n -- END : GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h, TH1D* hist) -- \n" << endl;
	cout << "------------------------------------------------------\n" << endl;
}

void GUICore::ChangeHistColor(){

	cout << "============================================================" << endl;
	cout << "GUICore::ChangeHistColor()" << endl;
	cout << "============================================================" << endl;

	int color = mNumberEntry["color"]->GetNumber();
	int style = mNumberEntry["markerStyle"]->GetNumber();
	int size = mNumberEntry["markerSize"]->GetNumber();

	//TGNumberEntry *tE = (TGNumberEntry*)gTQSender;
	cout << mNumberEntry["color"]->GetNumber() << endl;
	//cout << "get text: " << tE->GetText();

	cout << "object: " << this << endl;
	cout << __LINE__ << endl;
	cout << hClick << endl;

	this->hClick->SetMarkerColor(color);
	this->hClick->SetMarkerStyle(style);
	this->hClick->SetMarkerSize(size);

	cout << __LINE__ << endl;
	//hClick->SetMarkerColor(color);

	cout << "\n -- END : GUICore::ChangeHistColor() -- \n" << endl;
	cout << "------------------------------------------------------\n" << endl;
}

void GUICore::ChangeHistProperties(TH1D *h){

	cout << "============================================================" << endl;
	cout << "GUICore::ChangeHistProperties(TH1D *h)" << endl;
	cout << "============================================================" << endl;


	//new GUICore(gClient->GetRoot(), 400, 400, h);
	new GUICore(gClient->GetRoot(), 400, 400, h);
	//new histOptionsWindow(gClient->GetRoot(), 400, 400);
	//new histOptionsWindow(gClient->GetRoot(), 400, 400);

	cout << "hClick: " << hClick << endl;
	cout << "-- END : GUICore::ChangeHistProperties(TH1D *h) -- " << endl;
	cout << "------------------------------------------------------\n" << endl;

}//close-GUICore::ChangeHistProperties()

void GUICore::ChangeHistOptions(TVirtualPad* vp, TObject* obj, Int_t event){

	cout << "============================================================" << endl;
	cout << "GUICore::ChangeHistOptions(TVirtualPad*, TObject*, Int_t)" << endl;
	cout << "============================================================" << endl;

	cout << "Event fired: " << event << endl;
	cout << "Object name: " << obj->GetName() << endl;
	cout << "Get nBins: " << ((TH1D*)obj)->GetSize() << endl;
	cout << "Get className: " << ((TH1D*)obj)->ClassName() << endl;

	hClick = (TH1D*)obj;
	cout << "hClick: " << hClick << endl;
	//hClick->SetMarkerStyle(20);

	cout << "Comparing objects: " << strcmp(obj->ClassName(), "TH1D") << endl;;
	//if(strcmp(obj->ClassName(), "TH1D")) ChangeHistProperties((TH1D*)obj);
	if(strcmp(obj->ClassName(), "TH1D")) ChangeHistProperties(hClick);

	cout << "\n -- END : GUICore::ChangeHistOptions(TVirtualPad*, TObject*, Int_t) -- " << endl;
	cout << "------------------------------------------------------\n" << endl;

}//close-GUICore::ChangeHistOptions(TVirtualPad*, TObject*, Int_t)
void GUICore::EventInfo(Int_t event, Int_t px, Int_t py, TObject *sel){

	cout << "events fired: " << endl;
}


void GUICore::SaveToFile(){

	cout <<"============================" << endl;
	cout << "GUICore::saveToFile()" << endl;
	cout <<"============================" << endl;

	writeToFile(canvas, saveToFile->GetText());

	cout << "Saved to the file: " << saveToFile->GetText() << endl;

	cout << " == end of saveToFile() ==" << endl;
	cout << "---------------------------" << endl;

}//close-GUICore::saveToFile()


void GUICore::writeToFile(TCanvas *c, cchar outputFile){

	TList *list = c->GetListOfPrimitives();
	
	TIter next(c->GetListOfPrimitives());
	TObject *obj = next();

	
	TFile *file = new TFile(outputFile, "RECREATE");
	
	obj->Write(obj->GetName());
	while (TObject *obj = next()){

		 cout << obj->GetName() << endl;
		 obj->Draw(next.GetOption());
		 cout << endl;
		 //obj->Dump();
		 obj->Write(obj->GetName());
		 
	}//close-while

	file->Close();

}//close-GUICore::writeToFile()


void GUICore::drawOnCanvas(TCanvas *mCanvas, TH1D *h1){

cout << "======================" << endl;
cout << "GUICore::drawOnCanvas" << endl;
cout << "======================" << endl;

cout << "canvas: " << mCanvas << endl;

		mCanvas->cd();
		//h1->SetMarkerStyle(20);
		//h1->Draw();
		
		cchar h1Name = h1->GetName();

		mhRatio[h1Name] = (TH1D*)h1->Clone(h1Name);
		mhRatio[h1Name]->SetTitle(h1Name);
		mhRatio[h1Name]->Draw("same");
		
		mCanvas->Modified();
		mCanvas->Update();

		TLegend *leg2 = new TLegend(0.1, 0.7, 0.48, 0.9);

		this->cLeg->cd();
		leg2->AddEntry(mhRatio[h1->GetName()], h1->GetName(), "lep");
		leg2->SetTextSize(0.05);
		leg2->SetLineWidth(0);
		//TStyle *st;
		//gStyle->SetLegendBorderSize(0.5);
		leg2->Draw("same");
		

cout << "----------------------" << endl;

}//close-GUICore::drawOnCanvas()


void GUICore::overrideClicked(){

	Emit("Clicked()");
}

void GUICore::drawWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names){}

GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names, GUICore *g){

	cout << "=========================================" << endl;
	cout << "Current Pointer GUICore: " << this << endl;
	cout << "Pointer to Main GUICore: " << this->guiMain << endl;
	cout << "=========================================" << endl;

	this->guiMain = g;

	cout << "GUICore:main from second: " << g << endl;
	TGVerticalFrame *hframe = new TGVerticalFrame(this);
	AddFrame(hframe);

	for (cchar h1Name : vh1Names){

		mcgbtn[h1Name] = new TGCheckButton(hframe, Form("&%s", h1Name), 200);
		mcgbtn[h1Name]->Connect("Pressed()", "GUICore", this, "Checking()");
		hframe->AddFrame(mcgbtn[h1Name], new TGLayoutHints(kLHintsLeft, 1, 1, 1, 1));
	}

	this->windowName = windowName;
	SetWindowName(windowName);
	SetWMSizeHints(400, 500, 400, 1000, 1, 1);
	MapSubwindows();
	MapWindow();

	Resize(GetDefaultSize());
	
}//close-GUI::Constructor

void GUICore::toggle(cchar fileName, cchar histName){

	cout << "===============================" << endl;
	cout << "toggle: " << fileName << "\n"
		<< "count: " << this->guiMain->count << "\n"
	     << histName << endl;

	cout << "===============================" << endl;

	
	cout << "===============================" << endl;
	switch (this->guiMain->count) {

		case 0 :

			if (this->guiMain->prevHistInfo.size() == 0) cout << "prev empty" << endl;
			this->guiMain->currHistInfo.empty();
			//this->prevHistInfo.empty();
			cout << "case0 " << fileName << "\n"
			     << "case0 " << histName << "\n";

			this->guiMain->currHistInfo.push_back(fileName);
			this->guiMain->currHistInfo.push_back(histName);
			(this->guiMain->count)++;

			cout << "Current Hist Info: fileName " << this->guiMain->currHistInfo.at(0) << "\n"
			<< "Current Hist Info: hisName " << this->guiMain->currHistInfo.at(1) << "\n"
			     << endl;

			break;

		case 1 :

			this->guiMain->prevHistInfo = this->guiMain->currHistInfo;
			cout << "from case 1: " << this->guiMain->prevHistInfo.size() << endl;
			this->guiMain->currHistInfo.clear();
			cout << "from case 2: sizeof currhistInfo" << this->guiMain->currHistInfo.size() << endl;

			cout << "case1 " << fileName << "\n"
			     << "case1 " << histName << "\n";

			this->guiMain->currHistInfo.push_back(fileName);
			this->guiMain->currHistInfo.push_back(histName);
			(this->guiMain->count) = 1 ;

			cout << "Previous Hist Info: fileName " << this->guiMain->prevHistInfo.at(0) << "\n"
			<< "Previous Hist Info: hisName " << this->guiMain->prevHistInfo.at(1) << "\n"

			<< "Current Hist Info: fileName " << this->guiMain->currHistInfo.at(0) << "\n"
			<< "Current Hist Info: hisName " << this->guiMain->currHistInfo.at(1) << "\n"
			     << endl;

			break;

	}//switch
	cout << "===============================" << endl;

}//close-GUICore::toggle(cchar fileName, cchar histName)

void GUICore::Checking(){

	TGCheckButton *tg = (TGCheckButton*)gTQSender;
	TGHotString *ths = (TGHotString*)gTQSender;

	cout << "Checking: checkbutton status: " << !tg->IsOn() << endl;
	cchar histName = tg->GetText()->GetString();
	//cout << tg->GetName();
	//cout << "string name: " << ths->GetString();
	//cout << "string name: " << tg->GetText()->GetString();
	//cout << this->windowName << endl;


	if (!tg->IsOn()){
		//this->guiMain->toggle(this->windowName, histName);
		this->toggle(this->windowName, histName);

		//this->Update(cchar fileName, cchar h1Name);
		this->file = new TFile(this->windowName, "r");
		//this->UpdateCanvas(this->windowName, histName);
		
		//this->UpdateCanvas(this->windowName, histName, this->canvas);
		this->guiMain->UpdateCanvas(this->windowName, histName, this->guiMain->canvas);
		cout << "Checking: canvas: " << this->canvas << endl;

		cout << file->IsOpen();
	}


}//close-histListWindow::Checking

void GUICore::ClearCanvas(){

	this->canvas->Clear();
	this->canvas->Modified();
	this->canvas->Update();

	this->cratio->Clear();
	this->cratio->Modified();
	this->cratio->Update();

	// Clear TLegend
	// Clear TCheckBoxes
	// Clear TCanvas
	// Loop(obj on Canvas)	{

	//}
	
}

GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h)
{

	this->count = 0;
	this->leg = new TLegend(0.9, 0.8, 0.6, 0.9);
	this->legRatio = new TLegend(0.9, 0.8, 0.6, 0.9);
	//this->hRatio = new TH1D();
	//cout << "main: " << this->hRatio << endl;
	this->cLeg = new TCanvas("Legends", "Legend", 500, 200);


	this->overrideClicked();
   	TGVerticalFrame *vframe = new TGVerticalFrame(this);
   	AddFrame(vframe, new TGLayoutHints(10, 20, 20, 20));

	txtBox = new TGTextEntry(this);
	txtBox->DrawBorder();
	vframe->AddFrame(txtBox, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));

   	//TGHorizontalFrame *hframe = new TGHorizontalFrame(this);
   	//AddFrame(hframe, new TGLayoutHints(10, 20, 20, 20));

	//vframe->AddFrame(hframe);

	TGCompositeFrame *hframe = new TGCompositeFrame ( this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth );

	btn["getHist"] = new TGTextButton(hframe, "&Get Histograms");
   	hframe->AddFrame(btn["getHist"], new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
	btn["getHist"]->Connect("Clicked()", "GUICore", this, "ReadFile()");

	btn["divide"] = new TGTextButton(hframe, "&Divide", divide);
   	hframe->AddFrame(btn["divide"], new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
	btn["divide"]->Connect("Clicked()", "GUICore", this, "Divide()");

	btn["clear"] = new TGTextButton(hframe, "&Clear", clear);
   	hframe->AddFrame(btn["clear"], new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
	btn["clear"]->Connect("Clicked()", "GUICore", this, "ClearCanvas()");

	/*
	btn["update"] = new TGTextButton(this, "&Update", divide);
   	vframe->AddFrame(btn["update"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["update"]->Connect("Clicked()", "GUICore", this, "UpdateCanvas()");

	btn["save"] = new TGTextButton(this, "&Update", divide);
   	vframe->AddFrame(btn["save"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["save"]->Connect("Clicked()", "GUICore", this, "writeEvent()");
	*/

	//btn["update"]->Connect("Clicked()", "GUICore", this, TString::Format("UpdateCanvas(=0x%0lx)",(ULong_t)this->obj));


	/*
	TGCompositeFrame *hframe = new TGCompositeFrame ( this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth );

	btn["alpha"] = new TGTextButton(hframe, "&alpha", divide);
   	hframe->AddFrame(btn["alpha"], new TGLayoutHints( kLHintsExpandX, 5, 5, 5, 5) );

	btn["beta"] = new TGTextButton(hframe, "&beta", divide);
   	hframe->AddFrame(btn["beta"], new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));

	btn["exit"] = new TGTextButton(hframe, "&exit", divide);
   	hframe->AddFrame(btn["exit"], new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
	btn["exit"]->Connect("Clicked()", "GUICore", this, "Exit()");

	*/
	vframe->AddFrame(hframe);


	cout << "Main: this " << this << endl;
	this->canvas = new TCanvas("canvas", " Histograms from Files  ", 900, 460);
	this->cratio = new TCanvas("cratio", " Ratio of Histograms ", 900, 460);

	//this->canvas->Connect("ProcessedEvent(Int_t, Int_t, Int_t, TObject*)", "GUICore", this, "EventInfo(Int_t, Int_t, Int_t, TObject*)");
	this->canvas->Connect("Selected(TVirtualPad*, TObject*, Int_t)", "GUICore", this, "ChangeHistOptions(TVirtualPad*, TObject*, Int_t)");

	cout << "Main: Canvas " << this->canvas << endl;

	/* Save to file */
	saveToFile = new TGTextEntry(this, "Click on Canvas, enter file name, click Save");
	saveToFile->DrawBorder();


	vframe->AddFrame(saveToFile, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));

	btn["saveToFile"] = new TGTextButton(vframe, "&SaveToFile", divide);
   	vframe->AddFrame(btn["saveToFile"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["saveToFile"]->Connect("Clicked()", "GUICore", this, "SaveToFile()");

	// Create canvas object, but hide it
	//this->h1 = new TH1D("h1", "", 1000, -10, 10);
	//this->h1->FillRandom("gaus", 100000);
	//TFile *file = new TFile("sample2.root", "r");
	//this->h1 = (TH1D*)file->Get("Ratio-NegByPos_CTEQ5L-LO_HardQCDAll");;
	//this->h1->Draw();

	SetWindowName("Input the file");
	SetWMSizeHints(350, 300, 350, 150, 1, 1);
	MapSubwindows();
	MapWindow();

}//close-GUICore

void GUICore::Exit(){

	gApplication->Terminate(0);

}//close-GUICore::Exit()

//void GUICore::UpdateCanvas(TObject *obj){
//void GUICore::UpdateCanvas(cchar windowName, cchar objName){
void GUICore::UpdateCanvas(cchar windowName, cchar objName, TCanvas *canvas2){

	cout << "clicked" << endl;
	cout << windowName << objName << endl;
	cout << "UpdateCanvas: canvas pointer: " << &(canvas2) << endl;

	TFile *file2 = new TFile(windowName, "r");
	cout << "Is open? " << file2->IsOpen() << endl;

	cout << "Update canvas: this " << this << endl;
	cout << "Update canvas: " << this->canvas << endl;
	this->leg->AddEntry(mh1[objName], Form("%s | %s", windowName, objName), "lep");
	this->mh1[objName] = (TH1D*)file2->Get(objName);

	this->mh1[objName]->Draw("same");
	this->leg->Draw("same");
	this->mh1[objName]->SetDirectory(0);

	file2->Close();

	(this->canvas)->Modified();
	(this->canvas)->Update();

}//close-GUICore::UpdateCanvas


void GUICore::calcRatio2(vector<cchar> hNumInfo, vector<cchar> hDenomInfo){

	cchar numFileName, denomFileName, numHistName, denomHistName;

	numFileName = hNumInfo.at(0);
	denomFileName = hDenomInfo.at(0);

	numHistName = hNumInfo.at(1);
	denomHistName = hDenomInfo.at(1);

	TH1D *hNum, *hDenom;
	TFile *numFile = new TFile(numFileName, "r");
	TFile *denomFile = new TFile(denomFileName, "r");


	cout << "file name 1 " << numFileName << " is Open? "  << numFile->IsOpen() << "\n"
	     << "file name 2 " << denomFileName << " is Open? "  << denomFile->IsOpen() << "\n"
	     << "hist name 1 " << numHistName << "\n"
	     << "hist name 2 " << denomHistName << "\n"
	     << endl;


		hNum = (TH1D*)numFile->Get(numHistName);
		hDenom = (TH1D*)denomFile->Get(denomHistName);

	if (hNum->GetSize() == hDenom->GetSize()){

		hNameForRatio = Form("%s_BY_%s", numHistName, denomHistName);
	        mhRatio[hNameForRatio] = (TH1D*)hNum->Clone(hNameForRatio);
		mhRatio[hNameForRatio]->Divide(hDenom);

		cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << endl;
		cout << "calcRatio2:name " << mhRatio[hNameForRatio]->GetName() << endl;
		cout << "calcRatio2:bins " << mhRatio[hNameForRatio]->GetSize() << endl;
		cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << endl;

		/*
		this->cratio->cd();
		this->hRatio->SetMarkerStyle(20);
		this->hRatio->Draw();
		this->cratio->Modified();
		this->cratio->Update();
		*/

		drawOnCanvas(this->canvas, mhRatio[hNameForRatio]);
		drawOnCanvas(this->cratio, mhRatio[hNameForRatio]);

		for (int i=0; i < mhRatio[hNameForRatio]->GetSize(); i++){
			cout << hRatio->GetBinContent(i) << "\t";
		}

	}

	numFile->Close();
	denomFile->Close();

	//delete hNum; delete hDenom;

	//return hRatio;
}

//void GUICore::Divide(TH1D *hNum, TH1D *hDenom){
void GUICore::Divide(){

	cout << "from Divide()" << endl;
	cout << "divide called by: " << this << endl;
	cout << "divide : TH1D " << this->hRatio << endl;

	TGCheckButton *tg = (TGCheckButton*)gTQSender;

	cout << "button name: " << tg->GetName() << endl;
	cout << "divide clicked" << endl;

	if ( (this->currHistInfo.size() == 2) && (this->prevHistInfo.size() == 2) ){

		cout << "divide : TH1D " << this->hRatio << endl;
		cout << "currhistInfo: fileName \t" << this->currHistInfo.at(0) << endl;
		cout << "currhistInfo: histName \t" << this->currHistInfo.at(1) << endl;

		cout << "prevhistInfo: fileName \t" << this->prevHistInfo.at(0) << endl;
		cout << "prevhistInfo: histName \t" << this->prevHistInfo.at(1) << endl;


		cout << __LINE__ << endl;
		cout << "before divide : TH1D " << this->hRatio << endl;
		cout << "before divide : TH1D " << hRatio << endl;
		//this->calcRatio(this->currHistInfo, this->prevHistInfo, this->hRatio);
		//hRatio = calcRatio(this->currHistInfo, this->prevHistInfo, this->hRatio);
		calcRatio2(this->currHistInfo, this->prevHistInfo);
		cout << "after divide : TH1D " << hRatio << endl;
		cout << "after divide : TH1D " << this->hRatio << endl;
		cout << "canvas obj: "  << this->canvas << endl;
		cout << __LINE__ << endl;
		cout << "after divide : bins: " << this->hRatio->GetSize() << endl;
		//cout << "after divide : bincontent: " << this->hRatio->GetBinContent(5) << endl;

		//hRatio->Dump();
		//cout << "after divide : name: " << this->hRatio->GetName() << endl;
		cout << __LINE__ << endl;

		//TCanvas *c = new TCanvas("c", "", 900, 500);
		//c->cd();
		//this->hRatio->FillRandom("gaus", 10000);
		//this->hRatio->Draw("same");
		cout << __LINE__ << endl;
		/*
		TFile *fil = new TFile("out.root", "RECREATE");
		cout << __LINE__ << endl;
		cout << "divide : hratio " << this->hRatio << endl;
		(this->hRatio)->Write("check");
		cout << __LINE__ << endl;
		fil->Close();
		*/
		cout << __LINE__ << endl;
		//c->Modified();
		//c->Update();

		cout << __LINE__ << endl;
		//this->hRatio->SetDirectory(0);
		//this->hRatio->Draw("same");
		cout << __LINE__ << endl;
		this->canvas->Modified();
		cout << __LINE__ << endl;
		this->canvas->Update();
		cout << __LINE__ << endl;
	}

	else { 
		cout << "not enough information - you might need to click one more histogram \
			 " << endl;
	}
	
		cout << "bins: " << this->hRatio->GetSize() << endl;
		cout << __LINE__ << endl;
		//this->hRatio->Draw("same");
		cout << __LINE__ << endl;

}//close-GUICore::Divide(){

void GUICore::createHistListWindow(cchar fileName){

	TFile *inputFile= new TFile(fileName, "r");
	cout << "Is the file open? " << inputFile->IsOpen() << endl;

		vector<cchar> vh1Names;
		inputFile->cd();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )){

			TString s0((char*)key->GetName());
			TPRegexp r2("([a-zA-Z0-9-().+/]+)_([a-zA-Z0-9-().+]+)_([a-zA-Z0-9-().+]+)");
			//charge = ((TObjString*)(r2.MatchS(s0)->At(1)))->GetString();
			//cout << key->GetName() << endl;
			vh1Names.push_back(key->GetName());

		}//Loop: Histogram objects

	//new GUICore(gClient->GetRoot(), 400, 400);
	cout << "createHistListWindow: canvas " << this->canvas << endl;
	this->mhWindow[inputFile->GetName()] = new GUICore(gClient->GetRoot(), 400, 400, inputFile->GetName(), vh1Names, this);
	//new GUICore(gClient->GetRoot(), 400, 400, inputFile->GetName(), vh1Names, this);
	//this->drawWindow(gClient->GetRoot(), 400, 400, inputFile->GetName(), vh1Names);
	//this->mhWindow[inputFile->GetName()] = new GUICore(gClient->GetRoot(), 500, 500, inputFile->GetName(), vh1Names);
	inputFile->Close();

}//close-GUICore::createHistList



/* Check if the file exists */
bool GUICore::FileExists(cchar fileName){

	if(1) this->createHistListWindow(fileName);
	else {
		cout << "\nThe file with the name provided: "
			<< fileName
			<< " doesn't exist in the path specified"
			<< endl;
		exit(1);
	}
	return 1;
}

/* Read the text file box */
cchar GUICore::ReadFile(){

	cchar fileName = txtBox->GetText();
	
	this->FileExists(fileName);

	return fileName;

}//close-GUICore::ReadFile()

void textBox_v15(){

	GUICore *gc = new GUICore(gClient->GetRoot(), 400, 400);
	
	TDirectory *td = new TDirectory();
	td->pwd();
	cout << td->GetFile();
	//cout << (td->OpenFile("samplefile.root"))->IsOpen();
	td->Print();



}//close-main
