// C++ Headers
#include <iostream>
#include <vector>
#include <map>


// ROOT GUI Headers
#include <TApplication.h>
//#include <TGCompositeFrame.h>
#include <TGClient.h>
#include <TGButton.h>
//#include <TGTextButton.h>
#include <TGTextEntry.h>
#include <TGWindow.h>
#include <TDirectory.h>

// ROOT GUI Headers
#include <TFile.h>
#include <TH1D.h>
#include <TRegexp.h>
#include <TCanvas.h>

typedef const char* cchar;

using namespace std;


/*
class processHist : public TGMainFrame{

	public:
		processHist (const TGWindow *p, UInt_t w, UInt_t h );

};//close-processHist


processHist::processHist(const TGWindow *p, UInt_t w, UInt_t h){

	//SetWindowName(windowName);
	SetWMSizeHints(200, 500, 300, 1000, 1, 1);
	MapSubwindows();

	Resize(GetDefaultSize());
	MapWindow();

}//close-processHist constructor
*/


class histListWindow : public TGMainFrame, public GUICore{

	public:
		histListWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar, vector<cchar> );
		void Checking();
		map<cchar, TGCheckButton*> mcgbtn;
		cchar windowName;
		GUICore guiObj;

};

void histListWindow::Checking(){

	TGCheckButton *tg = (TGCheckButton*)gTQSender;
	TGHotString *ths = (TGHotString*)gTQSender;

	cchar histName = tg->GetText()->GetString();
	//cout << tg->GetName();
	cout << "string name: " << ths->GetString();
	cout << "string name: " << tg->GetText()->GetString();
	cout << this->windowName << endl;

	TFile *file = new TFile(this->windowName, "r");
	cout << file->IsOpen();

}//close-histListWindow::Checking

histListWindow :: histListWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names){

	TGVerticalFrame *hframe = new TGVerticalFrame(this);
	AddFrame(hframe);

	for (cchar h1Name : vh1Names){
		mcgbtn[h1Name] = new TGCheckButton(hframe, Form("&%s", h1Name), 200);
		//mcgbtn[h1Name]->Connect("Pressed()", "histListWindow", this, "Checking()");
		mcgbtn[h1Name]->Connect("Pressed()", "GUICore", guiObj, "Checking()");
		hframe->AddFrame(mcgbtn[h1Name], new TGLayoutHints(kLHintsLeft, 1, 1, 1, 1));
	}

	this->windowName = windowName;
	SetWindowName(windowName);
	SetWMSizeHints(400, 500, 400, 1000, 1, 1);
	MapSubwindows();

	Resize(GetDefaultSize());
	MapWindow();

}//close;histListWindow-constructor

void GUICore::Checking(){

	cout << "from GUICORE Checking" << endl;
}


/* GUICore - Main GUI Class definition */
class GUICore : public TGMainFrame{

	private :
		enum {clear, getHist, divide};

	public :
		// Methods
		GUICore(const TGWindow *p, UInt_t w, UInt_t h);
		cchar ReadFile();
		bool FileExists(cchar);
		void createHistListWindow(cchar);
		void Divide();
		void Divide(TH1D *hNum, TH1D *hDenom);
		void ClearCanvas();
		void UpdateCanvas();
		void Checking();

		// Variables
		TCanvas *canvas;
		TGTextEntry *txtBox;
		map<cchar, TGTextButton*> btn;
		map<cchar, histListWindow*> mhWindow;

};//close-textEntry

GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h)
{


   	TGVerticalFrame *vframe = new TGVerticalFrame(this);
   	AddFrame(vframe, new TGLayoutHints(10, 20, 20, 20));

	txtBox = new TGTextEntry(this);
	txtBox->DrawBorder();
	vframe->AddFrame(txtBox, new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

   	//TGHorizontalFrame *hframe = new TGHorizontalFrame(this);
   	//AddFrame(hframe, new TGLayoutHints(10, 20, 20, 20));

	//vframe->AddFrame(hframe);

	btn["getHist"] = new TGTextButton(this, "&Get Histograms");
   	vframe->AddFrame(btn["getHist"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["getHist"]->Connect("Clicked()", "GUICore", this, "ReadFile()");

	btn["clear"] = new TGTextButton(this, "&Clear", clear);
   	vframe->AddFrame(btn["clear"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["clear"]->Connect("Clicked()", "GUICore", this, "ReadFile()");

	btn["divide"] = new TGTextButton(this, "&Divide", divide);
   	vframe->AddFrame(btn["divide"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["divide"]->Connect("Clicked()", "GUICore", this, "Divide()");

	btn["update"] = new TGTextButton(this, "&Update", divide);
   	vframe->AddFrame(btn["update"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["update"]->Connect("Clicked()", "GUICore", this, "UpdateCanvas()");


	TGCompositeFrame *hframe = new TGCompositeFrame (this, 300, 400, kLHintsCenterX | kHorizontalFrame | kFixedWidth);

	btn["alpha"] = new TGTextButton(hframe, "&alpha", divide);
   	hframe->AddFrame(btn["alpha"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

	btn["beta"] = new TGTextButton(hframe, "&beta", divide);
   	hframe->AddFrame(btn["beta"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

	vframe->AddFrame(hframe);

	SetWindowName("Input the file");
	SetWMSizeHints(350, 300, 350, 150, 1, 1);
	MapSubwindows();
	MapWindow();

}//close-GUICore

void GUICore::UpdateCanvas(){

	this->canvas = new TCanvas("c", "", 900, 460);
	canvas->Update();
	canvas->Draw();

}//close-GUICore::UpdateCanvas

//void GUICore::Divide(TH1D *hNum, TH1D *hDenom){
void GUICore::Divide(){

	TGCheckButton *tg = (TGCheckButton*)gTQSender;
	cout << "button name: " << tg->GetName() << endl;
	cout << "divide clicked" << endl;
}

void GUICore::createHistListWindow(cchar fileName){

	TFile *inputFile= new TFile(fileName, "r");
	cout << "Is the file open? " << inputFile->IsOpen() << endl;

		vector<cchar> vh1Names;
		inputFile->cd();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )){

			TString s0((char*)key->GetName());
			TPRegexp r2("([a-zA-Z0-9-().+/]+)_([a-zA-Z0-9-().+]+)_([a-zA-Z0-9-().+]+)");
			//charge = ((TObjString*)(r2.MatchS(s0)->At(1)))->GetString();
			cout << key->GetName() << endl;
			vh1Names.push_back(key->GetName());

		}//Loop: Histogram objects

	
	mhWindow[inputFile->GetName()] = new histListWindow(gClient->GetRoot(), 500, 500, inputFile->GetName(), vh1Names);
	inputFile->Close();

}//close-GUICore::createHistList

/* Check if the file exists */
bool GUICore::FileExists(cchar fileName){

	if(1) this->createHistListWindow(fileName);
	else {
		cout << "\nThe file with the name provided: "
			<< fileName
			<< " doesn't exist in the path specified"
			<< endl;
		exit(1);
	}
	return 1;
}

/* Read the text file box */
cchar GUICore::ReadFile(){

	cchar fileName = txtBox->GetText();
	
	this->FileExists(fileName);

	return fileName;

}//close-GUICore::ReadFile()

void textBox_v3(){

	new GUICore(gClient->GetRoot(), 400, 400);
	
	TDirectory *td = new TDirectory();
	td->pwd();
	cout << td->GetFile();
	//cout << (td->OpenFile("samplefile.root"))->IsOpen();
	td->Print();


}//close-main
