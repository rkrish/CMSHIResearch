#!/bin/bash

echo "Ratio of Bound to Free PDF"

bound=BoundProton_208_EPPS16nlo/BoundProton_208_EPPS16nlo_HardQCDAll_Combined_2017-04-01-092356.root
free=FreeProton_208_CT14nlo/FreeProton_208_CT14nlo_HardQCDAll_Combined_2017-03-29-101654.root

bound=BoundAndFreeProton_208_EPPS16LO_CT14lo/BoundAndFreeProton_208_EPPS16LO_CT14lo_HardQCDAll_Combined_2017-03-20-114111.root
free=FreeProton_208_CT14nlo/FreeProton_208_CT14nlo_HardQCDAll_Combined_2017-03-29-101654.root
root -l "testRatio.C(\"$bound\", \"$free\")"

bound=BoundAndFreeProton_208_EPS09NLO_CT10nlo/BoundAndFreeProton_208_EPS09NLO_CT10nlo_HardQCDAll_Combined_2017-03-29-104604.root
free=FreeProton_208_CT10nlo/FreeProton_208_CT10nlo_HardQCDAll_Combined_2017-03-29-101654.root





