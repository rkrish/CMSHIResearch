/*
 * =====================================================================================
 *
 *       Filename:  TrackVsVertexpTSpectra.C
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/22/16 09:52:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

// ROOT Headers
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>

// C++ Headers
#include <iostream>
#include <vector>

using namespace std;

//class Test : public TCanvas{};

/* Normalizing Variable Binned Histograms */
TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles
		lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		/*
		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";
		 */

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist

/* Apply Cosmetics to the Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mColor, Style_t mStyle, double mSize)
{

	h->SetMarkerColor(mColor);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerSize(mSize);

	return h;
}

vector<TH1D*> processHistograms(TFile *file, double nEvents)
{

	vector<TH1D*> vh;
	const char *sPos_Trk, *sNeg_Trk, *sPos_Vtx, *sNeg_Vtx;
	TH1D *hPos_Trk, *hNeg_Trk, *hPos_Vtx, *hNeg_Vtx, 
	     *hVarPos_Trk, *hVarNeg_Trk, *hVarPos_Vtx, *hVarNeg_Vtx;

	// For Tracks
	sPos_Trk = "userAnalyzer/htrackPosPt";
	sNeg_Trk = "userAnalyzer/htrackNegPt";

	// For Vertices
	sPos_Vtx = "userAnalyzer/hvtxTracksPosPt";
	sNeg_Vtx = "userAnalyzer/hvtxTracksNegPt";

	hPos_Trk = (TH1D*)file->Get(sPos_Trk);
	hNeg_Trk = (TH1D*)file->Get(sNeg_Trk);

	hPos_Vtx = (TH1D*)file->Get(sPos_Vtx);
	hNeg_Vtx = (TH1D*)file->Get(sNeg_Vtx);

	hNeg_Vtx->Scale(0.0001);


	// Apply Variable Binning
	double varBinArr[] = { 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 3.2, 4.0, 4.8, 5.6, 6.4, 7.2, 9.6, 12.0, 14.4, 19.2, 24.0, 28.8, 35.2, 41.6, 48.0, 60.8, 73.6, 86.4, 103.6, 115.3, 130.0, 140, 150, 160, 170, 180, 190, 200 };

	// Get the number of variables bins declared
	const int nVarBins = sizeof(varBinArr)/sizeof(double)-1;
	cout << "Number of Variable Bins declared: " << nVarBins << endl;


	// Rebin the Histograms from normalized histograms
	hVarPos_Trk = (TH1D*)hPos_Trk->Rebin(nVarBins, "hVarPos_Trk", varBinArr);
	hVarNeg_Trk = (TH1D*)hNeg_Trk->Rebin(nVarBins, "hVarNeg_Trk", varBinArr);

	hVarPos_Vtx = (TH1D*)hPos_Vtx->Rebin(nVarBins, "hVarPos_Vtx", varBinArr);
	hVarNeg_Vtx = (TH1D*)hNeg_Vtx->Rebin(nVarBins, "hVarNeg_Vtx", varBinArr);
	
	// Normalize Variable Binned Histograms
	hVarPos_Trk = normalizeVarBinHist(hVarPos_Trk, nVarBins);	
	hVarNeg_Trk = normalizeVarBinHist(hVarNeg_Trk, nVarBins);	

	hVarPos_Vtx = normalizeVarBinHist(hVarPos_Vtx, nVarBins);	
	hVarNeg_Vtx = normalizeVarBinHist(hVarNeg_Vtx, nVarBins);	

	// Apply Cosmetics
	applyCosmetics(hVarPos_Trk, kBlue, 20, 1.4);
	applyCosmetics(hVarNeg_Trk, kRed, 20, 1.4);

	applyCosmetics(hVarPos_Vtx, kBlue, 24, 1.4);
	applyCosmetics(hVarNeg_Vtx, kRed, 24, 1.4);

	TH1D *hVarNegClone_Trk, *hVarNegClone_Vtx;

	hVarNegClone_Trk = (TH1D*)hVarNeg_Trk->Clone("hVarNegClone_Trk");
	hVarNegClone_Vtx = (TH1D*)hVarNeg_Vtx->Clone("hVarNegClone_Vtx");

	hVarNegClone_Trk->Divide(hVarPos_Trk);
	hVarNegClone_Vtx->Divide(hVarPos_Vtx);

	applyCosmetics(hVarNegClone_Trk, kGreen+3, 22, 1.4);
	applyCosmetics(hVarNegClone_Vtx, kGreen+3, 24, 1.4);

	vh.push_back(hVarNeg_Trk);
	vh.push_back(hVarPos_Trk);
	vh.push_back(hVarPos_Vtx);
	vh.push_back(hVarNeg_Vtx);
	vh.push_back(hVarNegClone_Vtx);
	vh.push_back(hVarNegClone_Trk);

	return vh;

}//close-

void calculateIntegral(TH1D *h, double &minpT, double &maxpT, double &int_val, double &int_err)
{


	double minBinNo, maxBinNo, err=0, integral_mean=0, integral_err=0; 
	minBinNo = h->GetXaxis()->FindBin(minpT);
	maxBinNo = h->GetXaxis()->FindBin(maxpT);

	cout << "Given: (BinNo, pT) , BinContent, BinCenter, BinWidth " 
	     << minpT << "\t"
	     << minBinNo << "\t" 
	     << h->GetBinContent(minBinNo) << "\t"
	     << h->GetBinCenter(minBinNo) << "\t"
	     << h->GetBinLowEdge(minBinNo) << "\t"
	     << h->GetBinWidth(minBinNo) << "\t"
	     << "\n";

	// Loop over the histogram to calculate integral(mean and error)
	for (int i = minBinNo; i < maxBinNo; i++ ){
		cout << h->GetBinLowEdge(i) << "," << h->GetBinContent(i) << "\n";
		integral_mean += h->GetBinContent(i); 
		err += (h->GetBinError(i))*(h->GetBinError(i));

	}//close-for

	integral_err = sqrt(err);

	int_val = integral_mean;
	int_err = integral_err;
}//close-calculateIntegral

void TrackVsVertexpTSpectra()
{

	TCanvas *c1 = new TCanvas("c1", " ", 900, 450);

	TFile *f = new TFile("output_pPbMinBiasUPC_1204_01_2016-04-19-131227.root", "r");
	TFile *f1 = new TFile("output_pPbHighpT_Track12_1204_01_2016-04-22-094010.root", "r");

	cout << processHistograms(f, 1000).size() << "\t xxxx\n";


	cout << "\n"
		<< "Is the file open? " 
		<< f->GetName() << "\t" << f->IsOpen() << "\t"
		<< "\n"
		<< f1->GetName() << "\t" << f1->IsOpen() << "\t"
		<< "\n\n";

	vector<TH1D*> vf = processHistograms(f, 1000);
	((TH1D*)processHistograms(f, 1000).at(0))->Draw();
	double nHist = processHistograms(f, 1000).size();
	for (int i=1; i < nHist; i++)
	{

		cout << "Hist Name: " << ((TH1D*)vf.at(i))->GetName() << "\n";
		//((TH1D*)processHistograms(f, 1000).at(i))->Draw("same");
	}

	TH1D* h = (TH1D*)vf.at(0);
	h->Scale(2);
	h->Draw();

	double minBinNo, maxBinNo, minpT=40, maxpT=100; 
	minBinNo = h->GetXaxis()->FindBin(minpT);
	maxBinNo = h->GetXaxis()->FindBin(maxpT);

	cout << "Given: (BinNo, pT) , BinContent, BinCenter, BinWidth " 
	     << minpT << "\t"
	     << minBinNo << "\t" 
	     << h->GetBinContent(minBinNo) << "\t"
	     << h->GetBinCenter(minBinNo) << "\t"
	     << h->GetBinLowEdge(minBinNo) << "\t"
	     << h->GetBinWidth(minBinNo) << "\t"
	     << "\n";

	double varBinArr[] = { 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 3.2, 4.0, 4.8, 5.6, 6.4, 7.2, 9.6, 12.0, 14.4, 19.2, 24.0, 28.8, 35.2, 41.6, 48.0, 60.8, 73.6, 86.4, 103.6, 115.3, 130.0, 140, 150, 160, 170, 180, 190, 200 };

	cout << minBinNo << ", " << maxBinNo << "\n";
	double integral_mean=0, integral_err=0;
	double err=0;

	for (int i = minBinNo; i < maxBinNo; i++ ){
		cout << h->GetBinLowEdge(i) << "," << h->GetBinContent(i) << "\n";
		integral_mean += h->GetBinContent(i); 
		err += (h->GetBinError(i))*(h->GetBinError(i));

	}//close-for

	integral_err = sqrt(err);
	cout << "Integral, Error " << integral_mean << ", "
	     << integral_err << "\n";

	double int_val=1, int_err=10;
	cout << "Before calculating in the function" << endl;
	cout << int_val << "\t" << int_err << endl;
	calculateIntegral(h, minpT, maxpT, int_val, int_err);
	cout << "After calculating in the function" << endl;
	cout << int_val << "\t" << int_err << endl;

}//close-main


