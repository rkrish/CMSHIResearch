## June 30, 2017
GUIMenu : To create a basic Menu, so when a user initializes with a TCanvas full of objects, change some core properties without having to get into code and write them into a file.
GUIMenu (TCanvas)



### processROOTFiles : To perform operations on the same histograms, given the path from the same root file. file[h1a, h1b].root, perform an operation like h1a/h1b (or) h1a+h1b (or) k1xh1a-k2xh1b, etc. 

---
I30062017-1 : Check if the object read from the file is a TH1D and only then start pulling them into the program variables, else continue
