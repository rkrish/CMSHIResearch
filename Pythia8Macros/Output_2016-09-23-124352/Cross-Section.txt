

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
15.5285	  6.57151e-06	  0.776425	  0.000588106	  MRST-LO(2008-CentralMember)_HardQCDMix_10To20_2016-09-23-124352
0.00056324	  7.27597e-15	  2.8162e-05	  1.9569e-08	  MRST-LO(2008-CentralMember)_HardQCDMix_120To170_2016-09-23-124352
9.4699e-05	  1.88646e-16	  4.73495e-06	  3.15099e-09	  MRST-LO(2008-CentralMember)_HardQCDMix_170To230_2016-09-23-124352
1.0984	  3.54819e-08	  0.05492	  4.32142e-05	  MRST-LO(2008-CentralMember)_HardQCDMix_20To30_2016-09-23-124352
1.79473e-05	  6.83083e-18	  8.97365e-07	  5.99598e-10	  MRST-LO(2008-CentralMember)_HardQCDMix_230To300_2016-09-23-124352
3.6843e-06	  2.80561e-19	  1.84215e-07	  1.21517e-10	  MRST-LO(2008-CentralMember)_HardQCDMix_300To380_2016-09-23-124352
0.25386	  1.62081e-09	  0.012693	  9.23612e-06	  MRST-LO(2008-CentralMember)_HardQCDMix_30To50_2016-09-23-124352
1.01985e-06	  2.6143e-20	  5.09925e-08	  3.70938e-11	  MRST-LO(2008-CentralMember)_HardQCDMix_380To10000_2016-09-23-124352
0.031219	  2.35033e-11	  0.00156095	  1.11221e-06	  MRST-LO(2008-CentralMember)_HardQCDMix_50To80_2016-09-23-124352
0.0038892	  3.52691e-13	  0.00019446	  1.36245e-07	  MRST-LO(2008-CentralMember)_HardQCDMix_80To120_2016-09-23-124352
