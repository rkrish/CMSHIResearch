

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
21.898	  1.21259e-05	  1.0949	  0.000798878	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_10To20_2016-09-16-150708
0.00041806	  4.01436e-15	  2.0903e-05	  1.45356e-08	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_120To170_2016-09-16-150708
5.576e-05	  7.40702e-17	  2.788e-06	  1.97444e-09	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_170To230_2016-09-16-150708
1.49303	  5.41154e-08	  0.0746515	  5.33683e-05	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_20To30_2016-09-16-150708
8.2531e-06	  1.7319e-18	  4.12655e-07	  3.01915e-10	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_230To300_2016-09-16-150708
1.31358e-06	  4.85143e-20	  6.5679e-08	  5.0531e-11	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_300To380_2016-09-16-150708
0.32083	  2.33259e-09	  0.0160415	  1.10801e-05	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_30To50_2016-09-16-150708
2.6233e-07	  2.17549e-21	  1.31165e-08	  1.07004e-11	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_380To10000_2016-09-16-150708
0.034391	  2.64638e-11	  0.00171955	  1.18018e-06	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_50To80_2016-09-16-150708
0.0035673	  2.77301e-13	  0.000178365	  1.20809e-07	  NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_80To120_2016-09-16-150708
