

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.289	  1.25773e-05	  1.11445	  0.000813611	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-01-30-100453
0.00043215	  4.27986e-15	  2.16075e-05	  1.50085e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-01-30-100453
5.6888e-05	  8.25995e-17	  2.8444e-06	  2.08503e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-01-30-100453
1.53674	  5.82542e-08	  0.076837	  5.53716e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-01-30-100453
8.3428e-06	  1.75611e-18	  4.1714e-07	  3.04018e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-01-30-100453
1.32273e-06	  4.72956e-20	  6.61365e-08	  4.98923e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-01-30-100453
0.33264	  2.50276e-09	  0.016632	  1.14771e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-01-30-100453
2.6778e-07	  2.1468e-21	  1.3389e-08	  1.06297e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-01-30-100453
0.035903	  2.74952e-11	  0.00179515	  1.20296e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-01-30-100453
0.0037212	  2.92893e-13	  0.00018606	  1.24159e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-01-30-100453
