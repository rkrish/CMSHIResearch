

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.485	  1.2662e-05	  1.12425	  0.000816346	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-06-163126
0.00039967	  3.49246e-15	  1.99835e-05	  1.35578e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-06-163126
5.4911e-05	  6.69414e-17	  2.74555e-06	  1.87703e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-06-163126
1.4606	  4.90714e-08	  0.07303	  5.08203e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-06-163126
8.5216e-06	  1.69595e-18	  4.2608e-07	  2.98765e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-06-163126
1.44268e-06	  5.25949e-20	  7.2134e-08	  5.26132e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-06-163126
0.30734	  2.05599e-09	  0.015367	  1.04024e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-06-163126
3.1812e-07	  2.9299e-21	  1.5906e-08	  1.24179e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-06-163126
0.032457	  2.28317e-11	  0.00162285	  1.09621e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-06-163126
0.0033639	  2.3688e-13	  0.000168195	  1.11657e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-06-163126
