

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.472	  1.26491e-05	  1.1236	  0.00081593	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-01-140536
0.00043194	  4.27782e-15	  2.1597e-05	  1.50049e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-01-140536
5.6892e-05	  8.24536e-17	  2.8446e-06	  2.08319e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-01-140536
1.46046	  4.90595e-08	  0.073023	  5.08142e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-01-140536
8.345e-06	  1.75783e-18	  4.1725e-07	  3.04167e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-01-140536
1.32275e-06	  4.73044e-20	  6.61375e-08	  4.9897e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-01-140536
0.30725	  2.05477e-09	  0.0153625	  1.03993e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-01-140536
2.6768e-07	  2.14659e-21	  1.3384e-08	  1.06291e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-01-140536
0.033492	  2.4238e-11	  0.0016746	  1.12946e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-01-140536
0.0037224	  2.93159e-13	  0.00018612	  1.24215e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-01-140536
