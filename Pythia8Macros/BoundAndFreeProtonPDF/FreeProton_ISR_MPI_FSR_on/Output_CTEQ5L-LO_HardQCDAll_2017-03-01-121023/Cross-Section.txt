

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
38.809	  3.98184e-05	  1.94045	  0.00144766	  CTEQ5L-LO_HardQCDAll_10To20_2017-03-01-121023
0.00120757	  3.31315e-14	  6.03785e-05	  4.17584e-08	  CTEQ5L-LO_HardQCDAll_120To170_2017-03-01-121023
0.000197656	  8.29576e-16	  9.8828e-06	  6.60771e-09	  CTEQ5L-LO_HardQCDAll_170To230_2017-03-01-121023
2.7274	  1.94471e-07	  0.13637	  0.00010117	  CTEQ5L-LO_HardQCDAll_20To30_2017-03-01-121023
3.7084e-05	  2.94492e-17	  1.8542e-06	  1.24497e-09	  CTEQ5L-LO_HardQCDAll_230To300_2017-03-01-121023
7.6994e-06	  1.24062e-18	  3.8497e-07	  2.5553e-10	  CTEQ5L-LO_HardQCDAll_300To380_2017-03-01-121023
0.61687	  9.82253e-09	  0.0308435	  2.27371e-05	  CTEQ5L-LO_HardQCDAll_30To50_2017-03-01-121023
2.2452e-06	  1.24872e-19	  1.1226e-07	  8.10692e-11	  CTEQ5L-LO_HardQCDAll_380To10000_2017-03-01-121023
0.072909	  1.26062e-10	  0.00364545	  2.57582e-06	  CTEQ5L-LO_HardQCDAll_50To80_2017-03-01-121023
0.008678	  1.74145e-12	  0.0004339	  3.02746e-07	  CTEQ5L-LO_HardQCDAll_80To120_2017-03-01-121023
