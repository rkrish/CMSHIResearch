

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
12.9291	  4.78849e-06	  0.646455	  0.000502022	  CTEQ5L-LO_HardQCDMix_10To20_2017-02-21-112359
0.00065843	  9.82664e-15	  3.29215e-05	  2.27418e-08	  CTEQ5L-LO_HardQCDMix_120To170_2017-02-21-112359
0.000109943	  2.54306e-16	  5.49715e-06	  3.65849e-09	  CTEQ5L-LO_HardQCDMix_170To230_2017-02-21-112359
1.04699	  3.26513e-08	  0.0523495	  4.14547e-05	  CTEQ5L-LO_HardQCDMix_20To30_2017-02-21-112359
2.0305e-05	  8.91808e-18	  1.01525e-06	  6.85108e-10	  CTEQ5L-LO_HardQCDMix_230To300_2017-02-21-112359
4.0102e-06	  3.75919e-19	  2.0051e-07	  1.4066e-10	  CTEQ5L-LO_HardQCDMix_300To380_2017-02-21-112359
0.26115	  2.04303e-09	  0.0130575	  1.03696e-05	  CTEQ5L-LO_HardQCDMix_30To50_2017-02-21-112359
1.05218e-06	  2.789e-20	  5.2609e-08	  3.83131e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2017-02-21-112359
0.034441	  3.02925e-11	  0.00172205	  1.26267e-06	  CTEQ5L-LO_HardQCDMix_50To80_2017-02-21-112359
0.004479	  4.7352e-13	  0.00022395	  1.57867e-07	  CTEQ5L-LO_HardQCDMix_80To120_2017-02-21-112359
