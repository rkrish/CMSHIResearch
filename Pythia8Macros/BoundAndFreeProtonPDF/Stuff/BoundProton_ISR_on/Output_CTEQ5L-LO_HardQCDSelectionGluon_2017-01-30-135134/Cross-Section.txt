

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.297	  1.25883e-05	  1.11485	  0.000813965	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-01-30-135134
0.00043203	  4.28045e-15	  2.16015e-05	  1.50096e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-01-30-135134
5.689e-05	  8.30404e-17	  2.8445e-06	  2.09059e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-01-30-135134
1.53672	  5.82401e-08	  0.076836	  5.53649e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-01-30-135134
8.345e-06	  1.757e-18	  4.1725e-07	  3.04095e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-01-30-135134
1.32269e-06	  4.72918e-20	  6.61345e-08	  4.98903e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-01-30-135134
0.33264	  2.50365e-09	  0.016632	  1.14792e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-01-30-135134
2.6784e-07	  2.14722e-21	  1.3392e-08	  1.06307e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-01-30-135134
0.035885	  2.74717e-11	  0.00179425	  1.20245e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-01-30-135134
0.0037232	  2.93208e-13	  0.00018616	  1.24226e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-01-30-135134
