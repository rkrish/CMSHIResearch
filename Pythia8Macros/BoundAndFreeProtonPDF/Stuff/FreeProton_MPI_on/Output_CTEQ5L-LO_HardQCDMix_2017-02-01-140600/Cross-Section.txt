

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
12.929	  4.78722e-06	  0.64645	  0.000501955	  CTEQ5L-LO_HardQCDMix_10To20_2017-02-01-140600
0.00065852	  9.82841e-15	  3.2926e-05	  2.27439e-08	  CTEQ5L-LO_HardQCDMix_120To170_2017-02-01-140600
0.000109937	  2.61133e-16	  5.49685e-06	  3.70727e-09	  CTEQ5L-LO_HardQCDMix_170To230_2017-02-01-140600
1.04679	  3.26472e-08	  0.0523395	  4.14521e-05	  CTEQ5L-LO_HardQCDMix_20To30_2017-02-01-140600
2.0302e-05	  8.91421e-18	  1.0151e-06	  6.84959e-10	  CTEQ5L-LO_HardQCDMix_230To300_2017-02-01-140600
4.0096e-06	  3.37792e-19	  2.0048e-07	  1.33336e-10	  CTEQ5L-LO_HardQCDMix_300To380_2017-02-01-140600
0.26111	  2.04141e-09	  0.0130555	  1.03655e-05	  CTEQ5L-LO_HardQCDMix_30To50_2017-02-01-140600
1.0523e-06	  2.78706e-20	  5.2615e-08	  3.82998e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2017-02-01-140600
0.034448	  3.03023e-11	  0.0017224	  1.26288e-06	  CTEQ5L-LO_HardQCDMix_50To80_2017-02-01-140600
0.0044788	  4.72782e-13	  0.00022394	  1.57744e-07	  CTEQ5L-LO_HardQCDMix_80To120_2017-02-01-140600
