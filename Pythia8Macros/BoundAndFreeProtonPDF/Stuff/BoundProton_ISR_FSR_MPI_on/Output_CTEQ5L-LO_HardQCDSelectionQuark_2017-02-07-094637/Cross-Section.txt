

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
1.97318	  1.18882e-07	  0.098659	  7.91008e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-07-094637
0.000182306	  7.75997e-16	  9.1153e-06	  6.39077e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-07-094637
3.6653e-05	  2.69616e-17	  1.83265e-06	  1.19123e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-07-094637
0.174042	  8.6393e-10	  0.0087021	  6.74315e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-07-094637
8.2919e-06	  1.34613e-18	  4.14595e-07	  2.66175e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-07-094637
2.0335e-06	  7.89837e-20	  1.01675e-07	  6.44751e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-07-094637
0.046931	  6.43688e-11	  0.00234655	  1.84061e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-07-094637
7.1621e-07	  1.15777e-20	  3.58105e-08	  2.4685e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-07-094637
0.0069712	  1.33273e-12	  0.00034856	  2.64846e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-07-094637
0.00104849	  3.19838e-14	  5.24245e-05	  4.10287e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-07-094637
