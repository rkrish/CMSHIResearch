

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.298	  1.25824e-05	  1.1149	  0.000813776	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-01-141035
0.00043206	  4.27899e-15	  2.1603e-05	  1.5007e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-01-141035
5.6903e-05	  8.26406e-17	  2.84515e-06	  2.08555e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-01-141035
1.53675	  5.8265e-08	  0.0768375	  5.53767e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-01-141035
8.3442e-06	  1.75765e-18	  4.1721e-07	  3.04151e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-01-141035
1.32343e-06	  4.73521e-20	  6.61715e-08	  4.99221e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-01-141035
0.33256	  2.50231e-09	  0.016628	  1.14761e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-01-141035
2.678e-07	  2.14763e-21	  1.339e-08	  1.06317e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-01-141035
0.035906	  2.74975e-11	  0.0017953	  1.20301e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-01-141035
0.0037221	  2.92941e-13	  0.000186105	  1.24169e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-01-141035
