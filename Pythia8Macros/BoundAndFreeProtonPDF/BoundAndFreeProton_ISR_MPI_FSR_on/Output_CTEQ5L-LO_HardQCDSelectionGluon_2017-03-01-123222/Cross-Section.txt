

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.427	  1.20275e-05	  1.12135	  0.000795631	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-03-01-123222
0.00041563	  3.85393e-15	  2.07815e-05	  1.42421e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-03-01-123222
5.6002e-05	  7.79573e-17	  2.8001e-06	  2.02559e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-03-01-123222
1.49936	  5.10859e-08	  0.074968	  5.1853e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-03-01-123222
8.4618e-06	  1.74623e-18	  4.2309e-07	  3.03161e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-03-01-123222
1.38754e-06	  5.10849e-20	  6.9377e-08	  5.18525e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-03-01-123222
0.31968	  2.17528e-09	  0.015984	  1.06999e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-03-01-123222
2.9336e-07	  2.54658e-21	  1.4668e-08	  1.15771e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-03-01-123222
0.034112	  2.46132e-11	  0.0017056	  1.13817e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-03-01-123222
0.0035369	  2.62411e-13	  0.000176845	  1.17521e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-03-01-123222
