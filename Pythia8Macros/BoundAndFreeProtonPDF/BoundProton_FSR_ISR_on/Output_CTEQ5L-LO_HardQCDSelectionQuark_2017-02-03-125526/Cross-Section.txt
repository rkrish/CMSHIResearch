

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
1.97371	  1.10008e-07	  0.0986855	  7.60912e-05	  CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-02-03-125526
0.000182239	  7.75175e-16	  9.11195e-06	  6.38739e-09	  CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-02-03-125526
3.6656e-05	  2.68911e-17	  1.8328e-06	  1.18967e-09	  CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-02-03-125526
0.174025	  8.63351e-10	  0.00870125	  6.74089e-06	  CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-02-03-125526
8.2902e-06	  1.36829e-18	  4.1451e-07	  2.68356e-10	  CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-02-03-125526
2.0335e-06	  8.36513e-20	  1.01675e-07	  6.63528e-11	  CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-02-03-125526
0.046944	  6.43903e-11	  0.0023472	  1.84091e-06	  CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-02-03-125526
7.1612e-07	  1.15729e-20	  3.5806e-08	  2.46799e-11	  CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-02-03-125526
0.0069676	  1.33113e-12	  0.00034838	  2.64687e-07	  CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-02-03-125526
0.00104856	  3.17545e-14	  5.2428e-05	  4.08814e-08	  CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-02-03-125526
