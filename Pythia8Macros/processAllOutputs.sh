#!/bin/bash
source ~/.BashFormatting.sh

if [[ $# == 0 ]];then

	echo -e "${fcRed} Should provide the path to a directory ${fc}"
	echo -e "${fcRed} This path should be the same as in PDFStudyData.sh <- getData.sh <-Cross-Section.txt ${fc}"
	echo ; echo ; 
	echo "==========================="
	echo "$0 Output_2016-08-30-140902"
	echo "==========================="

	exit 1
fi

dirPath=$1

count=1
source ./PDFStudyData.sh
echo "The number of declared values in the data file, if OK, press y,else it exits"
echo "Number of sets(PDF, Cross-Sectios):  ${nFiles}"
read check

if [ "${check}" == "y" ]
then 
	echo " Proceeding with next steps ? "
else 
	echo -e "${fBlink}Invalid Option${fc}"
	exit 1
fi


echo ; echo ; 
for i in `seq 1 ${nFiles}`
do
	echo "Looping over the set: "  $i
	testS="fileS$i[@]"
	testV="Val$i[@]"
	echo "in loop: " ${!testS}
	echo ; 
	echo "in loop: " ${!testV}
	size_testS=`echo ${!testS} | tr "," "\n" | wc -l`
	size_testV=`echo ${!testV} | tr "," "\n" | wc -l`
	echo ; echo ; 
	


ss=`echo "{${!testS}}"`
vv=`echo "{${!testV}}"`
#ss_size=${#testS[@]}


if [[ ${size_testS} != ${size_testV} ]]; then
	echo -e "${fcRed}\n====================================================================================${fc}"
	echo -e "${fcRed} ---- ERROR :Array sizes don't match i.e. Number of files ${size_testV}!= Number of cross-sections ${size_testS}${fc}"
	echo -e "${fcRed}====================================================================================${fc}"
	
else 
outfile=`echo "${!testS}" | tr "," " " | awk '{print $1}' | sed 's/_\([0-9]\+To[0-9]\+\)/_Combined/g' ` 
echo "Output file name: " ${outfile}

echo -e "\n\nProcessing the Next List in sequence: " ${count}
echo "========================================================"
root -b -q "makeCombinedPtHatSample.C+(${size_testS}, ${vv}, ${ss}, ${outfile})"
echo ; echo ; 
let count=count+1
fi
done

echo "The following files are the Combined pT Hat Results available in the present directory"
echo "=============================================="
ls -ltrh ${dirPath}/*Combined*.root 
echo "=============================================="

