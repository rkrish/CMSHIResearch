// C++ Headers
#include <iostream>
#include <vector>
#include <map>


// ROOT GUI Headers
#include <TApplication.h>
#include <TGClient.h>
#include <TGButton.h>
//#include <TGTextButton.h>
#include <TGTextEntry.h>
#include <TGWindow.h>
#include <TDirectory.h>

// ROOT GUI Headers
#include <TFile.h>
#include <TH1D.h>
#include <TRegexp.h>

typedef const char* cchar;

using namespace std;


class processHist : public TGMainFrame{

	public:
		processHist (const TGWindow *p, UInt_t w, UInt_t h );

};//close-processHist


processHist::processHist(const TGWindow *p, UInt_t w, UInt_t h){

	//SetWindowName(windowName);
	SetWMSizeHints(200, 500, 300, 1000, 1, 1);
	MapSubwindows();

	Resize(GetDefaultSize());
	MapWindow();

}//close-processHist constructor


class histListWindow : public TGMainFrame{

	public:
		histListWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar, vector<cchar> );
		map<cchar, TGCheckButton*> mcgbtn;
		cchar windowName;

};

histListWindow :: histListWindow(const TGWindow *p, UInt_t w, UInt_t h, cchar windowName, vector<cchar> vh1Names){

	TGVerticalFrame *hframe = new TGVerticalFrame(this);
	AddFrame(hframe);

	for (cchar h1Name : vh1Names){
		mcgbtn[h1Name] = new TGCheckButton(hframe, Form("&%s", h1Name), 200);
		hframe->AddFrame(mcgbtn[h1Name], new TGLayoutHints(kLHintsLeft, 1, 1, 1, 1));
	}

	SetWindowName(windowName);
	SetWMSizeHints(400, 500, 400, 1000, 1, 1);
	MapSubwindows();

	Resize(GetDefaultSize());
	MapWindow();

}//close;histListWindow-constructor

class GUICore : public TGMainFrame{

	private :
		enum {clear, getHist, divide};

	public :
		GUICore(const TGWindow *p, UInt_t w, UInt_t h);
		cchar ReadFile();
		bool FileExists(cchar);
		void createHistListWindow(cchar);
		map<cchar, TGTextButton*> btn;
		TGTextEntry *txtBox;
		void Divide();
		void Divide(TH1D *hNum, TH1D *hDenom);

};//close-textEntry

GUICore::GUICore(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h){


   	TGVerticalFrame *vframe = new TGVerticalFrame(this);
   	AddFrame(vframe, new TGLayoutHints(10, 20, 20, 20));

	txtBox = new TGTextEntry(this);
	txtBox->DrawBorder();
	vframe->AddFrame(txtBox, new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));

   	//TGHorizontalFrame *hframe = new TGHorizontalFrame(this);
   	//AddFrame(hframe, new TGLayoutHints(10, 20, 20, 20));

	//vframe->AddFrame(hframe);

	btn["getHist"] = new TGTextButton(this, "&Get Histograms");
   	vframe->AddFrame(btn["getHist"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["getHist"]->Connect("Clicked()", "GUICore", this, "ReadFile()");

	btn["clear"] = new TGTextButton(this, "&Clear", clear);
   	vframe->AddFrame(btn["clear"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["clear"]->Connect("Clicked()", "GUICore", this, "ReadFile()");

	btn["divide"] = new TGTextButton(this, "&Divide", divide);
   	vframe->AddFrame(btn["divide"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["divide"]->Connect("Clicked()", "GUICore", this, "Divide()");


	btn["divide2"] = new TGTextButton(this, "&Divide2", divide);
   	vframe->AddFrame(btn["divide2"], new TGLayoutHints(kLHintsLeft, 5, 5, 5, 5));
	btn["divide2"]->Connect("Clicked()", "GUICore", this, "Divide()");

	SetWindowName("Input the file");
	SetWMSizeHints(250, 150, 250, 150, 1, 1);
	MapSubwindows();
	MapWindow();

}//close-GUICore

//void GUICore::Divide(TH1D *hNum, TH1D *hDenom){
void GUICore::Divide(){

	TGCheckButton *tg = (TGCheckButton*)gTQSender;
	cout << "button name: " << tg->GetName() << endl;
	cout << "divide clicked" << endl;
}

void GUICore::createHistListWindow(cchar fileName){

	TFile *inputFile= new TFile(fileName, "r");
	cout << "Is the file open? " << inputFile->IsOpen() << endl;


		vector<cchar> vh1Names;
		inputFile->cd();
		TDirectory *cdir = gDirectory;
		TIter nextkey( cdir->GetListOfKeys() );
		TKey *key, *oldkey;

		while( (key = (TKey*)nextkey() )){

			TString s0((char*)key->GetName());
			TPRegexp r2("([a-zA-Z0-9-().+/]+)_([a-zA-Z0-9-().+]+)_([a-zA-Z0-9-().+]+)");
			//charge = ((TObjString*)(r2.MatchS(s0)->At(1)))->GetString();
			cout << key->GetName() << endl;
			vh1Names.push_back(key->GetName());

		}//Loop: Histogram objects

	histListWindow *h = new histListWindow(gClient->GetRoot(), 500, 500, inputFile->GetName(), vh1Names);
	inputFile->Close();

}//close-GUICore::createHistList

/* Check if the file exists */
bool GUICore::FileExists(cchar fileName){

	if(1) this->createHistListWindow(fileName);
	else {
		cout << "\nThe file with the name provided: "
			<< fileName
			<< " doesn't exist in the path specified"
			<< endl;
		exit(1);
	}
	return 1;
}

/* Read the text file box */
cchar GUICore::ReadFile(){

	cchar fileName = txtBox->GetText();
	
	this->FileExists(fileName);

	return fileName;

}//close-GUICore::ReadFile()

void textBox_v2(){

	new GUICore(gClient->GetRoot(), 300, 300);
	
	TDirectory *td = new TDirectory();
	td->pwd();
	cout << td->GetFile();
	//cout << (td->OpenFile("samplefile.root"))->IsOpen();
	td->Print();


}//close-main
