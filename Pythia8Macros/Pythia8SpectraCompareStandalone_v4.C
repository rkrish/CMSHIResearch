
// C++ Headers
#include <iostream>
#include <map>
#include <vector>

// ROOT Headers
#include <TFile.h>
#include <TH1D.h>
#include <TDirectory.h>
#include <TPRegexp.h>
#include "TClonesArray.h"
#include <TObjString.h>

#include <guiDrawWhenSelected_v4.C>

using namespace std;

typedef const char* cchar;
typedef map<string, map<string, map<string, TH1D*>>> map3D;
typedef map<string, vector<TH1D*>> MVh1;


/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics

MVh1 fracYield(vector<string> vPDFList, map3D mh, string partonType, MVh1 mhRes)
{

	cout << "Calculating ratios for: " << Form("%s/HardQCDAll for all PDFs (+, -, -/+)", partonType.c_str()) << endl;

	Style_t mStyle[] = {20, 24, 29}; int styleIt=0;
	map<string, TH1D*> hNumer, hDenom, hRatio;
	for (string pdf : vPDFList){
		for (auto type : {"Pos", "Neg", "Ratio-NegByPos"}){

			hNumer[type] = (TH1D*)mh[type][pdf][partonType]->Clone(pdf.c_str());
			hDenom[type] = (TH1D*)mh[type][pdf]["HardQCDAll"]->Clone(pdf.c_str());

			hRatio[type] = (TH1D*) hNumer[type]->Clone(pdf.c_str());
			hRatio[type]->Divide(hDenom[type]);

			applyCosmetics(hRatio[type], hNumer[type]->GetMarkerColor(), mStyle[(styleIt++)], 1.8 );

		}//close-for : type
		mhRes[pdf] = {hRatio["Pos"], hRatio["Neg"], hRatio["Ratio-NegByPos"]};
		styleIt = 0;
	}//close-for : Loop(PDFList)


	return mhRes;

}//close-PartonYieldFraction

/* Get the histograms */
void fetchHistForAllPDFs(TFile *file, string label)
{}//close-processHist

/* Main Program */
void Pythia8SpectraCompareStandalone_v4()
{

	map<cchar,cchar> fileName;
	map<cchar, TFile*> file;

	file["Gluon"] = new TFile("ProcessedOutput_pdfs-12-HardQCDSelectionGluon-2016-09-13-193831.root", "r");
	file["Quark"] = new TFile("ProcessedOutput_pdfs-12-HardQCDSelectionQuark-2016-09-12-075049.root", "r");
	file["HardQCDAll"] = new TFile("ProcessedOutput_pdfs-12-HardQCDAll-2016-09-10-123538.root", "r");

	vector<string> vPDFList ={
		"GRV94L-LO",
		"CTEQ5L-LO",

		//"MRST-LO(2007)",
		//"MRST-LO(2008)",

		"MRST-LO(2008-CentralMember)",
		"MRST-NLO(2008-CentralMember)",

		"CTEQ6L-NLO",
		"CTEQ6L1-LO",
		"CTEQ66.00",
		"CT09MC1-LO",
		"CT09MC2-NLO",

		//"CT09MCS-NLO",
		//"NNPDF2.3-QCD+QED-LO13"

		"NNPDF2.3-QCD+QED-LO14",
		"NNPDF2.3-QCD+QED-NLO",
		"NNPDF2.3-QCD+QED-NNLO"
	};

	map<string, const char*> hName;
	map<string, TCanvas*> mCanvas;
	map3D mh, mhC;

	map<string, string> ms;
	ms["Gluon"] = "HardQCDSelectionGluon";	ms["Quark"] = "HardQCDSelectionQuark";	ms["HardQCDAll"] = "HardQCDAll";

	
	map<string, TLegend*> mleg;
	for (auto prSw : {"Gluon", "Quark", "HardQCDAll"}){
		for (auto type : {"Pos", "Neg", "Ratio-NegByPos"}){
			//mleg[prSw] = new TLegend(0.9, 0.6, 0.7, 0.8);
			//mCanvas[prSw] = new TCanvas(prSw, prSw, 900, 465);

			for (string pdf : vPDFList){

				hName[prSw] = Form("%s_%s_%s", type, pdf.c_str(), ms[prSw].c_str());
				mh[type][pdf][prSw] = (TH1D*)file[prSw]->Get(hName[prSw]);
				mh[type][pdf][prSw]->SetFillColor(kGreen);
				mhC[type][pdf][prSw] = (TH1D*)mh[type][pdf][prSw]->Clone();
				mhC[type][pdf][prSw]->SetFillColor(mh[type][pdf][prSw]->GetMarkerColor());
				mhC[type][pdf][prSw]->SetFillStyle(3001);

				/*
				mCanvas[prSw]->cd();
				mleg[prSw]->AddEntry(mhC[type][pdf][prSw], Form("%s-%s", pdf.c_str(), prSw), "lep");
				mhC[type][pdf][prSw]->Draw("E3 same");
				mleg[prSw]->Draw();
				gPad->SetGrid();
				*/

			}//close-for: Loop(PDFList)
		}//close-for: Loop(prSw)
	}//close-for: Loop(type)

	TCanvas *c = new TCanvas("c", " ", 900, 460);
	mleg["all"] = new TLegend(0.9, 0.6, 0.7, 0.8);
	c->cd();
	map<string, MVh1> mhYield;
	mhYield["Quark"] = fracYield(vPDFList, mh, "Quark", mhYield["Quark"]);
	mhYield["Gluon"] = fracYield(vPDFList, mh, "Gluon", mhYield["Gluon"]);
	if (1){
	for (string pdf : vPDFList){

	//	mhYield["Quark"][pdf].at(1)->Draw("same");
	//	mhYield["Quark"][pdf].at(0)->Draw("same");
	//	mhYield["Gluon"][pdf].at(1)->Draw("same");
	//	mhYield["Gluon"][pdf].at(0)->Draw("same");
		mhYield["Quark"][pdf].at(2)->Draw("same");
		//mhYield["Gluon"][pdf].at(2)->Draw("same");

		mleg["all"]->AddEntry(mhYield["Quark"][pdf].at(0), pdf.c_str(), "lep");
		mleg["all"]->Draw();
		gPad->SetGrid();
	}
	}


	GUICore *gQuark = new GUICore(vPDFList, mhYield["Quark"], mhYield["Gluon"], gClient->GetRoot(), 500, 500);
	gQuark->RawPlots(mhC);
	//gQuark = new GUICore(vPDFList, mhYield["Quark"], mhYield["Gluon"], gClient->GetRoot(), 500, 500);
}//close-main

