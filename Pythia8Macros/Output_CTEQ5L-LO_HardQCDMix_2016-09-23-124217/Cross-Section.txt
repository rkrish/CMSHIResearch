

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
16.2504	  7.09134e-06	  0.81252	  0.000610924	  CTEQ5L-LO_HardQCDMix_10To20_2016-09-23-124217
0.00057287	  7.46641e-15	  2.86435e-05	  1.98234e-08	  CTEQ5L-LO_HardQCDMix_120To170_2016-09-23-124217
9.671e-05	  1.97444e-16	  4.8355e-06	  3.22363e-09	  CTEQ5L-LO_HardQCDMix_170To230_2016-09-23-124217
1.12393	  3.6636e-08	  0.0561965	  4.39114e-05	  CTEQ5L-LO_HardQCDMix_20To30_2016-09-23-124217
1.8386e-05	  7.11041e-18	  9.193e-07	  6.11745e-10	  CTEQ5L-LO_HardQCDMix_230To300_2016-09-23-124217
3.7836e-06	  3.00738e-19	  1.8918e-07	  1.25811e-10	  CTEQ5L-LO_HardQCDMix_300To380_2016-09-23-124217
0.25787	  1.83084e-09	  0.0128935	  9.81631e-06	  CTEQ5L-LO_HardQCDMix_30To50_2016-09-23-124217
1.04819e-06	  2.77185e-20	  5.24095e-08	  3.81951e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2016-09-23-124217
0.031604	  2.37489e-11	  0.0015802	  1.11801e-06	  CTEQ5L-LO_HardQCDMix_50To80_2016-09-23-124217
0.003944	  3.57701e-13	  0.0001972	  1.37209e-07	  CTEQ5L-LO_HardQCDMix_80To120_2016-09-23-124217
