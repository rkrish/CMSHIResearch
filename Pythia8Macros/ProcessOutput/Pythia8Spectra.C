#pragma once

// C++ Headers
#include <iostream>
#include <vector>
#include <map>

// ROOT Headers
#include <TGraph.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TPRegexp.h>
#include "TClonesArray.h"
#include <TObjString.h>
#include <TString.h>

using namespace std;

typedef const char* cchar;
typedef map<string, map<string, TH1D*>> map2Dh1;
typedef map<string, map2Dh1> map3Dh1;

/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics


/* Show all the initialized map3D Labels */
void map3DLabels(map3Dh1 hTrack)
{

	for(map3Dh1::iterator it = hTrack.begin(); it != hTrack.end(); ++it){
		for(map2Dh1::iterator it1 = (&(it->second))->begin(); it1 != (&(it->second))->end(); ++it1){
			for (map<string, TH1D*>::iterator it2 = (&(it1->second))->begin(); it2 != (&(it1->second))->end(); ++it2){
				cout << "[\"" << it->first << "\"]" ;
				cout << "[\"" << it1->first << "\"]" ;
				cout << "[\"" << it2->first<<"\"]";
				cout << endl;
				}
			}
	}

}//close-map3DLabels

map3Dh1 processHist(cchar fileName, map3Dh1 mh1, string pdfName, string procSw)
{

	TFile *file = new TFile(fileName, "r");

	cout << "Is the file open? (1: yes, 0:no)\t" 
	     << file->GetName() << "\t"
	     << file->IsOpen() << endl;

	cchar sPathPos, sPathAll;
	sPathPos = "QCDAna/pchspectrum";
	sPathAll = "QCDAna/chspectrum";


	TH1D *hPos, *hNeg, *hRatio, *hAll;
	hPos = (TH1D*)file->Get(sPathPos)->Clone("hPos");
	hAll = (TH1D*)file->Get(sPathAll)->Clone("hAll");

	hNeg = (TH1D*)hPos->Clone("hNeg");
	hNeg->Add(hAll, hPos, 1, -1);
	hRatio = (TH1D*)hNeg->Clone("hRatio");

	hRatio->Divide(hPos);

	mh1[pdfName][procSw]["Pos"] = (TH1D*)hPos;
	mh1[pdfName][procSw]["Neg"] = (TH1D*)hNeg;
	mh1[pdfName][procSw]["Ratio-Neg/Pos"] = (TH1D*)hRatio;

	//file->Close();

	return mh1;

}//close-processHist

void Pythia8Spectra(string pdfName){

	cout << "from main " << pdfName << endl;

	vector<cchar> vfileName;
	vfileName = { 

		"CT09MC2-NLO_HardQCDAll_Combined_2016-08-30-140902.root",  "MRST-LO(2008-CentralMember)_HardQCDAll_Combined_2016-08-30-140902.root",
		"CTEQ6L1-LO_HardQCDAll_Combined_2016-08-30-140902.root"   ,
		"GRV94L-LO_HardQCDAll_Combined_2016-08-30-140902.root"

	};


	map3Dh1 mh1;
	vector<string> vpdfList;
	for (auto fName : vfileName)
	{

		TString s0(fName);
		TPRegexp r0("([a-zA-Z0-9-()]+)_(.*)");
		pdfName = ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString();
		vpdfList.push_back(pdfName); 
		mh1 = processHist(fName, mh1, pdfName, "HardQCDAll");

	}//close-for: Loop over file names


	cout << __LINE__ << pdfName << endl;
	TCanvas *cRatio = new TCanvas("cRatio", "", 900, 450);
	TLegend *leg[3]; 
	leg[0] = new TLegend(0.6, 0.7, 0.8, 0.9);

	cout << __LINE__ << pdfName << endl;
	applyCosmetics(mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"], kRed, (20), 1.5);
	mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"]->Draw("P");
	leg[0]->AddEntry(mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"], pdfName.c_str(), "lep");
	leg[0]->Draw("same");
			

	//map3DLabels(mh1);

	if(0)
	{
		TCanvas *cCharge = new TCanvas("cCharge", "", 900, 450);
		TCanvas *cRatio = new TCanvas("cRatio", "", 900, 450);
		TLegend *leg[3]; 

		leg[0] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[1] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[2] = new TLegend(0.6, 0.7, 0.8, 0.9);

		cCharge->cd();
		Color_t colArr[] = {kGreen, kBlue, kRed, kBlack, kCyan};
		for (int i=0; i < vpdfList.size(); i++){

			pdfName = vpdfList.at(i);	
			applyCosmetics(mh1[pdfName]["HardQCDAll"]["Pos"], colArr[i], (20+i), 1.5);
			applyCosmetics(mh1[pdfName]["HardQCDAll"]["Neg"], colArr[i], (22+i), 1.5);

			leg[1]->AddEntry(mh1[pdfName]["HardQCDAll"]["Pos"], Form("Pos-%s", pdfName.c_str()), "lep");
			leg[2]->AddEntry(mh1[pdfName]["HardQCDAll"]["Neg"], Form("Neg-%s", pdfName.c_str()), "lep");

			mh1[pdfName]["HardQCDAll"]["Pos"]->Draw("same P");
			mh1[pdfName]["HardQCDAll"]["Neg"]->Draw("same P");
			leg[1]->Draw("same");
			leg[2]->Draw("same");

		}//close-for: Loop over pdf names 
		gPad->SetLogy();


		cRatio->cd();
		for (int i=0; i < vpdfList.size(); i++){
			pdfName = vpdfList.at(i);
			applyCosmetics(mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"], colArr[i], (20+i), 1.5);

			mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"]->Draw("same P");

			leg[0]->AddEntry(mh1[pdfName]["HardQCDAll"]["Ratio-Neg/Pos"], pdfName.c_str(), "lep");
			leg[0]->Draw("same");
		}
	}//close-if


	//file->Close();

	


}//close-main
