#!/bin/bash


root -b -q "vecInput.c+({\"a\", \"b\"}, {3.0e+3, 5e-2})"

testS=(\"a\", \"b\")
echo "String Array: ", ${testS[@]}

testV=(3e+2, 2e-1)
echo "Value Array: ", ${testV[@]}

ss=`echo "{${testS[@]}}"`
vv=`echo "{${testV[@]}}"`

echo "second procs"
root -b -q "vecInput.c+(${ss}, ${vv}, \"outfilename.root\")"

