#!/bin/bash

fileS1=(
\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-01-24-125156.root\",\
	\"Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-24-125156/CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-01-24-125156.root\")

Val1=(
2.2045,\
	0.000177283,\
	3.5902e-05,\
	0.183449,\
	8.2586e-06,\
	2.0744e-06,\
	0.047939,\
	7.5846e-07,\
	0.0069188,\
	0.001024 )

nFiles=1
