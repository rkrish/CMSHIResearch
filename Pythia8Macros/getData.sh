#!/bin/bash

message(){
	echo ; 
	echo "Get the File List and Cross-Section list from the file Cross-Section.txt"
	echo "========================================================="
	echo "Provide a PDFSetName to get the information"
	echo "$0 -p <PDFSetName> <DirPath>"
	echo "----------------------------------------------------"
	echo "Eg : $0 -p GRV94LO Output_2016-08-30-140902"
	echo "----------------------------------------------------"

	echo ; echo ; 
}

case $1 in 

	-old)
		if [[ $# == 3 ]]
		then
			dirPath=$3
			#cat ${dirPath}/Cross-Section.txt | grep "$2" | awk -v var="${dirPath}" '{printf "\n\\\"%s/%s.root\\\",\\", var, $3}' | sed '$s/,\\$/)/g' | sed '1 s/^/fileS=\(/g'
			#cat ${dirPath}/Cross-Section.txt  

			echo ; echo ; 
			#cat ${dirPath}/Cross-Section.txt | grep "$2" | awk '{printf "\n%s,\\", $1}' | sed '$s/,\\$/ )/g' | sed '1 s/^/Val=\(/g'

		echo ; 
		fi

		;;

	-p)
		if [[ $# == 3 ]]
		then
			dirPath=$3
			cat ${dirPath}/Cross-Section.txt | grep "$2" | awk -v var="${dirPath}" '{printf "\n\\\"%s/%s.root\\\",\\", var, $5}' | sed '$s/,\\$/)/g' | sed '1 s/^/fileS=\(/g' | sed 's/\/\//\//g'

			echo ; echo ; 
			cat ${dirPath}/Cross-Section.txt | grep "$2" | awk '{printf "\n%s,\\", $1}' | sed '$s/,\\$/ )/g' | sed '1 s/^/Val=\(/g'

		echo ; 
		fi
		;;
		
	*)
		message
esac
