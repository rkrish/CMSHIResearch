


func(){
./getData.sh -p GRV $1;
echo ; 
./getData.sh -p MRS $1;
echo ; 
./getData.sh -p NN $1;
echo ; 
./getData.sh -p CTEQ $1;
echo ; 
./getData.sh -p CT09MC $1;
echo ; 

nFiles=5
}

echo "Copy everything above and paste into PDFSetData.sh"
echo ; echo ;




dirList=(\
	Output_CT09MC1-LO_HardQCDMix_2016-09-23-124415\
	Output_CT09MC2-NLO_HardQCDMix_2016-09-23-123033\
	Output_CTEQ5L-LO_HardQCDMix_2016-09-23-124217\
	Output_CTEQ66.00_HardQCDMix_2016-09-23-124408\
	Output_CTEQ6L1-LO_HardQCDMix_2016-09-23-123033\
	Output_CTEQ6L-NLO_HardQCDMix_2016-09-23-124401
	Output_GRV94L-LO_HardQCDMix_2016-09-23-123033\
	Output_MRST-LO\(2008-CentralMember\)_HardQCDMix_2016-09-23-123033\
	Output_MRST-LO\(2008-CentralMember\)_HardQCDMix_2016-09-23-124352\
	Output_MRST-NLO\(2008-CentralMember\)_HardQCDMix_2016-09-23-124329\
	Output_NNPDF2.3-QCD+QED-LO14_HardQCDMix_2016-09-23-124340\
	Output_NNPDF2.3-QCD+QED-NLO_HardQCDMix_2016-09-23-124251\
	Output_NNPDF2.3-QCD+QED-NNLO_HardQCDMix_2016-09-23-123033\
	Output_NNPDF2.3-QCD+QED-NNLO_HardQCDMix_2016-09-23-123033\
)


for dir in ${dirList[@]}
do

	ls $dir/*Combined*2016-09-23* . | sed 's/^/\"/g; s/$/\"/g; s/(/\\(/g; s/)/\\)/g'
	#pdf=`sed 's/Output_\(.*\)_\(.*\)_\(.*\)/\1 /; s/(/\\(/g; s/)/\\)/p' <<< ${dir}`
	#./getData.sh -p $pdf $dir > PDFStudyData.sh 
	#echo nFiles=1 >>PDFStudyData.sh
	
done
