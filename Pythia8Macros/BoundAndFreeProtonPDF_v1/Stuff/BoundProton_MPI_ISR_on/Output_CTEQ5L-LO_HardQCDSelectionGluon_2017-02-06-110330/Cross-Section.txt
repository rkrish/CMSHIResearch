

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.48	  1.26558e-05	  1.124	  0.000816145	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-06-110330
0.0003997	  3.4922e-15	  1.9985e-05	  1.35573e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-06-110330
5.4916e-05	  6.69745e-17	  2.7458e-06	  1.87749e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-06-110330
1.46116	  4.91081e-08	  0.073058	  5.08393e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-06-110330
8.5252e-06	  1.69776e-18	  4.2626e-07	  2.98924e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-06-110330
1.443e-06	  5.26164e-20	  7.215e-08	  5.2624e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-06-110330
0.30731	  2.05518e-09	  0.0153655	  1.04003e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-06-110330
3.1818e-07	  2.93208e-21	  1.5909e-08	  1.24226e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-06-110330
0.032463	  2.28403e-11	  0.00162315	  1.09641e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-06-110330
0.0033654	  2.3701e-13	  0.00016827	  1.11688e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-06-110330
