

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.297	  1.25825e-05	  1.11485	  0.000813781	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-02-06-103716
0.00043194	  4.27723e-15	  2.1597e-05	  1.50039e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-02-06-103716
5.6897e-05	  8.30649e-17	  2.84485e-06	  2.09089e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-02-06-103716
1.53702	  5.82596e-08	  0.076851	  5.53741e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-02-06-103716
8.3454e-06	  1.75765e-18	  4.1727e-07	  3.04151e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-02-06-103716
1.32271e-06	  4.73024e-20	  6.61355e-08	  4.98959e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-02-06-103716
0.33262	  2.50186e-09	  0.016631	  1.14751e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-02-06-103716
2.6776e-07	  2.14597e-21	  1.3388e-08	  1.06276e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-02-06-103716
0.035904	  2.75022e-11	  0.0017952	  1.20311e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-02-06-103716
0.0037229	  2.93135e-13	  0.000186145	  1.2421e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-02-06-103716
