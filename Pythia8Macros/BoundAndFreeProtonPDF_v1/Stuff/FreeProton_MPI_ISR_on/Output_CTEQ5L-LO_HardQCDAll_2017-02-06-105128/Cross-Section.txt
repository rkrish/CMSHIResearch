

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
37.443	  3.73547e-05	  1.87215	  0.00140215	  CTEQ5L-LO_HardQCDAll_10To20_2017-02-06-105128
0.00128074	  3.75732e-14	  6.4037e-05	  4.44695e-08	  CTEQ5L-LO_HardQCDAll_120To170_2017-02-06-105128
0.00020477	  9.31163e-16	  1.02385e-05	  7.00061e-09	  CTEQ5L-LO_HardQCDAll_170To230_2017-02-06-105128
2.7773	  2.07164e-07	  0.138865	  0.000104419	  CTEQ5L-LO_HardQCDAll_20To30_2017-02-06-105128
3.7161e-05	  3.04477e-17	  1.85805e-06	  1.2659e-09	  CTEQ5L-LO_HardQCDAll_230To300_2017-02-06-105128
7.4059e-06	  1.18741e-18	  3.70295e-07	  2.4999e-10	  CTEQ5L-LO_HardQCDAll_300To380_2017-02-06-105128
0.64557	  1.08498e-08	  0.0322785	  2.38965e-05	  CTEQ5L-LO_HardQCDAll_30To50_2017-02-06-105128
2.047e-06	  1.04426e-19	  1.0235e-07	  7.41357e-11	  CTEQ5L-LO_HardQCDAll_380To10000_2017-02-06-105128
0.077883	  1.44373e-10	  0.00389415	  2.75655e-06	  CTEQ5L-LO_HardQCDAll_50To80_2017-02-06-105128
0.0093149	  2.01539e-12	  0.000465745	  3.25689e-07	  CTEQ5L-LO_HardQCDAll_80To120_2017-02-06-105128
