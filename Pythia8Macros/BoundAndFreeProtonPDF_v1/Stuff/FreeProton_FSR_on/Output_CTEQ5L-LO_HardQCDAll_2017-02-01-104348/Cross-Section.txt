

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
38.816	  3.98552e-05	  1.9408	  0.00144832	  CTEQ5L-LO_HardQCDAll_10To20_2017-02-01-104348
0.00120729	  3.3125e-14	  6.03645e-05	  4.17543e-08	  CTEQ5L-LO_HardQCDAll_120To170_2017-02-01-104348
0.000197623	  8.29228e-16	  9.88115e-06	  6.60633e-09	  CTEQ5L-LO_HardQCDAll_170To230_2017-02-01-104348
2.7268	  1.94339e-07	  0.13634	  0.000101135	  CTEQ5L-LO_HardQCDAll_20To30_2017-02-01-104348
3.7084e-05	  2.94492e-17	  1.8542e-06	  1.24497e-09	  CTEQ5L-LO_HardQCDAll_230To300_2017-02-01-104348
7.697e-06	  1.23977e-18	  3.8485e-07	  2.55443e-10	  CTEQ5L-LO_HardQCDAll_300To380_2017-02-01-104348
0.617	  9.85023e-09	  0.03085	  2.27691e-05	  CTEQ5L-LO_HardQCDAll_30To50_2017-02-01-104348
2.2454e-06	  1.24842e-19	  1.1227e-07	  8.10595e-11	  CTEQ5L-LO_HardQCDAll_380To10000_2017-02-01-104348
0.072892	  1.26082e-10	  0.0036446	  2.57603e-06	  CTEQ5L-LO_HardQCDAll_50To80_2017-02-01-104348
0.0086768	  1.7408e-12	  0.00043384	  3.02689e-07	  CTEQ5L-LO_HardQCDAll_80To120_2017-02-01-104348
