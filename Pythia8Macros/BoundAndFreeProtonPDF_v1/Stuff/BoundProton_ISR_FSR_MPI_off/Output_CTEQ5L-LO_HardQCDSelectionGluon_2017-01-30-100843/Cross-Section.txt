

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
22.294	  1.25787e-05	  1.1147	  0.000813657	  CTEQ5L-LO_HardQCDSelectionGluon_10To20_2017-01-30-100843
0.00043212	  4.27957e-15	  2.1606e-05	  1.5008e-08	  CTEQ5L-LO_HardQCDSelectionGluon_120To170_2017-01-30-100843
5.6893e-05	  8.255e-17	  2.84465e-06	  2.0844e-09	  CTEQ5L-LO_HardQCDSelectionGluon_170To230_2017-01-30-100843
1.53652	  5.8238e-08	  0.076826	  5.53639e-05	  CTEQ5L-LO_HardQCDSelectionGluon_20To30_2017-01-30-100843
8.3439e-06	  1.757e-18	  4.17195e-07	  3.04095e-10	  CTEQ5L-LO_HardQCDSelectionGluon_230To300_2017-01-30-100843
1.32281e-06	  4.73112e-20	  6.61405e-08	  4.99005e-11	  CTEQ5L-LO_HardQCDSelectionGluon_300To380_2017-01-30-100843
0.33259	  2.50209e-09	  0.0166295	  1.14756e-05	  CTEQ5L-LO_HardQCDSelectionGluon_30To50_2017-01-30-100843
2.6778e-07	  2.14784e-21	  1.3389e-08	  1.06322e-11	  CTEQ5L-LO_HardQCDSelectionGluon_380To10000_2017-01-30-100843
0.03589	  2.74717e-11	  0.0017945	  1.20245e-06	  CTEQ5L-LO_HardQCDSelectionGluon_50To80_2017-01-30-100843
0.0037232	  2.93401e-13	  0.00018616	  1.24267e-07	  CTEQ5L-LO_HardQCDSelectionGluon_80To120_2017-01-30-100843
