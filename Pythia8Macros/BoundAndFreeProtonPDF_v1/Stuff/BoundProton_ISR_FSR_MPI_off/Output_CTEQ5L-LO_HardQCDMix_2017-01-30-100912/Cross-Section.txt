

Script to fetch cross-section info from Log files /store/user/rjanjam/Folder/*.log
==================================================================================
Sum(cross-section/pTHat), Sqrt(Sum(error/pTHat*error/pTHat)), Avg(cross-section/pTHat), Avg(Error/pTHat), filename 
---------------------------------------------------------------------------
12.9245	  4.78575e-06	  0.646225	  0.000501878	  CTEQ5L-LO_HardQCDMix_10To20_2017-01-30-100912
0.00065842	  9.82708e-15	  3.2921e-05	  2.27424e-08	  CTEQ5L-LO_HardQCDMix_120To170_2017-01-30-100912
0.000109921	  2.65066e-16	  5.49605e-06	  3.73508e-09	  CTEQ5L-LO_HardQCDMix_170To230_2017-01-30-100912
1.04737	  3.26764e-08	  0.0523685	  4.14706e-05	  CTEQ5L-LO_HardQCDMix_20To30_2017-01-30-100912
2.0303e-05	  8.91915e-18	  1.01515e-06	  6.85149e-10	  CTEQ5L-LO_HardQCDMix_230To300_2017-01-30-100912
4.0103e-06	  3.37922e-19	  2.00515e-07	  1.33362e-10	  CTEQ5L-LO_HardQCDMix_300To380_2017-01-30-100912
0.26113	  2.04162e-09	  0.0130565	  1.0366e-05	  CTEQ5L-LO_HardQCDMix_30To50_2017-01-30-100912
1.05232e-06	  2.78758e-20	  5.2616e-08	  3.83034e-11	  CTEQ5L-LO_HardQCDMix_380To10000_2017-01-30-100912
0.034443	  3.02826e-11	  0.00172215	  1.26247e-06	  CTEQ5L-LO_HardQCDMix_50To80_2017-01-30-100912
0.0044796	  4.73305e-13	  0.00022398	  1.57831e-07	  CTEQ5L-LO_HardQCDMix_80To120_2017-01-30-100912
