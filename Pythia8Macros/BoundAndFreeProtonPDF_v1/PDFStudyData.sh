#!/bin/bash
fileS1=(
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_10To20_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_120To170_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_170To230_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_20To30_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_230To300_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_300To380_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_30To50_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_380To10000_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_50To80_2017-01-31-105825.root\",\
\"FreeProton_ISR_on/Output_CTEQ5L-LO_HardQCDSelectionQuark_2017-01-31-105825/CTEQ5L-LO_HardQCDSelectionQuark_80To120_2017-01-31-105825.root\")

Val1=(
2.2044,\
0.000177215,\
3.5903e-05,\
0.183479,\
8.2586e-06,\
2.0745e-06,\
0.047947,\
7.5839e-07,\
0.0069214,\
0.00102394 )

nFiles=1

