#!/bin/bash


# Two Switches (MPI, ISR, FSR) at a time
BoundProton_FSR_ISR_on=(
\"ProcessedOutput_pdfs-1-BoundProton_FSR_ISR_on_HardQCDAll_Combined_2017-02-03-125516.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_ISR_on_HardQCDMix_Combined_2017-02-03-125507.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_ISR_on_HardQCDSelectionGluon_Combined_2017-02-03-125534.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_ISR_on_HardQCDSelectionQuark_Combined_2017-02-03-125526.root\", \
)

FreeProton_FSR_ISR_on=(
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_ISR_on_HardQCDAll_Combined_2017-02-06-103655.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_ISR_on_HardQCDMix_Combined_2017-02-06-103642.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_ISR_on_HardQCDSelectionGluon_Combined_2017-02-06-103716.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_ISR_on_HardQCDSelectionQuark_Combined_2017-02-06-103706.root\", \
	)

BoundProton_MPI_FSR_on=(
\"ProcessedOutput_pdfs-1-BoundProton_MPI_FSR_on_HardQCDAll_Combined_2017-02-06-162217.root\", \
\"ProcessedOutput_pdfs-1-BoundProton_MPI_FSR_on_HardQCDMix_Combined_2017-02-06-162207.root\", \
\"ProcessedOutput_pdfs-1-BoundProton_MPI_FSR_on_HardQCDSelectionGluon_Combined_2017-02-06-162252.root\", \
\"ProcessedOutput_pdfs-1-BoundProton_MPI_FSR_on_HardQCDSelectionQuark_Combined_2017-02-06-162245.root\" \
)

FreeProton_MPI_FSR_on=(
\"ProcessedOutput_pdfs-1-FreeProton_MPI_FSR_on_HardQCDAll_Combined_2017-02-06-163104.root\", \
\"ProcessedOutput_pdfs-1-FreeProton_MPI_FSR_on_HardQCDMix_Combined_2017-02-06-163053.root\", \
\"ProcessedOutput_pdfs-1-FreeProton_MPI_FSR_on_HardQCDSelectionGluon_Combined_2017-02-06-163126.root\", \
\"ProcessedOutput_pdfs-1-FreeProton_MPI_FSR_on_HardQCDSelectionQuark_Combined_2017-02-06-163116.root\", \
)

BoundProton_MPI_ISR_on=(
\"ProcessedOutput_pdfs-1-BoundProton_MPI_ISR_on_HardQCDAll_Combined_2017-02-06-110214.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_MPI_ISR_on_HardQCDMix_Combined_2017-02-06-110157.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_MPI_ISR_on_HardQCDSelectionGluon_Combined_2017-02-06-110330.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_MPI_ISR_on_HardQCDSelectionQuark_Combined_2017-02-06-110319.root\", \
)

FreeProton_MPI_ISR_on=(
	\"ProcessedOutput_pdfs-1-FreeProton_MPI_ISR_on_HardQCDAll_Combined_2017-02-06-105128.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_MPI_ISR_on_HardQCDMix_Combined_2017-02-06-105029.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_MPI_ISR_on_HardQCDSelectionGluon_Combined_2017-02-06-105408.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_MPI_ISR_on_HardQCDSelectionQuark_Combined_2017-02-06-105319.root\", \
	)

# All three switches (MPI, ISR, FSR) at a time
BoundProton_ISR_FSR_MPI_off=(
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_FSR_MPI_off_HardQCDAll_Combined_2017-01-30-100813.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_FSR_MPI_off_HardQCDMix_Combined_2017-01-30-100912.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_FSR_MPI_off_HardQCDSelectionGluon_Combined_2017-01-30-100843.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_FSR_MPI_off_HardQCDSelectionQuark_Combined_2017-01-30-100828.root\", \
)

FreeProton_ISR_FSR_MPI_off=(
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_FSR_MPI_off_HardQCDAll_Combined_2017-01-30-100413.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_FSR_MPI_off_HardQCDMix_Combined_2017-01-30-100511.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_FSR_MPI_off_HardQCDSelectionGluon_Combined_2017-01-30-100453.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_FSR_MPI_off_HardQCDSelectionQuark_Combined_2017-01-30-100437.root\"
	)

# One Switch (MPI, ISR, FSR) at a time
BoundProton_MPI_on=(
	\"ProcessedOutput_pdfs-1-BoundProton_MPI_on_HardQCDAll_Combined_2017-02-01-140958.root\", \
		\"ProcessedOutput_pdfs-1-BoundProton_MPI_on_HardQCDMix_Combined_2017-02-01-141056.root\", \
		\"ProcessedOutput_pdfs-1-BoundProton_MPI_on_HardQCDSelectionGluon_Combined_2017-02-01-141035.root\", \
		\"ProcessedOutput_pdfs-1-BoundProton_MPI_on_HardQCDSelectionQuark_Combined_2017-02-01-141017.root\", \
)

FreeProton_MPI_on=(
		\"ProcessedOutput_pdfs-1-FreeProton_MPI_on_HardQCDAll_Combined_2017-02-01-140504.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_MPI_on_HardQCDMix_Combined_2017-02-01-140600.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_MPI_on_HardQCDSelectionGluon_Combined_2017-02-01-140536.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_MPI_on_HardQCDSelectionQuark_Combined_2017-02-01-140520.root\", \

) 

BoundProton_ISR_on=(
		\"ProcessedOutput_pdfs-1-BoundProton_ISR_on_HardQCDAll_Combined_2017-01-30-135045.root\", \
			\"ProcessedOutput_pdfs-1-BoundProton_ISR_on_HardQCDMix_Combined_2017-01-30-135203.root\", \
			\"ProcessedOutput_pdfs-1-BoundProton_ISR_on_HardQCDSelectionGluon_Combined_2017-01-30-135134.root\", \
			\"ProcessedOutput_pdfs-1-BoundProton_ISR_on_HardQCDSelectionQuark_Combined_2017-01-30-135111.root\", \
		)

FreeProton_ISR_on=(
		\"ProcessedOutput_pdfs-1-FreeProton_ISR_on_HardQCDAll_Combined_2017-01-31-105653.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_ISR_on_HardQCDMix_Combined_2017-01-31-105912.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_ISR_on_HardQCDSelectionGluon_Combined_2017-01-31-105846.root\", \
		\"ProcessedOutput_pdfs-1-FreeProton_ISR_on_HardQCDSelectionQuark_Combined_2017-01-31-105825.root\", \
	)

BoundProton_FSR_on=(
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_on_HardQCDAll_Combined_2017-02-21-112036.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_on_HardQCDMix_Combined_2017-02-21-112023.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_on_HardQCDSelectionGluon_Combined_2017-02-21-112057.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_FSR_on_HardQCDSelectionQuark_Combined_2017-02-21-112046.root\", \
)


FreeProton_FSR_on=(
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_on_HardQCDAll_Combined_2017-02-21-112408.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_on_HardQCDMix_Combined_2017-02-21-112359.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_on_HardQCDSelectionGluon_Combined_2017-02-21-112430.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_FSR_on_HardQCDSelectionQuark_Combined_2017-02-21-112420.root\", \
)


BoundAndFreeProton_ISR_MPI_FSR_on=(
	\"ProcessedOutput_pdfs-1-BoundAndFreeProton_ISR_MPI_FSR_on_HardQCDAll_Combined_2017-02-27-101913.root\", \
	\"ProcessedOutput_pdfs-1-BoundAndFreeProton_ISR_MPI_FSR_on_HardQCDMix_Combined_2017-02-27-101904.root\", \
	\"ProcessedOutput_pdfs-1-BoundAndFreeProton_ISR_MPI_FSR_on_HardQCDSelectionGluon_Combined_2017-02-27-101931.root\" \
	\"ProcessedOutput_pdfs-1-BoundAndFreeProton_ISR_MPI_FSR_on_HardQCDSelectionQuark_Combined_2017-02-27-101923.root\" \
)


BoundProton_ISR_MPI_FSR_on=(
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_MPI_FSR_on_HardQCDAll_Combined_2017-03-01-122227.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_MPI_FSR_on_HardQCDMix_Combined_2017-03-01-122218.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_MPI_FSR_on_HardQCDSelectionGluon_Combined_2017-03-01-122244.root\", \
	\"ProcessedOutput_pdfs-1-BoundProton_ISR_MPI_FSR_on_HardQCDSelectionQuark_Combined_2017-03-01-122236.root\", \
)

FreeProton_ISR_MPI_FSR_on=(
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_MPI_FSR_on_HardQCDAll_Combined_2017-03-01-121023.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_MPI_FSR_on_HardQCDMix_Combined_2017-03-01-121013.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_MPI_FSR_on_HardQCDSelectionGluon_Combined_2017-03-01-121042.root\", \
	\"ProcessedOutput_pdfs-1-FreeProton_ISR_MPI_FSR_on_HardQCDSelectionQuark_Combined_2017-03-01-121033.root\", \
)



# Works
#root -l "AllSwitchesOnSameCanvas_v2.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v2.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v2.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Ratio).*\")"
#root -l "AllSwitchesOnSameCanvas_v3.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Ratio).*\")"

# All 4 process switches per charge type
#root -l "AllSwitchesOnSameCanvas_v4.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Ratio).*\")"

#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Ratio).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_FSR_on[*]}}, {${BoundProton_MPI_FSR_on[*]}}, \"^(Neg).*\")"



#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_ISR_FSR_MPI_off[*]}}, {${BoundProton_ISR_FSR_MPI_off[*]}}, \"^(Pos).*\")"


# Interesting 
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_on[*]}}, {${BoundProton_MPI_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_on[*]}}, {${BoundProton_MPI_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_on[*]}}, {${BoundProton_MPI_on[*]}}, \"^(Ratio).*\")"


# Interesting 
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_ISR_on[*]}}, {${BoundProton_ISR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_ISR_on[*]}}, {${BoundProton_ISR_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_ISR_on[*]}}, {${BoundProton_ISR_on[*]}}, \"^(Ratio).*\")"


# Interesting 
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_on[*]}}, {${BoundProton_FSR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_on[*]}}, {${BoundProton_FSR_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_on[*]}}, {${BoundProton_FSR_on[*]}}, \"^(Ratio).*\")"

# Interesting 
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_ISR_on[*]}}, {${BoundProton_FSR_ISR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_ISR_on[*]}}, {${BoundProton_FSR_ISR_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_ISR_on[*]}}, {${BoundProton_FSR_ISR_on[*]}}, \"^(Ratio).*\")"


# Interesting 
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_MPI_ISR_on[*]}}, {${BoundProton_MPI_ISR_on[*]}}, \"^(Pos).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_ISR_on[*]}}, {${BoundProton_FSR_ISR_on[*]}}, \"^(Neg).*\")"
#root -l "AllSwitchesOnSameCanvas_v6.C+({${FreeProton_FSR_ISR_on[*]}}, {${BoundProton_FSR_ISR_on[*]}}, \"^(Ratio).*\")"

#root -l "AllSwitchesOnSameCanvas_v8.C+({${FreeProton_MPI_ISR_on[*]}}, {${BoundProton_MPI_ISR_on[*]}})"
#root -l "AllSwitchesOnSameCanvas_v8.C+({${FreeProton_MPI_on[*]}}, {${BoundProton_MPI_on[*]}})"
root -l "AllSwitchesOnSameCanvas_v8.C+({${FreeProton_ISR_on[*]}}, {${BoundProton_ISR_on[*]}})"

#root -l "AllSwitchesOnSameCanvas_v8.C+({${FreeProton_ISR_MPI_FSR_on[*]}}, {${BoundProton_ISR_MPI_FSR_on[*]}})"

