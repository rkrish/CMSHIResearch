#pragma once

// C++ Headers
#include <iostream>
#include <vector>
#include <map>

// ROOT Headers
#include <TGraph.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TPRegexp.h>
#include "TClonesArray.h"
#include <TObjString.h>
#include <TString.h>
#include <TMultiGraph.h>
#include <TGraph.h>

using namespace std;

typedef const char* cchar;
typedef map<string, map<string, TH1D*>> map2Dh1;
typedef map<string, map2Dh1> map3Dh1;
 

/*
void histToGraph(TH1D *h, TGraph *g){

	int NBINS = h->GetSize();
	cout << __LINE__ << NBINS << endl;
}
*/

/* Normalizing Variable Binned Histograms */
TH1D* normalizeVarBinHist (TH1D *hVarPt, double nVarBins)
{

	double lbinWidth, lbinContent, lbinError;	

	for (int tBin=1; tBin < nVarBins; tBin++){

		// Positively Charged Particles
		lbinWidth = hVarPt->GetBinWidth(tBin);
		lbinContent = hVarPt->GetBinContent(tBin);
		lbinError = hVarPt->GetBinError(tBin);
		hVarPt->SetBinContent(tBin, lbinContent/lbinWidth);
		hVarPt->SetBinError(tBin, lbinError/lbinWidth);

		cout << "hVar (binContent, binError) : "  << "\t"
		     << hVarPt->GetBinContent(tBin) << "\t"
		     << hVarPt->GetBinError(tBin) << "\t" << "\n";

	}

	cout << "\n\n";
	cout << "============= normalizeVarBinHist : Executed successfully =============" << endl;
	cout << "Number of bins in the rebinned histogram: " << hVarPt->GetSize() << endl;
	cout << "File used for normalizing: " << hVarPt->GetName() << endl;
	cout << "=======================================================================" << endl;
	cout << "\n\n";
	return hVarPt;

}//close-normalizeVarBinHist

TGraph* histToGraph(TH1D *h, TGraph *g)
{

	int NBINS = h->GetSize();
	double nx[NBINS], ny[NBINS];
	for (int tBin=0; tBin < NBINS ; tBin++){ 

		nx[tBin] = tBin;
		ny[tBin] = h->GetBinContent(tBin);

	}//close-for

	//g = new TGraph(NBINS, nx, h->GetArray() );
	g = new TGraph( NBINS, nx, ny );
	g->SetMarkerSize(h->GetMarkerSize());
	g->SetMarkerStyle(h->GetMarkerStyle());
	g->SetMarkerColor(h->GetMarkerColor());

	return g;

}//close-histToGraph

/* Apply Cosmetics to Histograms */
TH1D* applyCosmetics(TH1D* h, Color_t mCol, int mStyle, float mSize )
{

	h->SetLineColor(mCol);
	h->SetMarkerStyle(mStyle);
	h->SetMarkerColor(mCol);
	h->SetMarkerSize(mSize);
	h->SetStats(0);

	return h;

}//close-applyCosmetics


/* Show all the initialized map3D Labels */
void map3DLabels(map3Dh1 hTrack)
{

	for(map3Dh1::iterator it = hTrack.begin(); it != hTrack.end(); ++it){
		for(map2Dh1::iterator it1 = (&(it->second))->begin(); it1 != (&(it->second))->end(); ++it1){
			for (map<string, TH1D*>::iterator it2 = (&(it1->second))->begin(); it2 != (&(it1->second))->end(); ++it2){
				cout << "[\"" << it->first << "\"]" ;
				cout << "[\"" << it1->first << "\"]" ;
				cout << "[\"" << it2->first<<"\"]";
				cout << endl;
				}
			}
	}

}//close-map3DLabels

void fileAvailability(cchar fileName)
{

	TFile *file = new TFile(fileName, "r");;
	if (!file->IsOpen()){
		cout << "\nThis file : " << file->GetName() << endl;
		cout << "is not available...." << endl;
		cout << "Exiting .... " << endl;
		exit(EXIT_FAILURE);
	}

	else {

	cout << "Is the file open? (1: yes, 0:no)\t" 
	     << file->GetName() << "\t"
	     << file->IsOpen() << endl;
	}
}

map3Dh1 processHist(cchar fileName, map3Dh1 mh1, string pdfName, string procSw)
{

	TFile *file = new TFile(fileName, "r");
	
	//fileAvailability(file) ;

	cchar sPathPos, sPathAll;
	sPathPos = "QCDAna/pchspectrum";
	sPathAll = "QCDAna/chspectrum";


	TH1D *hPos, *hNeg, *hRatio, *hAll;
	hPos = (TH1D*)file->Get(sPathPos)->Clone("hPos");
	hAll = (TH1D*)file->Get(sPathAll)->Clone("hAll");

	hNeg = (TH1D*)hPos->Clone("hNeg");
	hNeg->Add(hAll, hPos, 1, -1);
	hRatio = (TH1D*)hNeg->Clone("hRatio");

	hRatio->Divide(hPos);

	mh1[pdfName][procSw]["Pos"] = (TH1D*)hPos;
	mh1[pdfName][procSw]["Neg"] = (TH1D*)hNeg;
	mh1[pdfName][procSw]["Ratio-NegByPos"] = (TH1D*)hRatio;

	//file->Close();

	return mh1;

}//close-processHist

void InfoAboutMacro()
{

	cout << "\n\n";
	cout << "This macro is used to obtain the Charge Asymmetry for different PDF Sets" << endl;
	cout << "\n\n";

}//close-InfoAboutMacro

void Pythia8SpectraStandalone()
{

	InfoAboutMacro();
	char *userInput;

	/*
	cin >> "Type y: if you would like to process, else n: "
	    >> userInput;

	if ( strncmp(userInput, 'n') ) {
		cout << "You opted to not proceed " << endl; exit(EXIT_FAILURE);
	}
	*/

	vector<cchar> vfileName;
	vfileName = { 

		/*
		"CT09MC2-NLO_HardQCDAll_Combined_2016-09-10-123538.root",
		"CTEQ6L1-LO_HardQCDAll_Combined_2016-09-10-123538.root",
		"GRV94L-LO_HardQCDAll_Combined_2016-09-10-123538.root",
		"NNPDF2.3-QCD+QED-NNLO_HardQCDAll_Combined_2016-09-10-123538.root",
		"CT09MC1-LO_HardQCDAll_Combined_2016-09-16-150557.root",
		"CTEQ5L-LO_HardQCDAll_Combined_2016-09-16-150435.root",
		"CTEQ66.00_HardQCDAll_Combined_2016-09-16-150549.root",
		"CTEQ6L-NLO_HardQCDAll_Combined_2016-09-16-150527.root",
		"MRST-LO(2008-CentralMember)_HardQCDAll_Combined_2016-09-16-150518.root",
		"MRST-NLO(2008-CentralMember)_HardQCDAll_Combined_2016-09-16-150456.root",
		"NNPDF2.3-QCD+QED-LO14_HardQCDAll_Combined_2016-09-16-150508.root",
		"NNPDF2.3-QCD+QED-NLO_HardQCDAll_Combined_2016-09-16-150446.root",
		*/

		/*
		"CT09MC2-NLO_HardQCDSelectionQuark_Combined_2016-09-12-075049.root",
		"CTEQ6L1-LO_HardQCDSelectionQuark_Combined_2016-09-12-075049.root",
		"GRV94L-LO_HardQCDSelectionQuark_Combined_2016-09-12-075049.root",
		"NNPDF2.3-QCD+QED-NNLO_HardQCDSelectionQuark_Combined_2016-09-12-075049.root",
		"CT09MC1-LO_HardQCDSelectionQuark_Combined_2016-09-16-150939.root",
		"CTEQ5L-LO_HardQCDSelectionQuark_Combined_2016-09-16-150845.root",
		"CTEQ66.00_HardQCDSelectionQuark_Combined_2016-09-16-150932.root",
		"CTEQ6L-NLO_HardQCDSelectionQuark_Combined_2016-09-16-150924.root",
		"MRST-LO(2008-CentralMember)_HardQCDSelectionQuark_Combined_2016-09-16-150918.root",
		"MRST-NLO(2008-CentralMember)_HardQCDSelectionQuark_Combined_2016-09-16-150903.root",
		"NNPDF2.3-QCD+QED-LO14_HardQCDSelectionQuark_Combined_2016-09-16-150946.root",
		"NNPDF2.3-QCD+QED-NLO_HardQCDSelectionQuark_Combined_2016-09-16-150854.root",
		*/

		/*
		"CT09MC2-NLO_HardQCDSelectionGluon_Combined_2016-09-13-193831.root",
		"CTEQ6L1-LO_HardQCDSelectionGluon_Combined_2016-09-13-193831.root",
		"GRV94L-LO_HardQCDSelectionGluon_Combined_2016-09-13-193831.root",
		"NNPDF2.3-QCD+QED-NNLO_HardQCDSelectionGluon_Combined_2016-09-13-193831.root",
		"CT09MC1-LO_HardQCDSelectionGluon_Combined_2016-09-16-150810.root",
		"CTEQ5L-LO_HardQCDSelectionGluon_Combined_2016-09-16-150659.root",
		"CTEQ66.00_HardQCDSelectionGluon_Combined_2016-09-16-150752.root",
		"CTEQ6L-NLO_HardQCDSelectionGluon_Combined_2016-09-16-150741.root",
		"MRST-LO(2008-CentralMember)_HardQCDSelectionGluon_Combined_2016-09-16-150733.root",
		"MRST-NLO(2008-CentralMember)_HardQCDSelectionGluon_Combined_2016-09-16-150717.root",
		"NNPDF2.3-QCD+QED-LO14_HardQCDSelectionGluon_Combined_2016-09-16-150818.root",
		"NNPDF2.3-QCD+QED-NLO_HardQCDSelectionGluon_Combined_2016-09-16-150708.root",
		*/

		/*
		"CT09MC1-LO_HardQCDMix_Combined_2016-09-23-124415.root",
		"CT09MC2-NLO_HardQCDMix_Combined_2016-09-23-123033.root",
		"CTEQ5L-LO_HardQCDMix_Combined_2016-09-23-124217.root",
		"CTEQ66.00_HardQCDMix_Combined_2016-09-23-124408.root",
		"CTEQ6L1-LO_HardQCDMix_Combined_2016-09-23-123033.root",
		"CTEQ6L-NLO_HardQCDMix_Combined_2016-09-23-124401.root",
		"GRV94L-LO_HardQCDMix_Combined_2016-09-23-123033.root",
		"NNPDF2.3-QCD+QED-LO14_HardQCDMix_Combined_2016-09-23-124340.root",
		"NNPDF2.3-QCD+QED-NLO_HardQCDMix_Combined_2016-09-23-124251.root",
		"NNPDF2.3-QCD+QED-NNLO_HardQCDMix_Combined_2016-09-23-123033.root",
		"MRST-LO(2008-CentralMember)_HardQCDMix_Combined_2016-09-23-124352.root",
		"MRST-NLO(2008-CentralMember)_HardQCDMix_Combined_2016-09-23-124329.root",
		*/

		
		//"CTEQ5L-LO_HardQCDAll_Combined_2017-01-22-111524.root",
		//"CTEQ5L-LO_HardQCDMix_Combined_2017-01-22-111711.root",
		//"CTEQ5L-LO_HardQCDSelectionGluon_Combined_2017-01-22-111655.root",
		//"CTEQ5L-LO_HardQCDSelectionQuark_Combined_2017-01-22-111633.root"

		//"CTEQ5L-LO_HardQCDAll_Combined_2017-01-24-125135.root",
		//"CTEQ5L-LO_HardQCDMix_Combined_2017-01-24-125234.root",

		//"CTEQ5L-LO_HardQCDSelectionGluon_Combined_2017-01-24-125220.root",
		"CTEQ5L-LO_HardQCDSelectionQuark_Combined_2017-01-24-125156.root"



	};



	double varBinArr[] = { 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45,
	        0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95,
	        1.1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 3.2, 4.0, 4.8, 5.6, 6.4,
	        7.2, 9.6, 12.0, 14.4, 19.2, 24.0, 28.8, 35.2, 41.6, 48.0, 60.8, 73.6, 86.4, 103.6,
	        130.0, 170, 200 };

	// Get the number of variables bins declared
	const int nVarBins = sizeof(varBinArr)/sizeof(double)-1;
	cout << "Number of Variable Bins declared: " << nVarBins << endl;


	for(auto file : vfileName) fileAvailability(file);

	vector<string> vpdfList;
	string pdfName, procSw;

	TString s0(vfileName[0]);
	TPRegexp r1("_([0-9-]+).root");
	string creationDate;
	creationDate = ((TObjString*)(r1.MatchS(s0)->At(1)))->GetString();

	TPRegexp r2("_([a-zA-Z]+)_(.*)");
	procSw = ((TObjString*)(r2.MatchS(s0)->At(1)))->GetString();

	map3Dh1 mh1;
	for (auto fName : vfileName)
	{

		TString s0(fName);
		TPRegexp r0("([a-zA-Z0-9-()+.]+)_(.*)");
		pdfName = ((TObjString*)(r0.MatchS(s0)->At(1)))->GetString();

		vpdfList.push_back(pdfName); 
		mh1 = processHist( fName, mh1, pdfName, procSw );

	}//close-for: Loop over file names



	//map3DLabels(mh1);

	cout << "Line " <<  __LINE__ << endl;
	TMultiGraph *multiG = new TMultiGraph();
	map<string, TGraph*> mg;

	if(1)
	{
		TCanvas *cCharge = new TCanvas("cCharge", "", 900, 450);
		TCanvas *cRatio = new TCanvas("cRatio", "", 900, 450);

		TLegend *leg[3]; 
		leg[0] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[1] = new TLegend(0.6, 0.7, 0.8, 0.9);
		leg[2] = new TLegend(0.6, 0.7, 0.8, 0.9);

		cCharge->cd();
		Color_t colArr[] = {kGreen, kBlue, kRed, kBlack, kCyan, kGreen-5, kRed-6, kGray, kBlue+3, 
				    kGreen-4, kOrange, kSpring+4, kCyan+2};
		for (int i=0; i < vpdfList.size(); i++){

			pdfName = vpdfList.at(i);	
			applyCosmetics(mh1[pdfName][procSw]["Pos"], colArr[i], (20+i), 1.5);
			applyCosmetics(mh1[pdfName][procSw]["Neg"], colArr[i], (22+i), 1.5);

			leg[1]->AddEntry(mh1[pdfName][procSw]["Pos"], Form("Pos-%s", pdfName.c_str()), "lep");
			leg[2]->AddEntry(mh1[pdfName][procSw]["Neg"], Form("Neg-%s", pdfName.c_str()), "lep");

			mh1[pdfName][procSw]["Pos-VarBin"] = (TH1D*)mh1[pdfName][procSw]["Pos"]->Rebin(nVarBins, Form("%s%s-Pos-VarBin", pdfName.c_str(), procSw.c_str()), varBinArr);
			mh1[pdfName][procSw]["Neg-VarBin"] = (TH1D*)mh1[pdfName][procSw]["Neg"]->Rebin(nVarBins, Form("%s%s-Neg-VarBin", pdfName.c_str(), procSw.c_str()), varBinArr);

			mh1[pdfName][procSw]["Pos-VarBin"]= normalizeVarBinHist(mh1[pdfName][procSw]["Pos-VarBin"], nVarBins);	
			mh1[pdfName][procSw]["Neg-VarBin"]= normalizeVarBinHist(mh1[pdfName][procSw]["Neg-VarBin"], nVarBins);	

			//mh1[pdfName][procSw]["Pos"]->Draw("same CP");
			//mh1[pdfName][procSw]["Neg"]->Draw("same CP");
			
			mh1[pdfName][procSw]["Pos-VarBin"]->Draw("same CP");
			mh1[pdfName][procSw]["Neg-VarBin"]->Draw("same CP");

			leg[1]->Draw("same");
			leg[2]->Draw("same");

		}//close-for: Loop over pdf names 
		gPad->SetLogy();


		cRatio->cd();
			for (int i=0; i < vpdfList.size(); i++){

				pdfName = vpdfList.at(i);
				applyCosmetics(mh1[pdfName][procSw]["Ratio-NegByPos"], colArr[i], (20), 1.5);

				cout << "Standard Deviation for : " 
				     << pdfName << "\t"
				     << mh1[pdfName][procSw]["Ratio-NegByPos"]->GetStdDev(1) 
				     << endl;


				mh1[pdfName][procSw]["Ratio-NegByPos"]->Rebin(5);
				mh1[pdfName][procSw]["Ratio-NegByPos"]->Draw("same P");

				//mg[pdfName] = histToGraph(mh1[pdfName][procSw]["Ratio-NegByPos"], mg[pdfName]);
				//multiG->Add(mg[pdfName]);


				leg[0]->AddEntry(mh1[pdfName][procSw]["Ratio-NegByPos"], pdfName.c_str(), "lep");
				leg[0]->Draw("same");
				gPad->SetGrid();
			}

		TCanvas *cGraph = new TCanvas("cGraph", "", 900, 450);
		cGraph->cd();
		multiG->Draw("alp");
	}//close-if

	if(0){
	pdfName = vpdfList.at(3);
		TCanvas *cRatio = new TCanvas("cRatio", "", 900, 450);
		TLegend *leg[3]; 
		leg[0] = new TLegend(0.6, 0.7, 0.8, 0.9);

			applyCosmetics(mh1[pdfName][procSw]["Ratio-NegByPos"], kBlue, (20), 1.5);

			mh1[pdfName][procSw]["Ratio-NegByPos"]->Draw("same P");

			leg[0]->AddEntry(mh1[pdfName][procSw]["Ratio-NegByPos"], pdfName.c_str(), "lep");
			leg[0]->Draw("same");

	}

	/* Writing the results to output file */
	TFile *oFile = new TFile(Form("ProcessedOutput_pdfs-%d-%s.root", vpdfList.size(), procSw.c_str()),  "recreate");
	for (auto pdfName : vpdfList){

		(mh1[pdfName][procSw]["Ratio-NegByPos"])->Write(Form("%s_%s_%s", "Ratio-NegByPos",pdfName.c_str(), procSw.c_str()));
		(mh1[pdfName][procSw]["Pos"])->Write(Form("%s_%s_%s", "Pos", pdfName.c_str(), procSw.c_str()));
		(mh1[pdfName][procSw]["Neg"])->Write(Form("%s_%s_%s", "Neg", pdfName.c_str(), procSw.c_str()));
	}
	oFile->Close();



}//close-main
