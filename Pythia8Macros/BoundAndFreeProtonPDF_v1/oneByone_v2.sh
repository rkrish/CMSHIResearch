#!/bin/bash

message(){

	echo "=============================================================="
	echo "This script is used to merge the spectra of different pT Hats \
	      and extract the positive and negative charged particle spectra \
	      and calculating the ratio of negative to positive spectra"
	echo "=============================================================="
	echo ; echo ;
}

message

mainFolder=(
	BoundProton_FSR_ISR_on \
	BoundProton_FSR_on \
	BoundProton_ISR_FSR_MPI_off \
	BoundProton_ISR_FSR_MPI_on \
	BoundProton_ISR_on \
	BoundProton_MPI_FSR_on \
	BoundProton_MPI_ISR_on \
	BoundProton_MPI_on \
	FreeProton_FSR_ISR_on \
	FreeProton_FSR_on \  
	FreeProton_ISR_FSR_MPI_off \
	FreeProton_ISR_FSR_MPI_on \
	FreeProton_ISR_on \
	FreeProton_MPI_FSR_on \
	FreeProton_MPI_ISR_on \
	FreeProton_MPI_on \
)


hide2(){
mainFolder=(
#	BoundProton_FSR_on \
	FreeProton_FSR_on \
)
}

mainFolder=( FreeProton_ISR_on )


#For Bound and Free Proton PDF Merging pt hats * process sw
pTHatMerge(){
# Loop over (free,bound)
for mf in ${mainFolder[*]}
do
	mainFolder=${mf}
	psFolders=`ls ${mf}`

	echo ${psFolders}
	# Loop over (proc sw)
	for psFile in ${psFolders}
	do
		# execute the algorithm 

		# Loop over folders
		folder=$mainFolder/$psFile

		# CTEQ5L is a placeholder
		./getData.sh -p CTEQ5L-LO ${folder} > PDFStudyData.sh


		# Merge the pT Hat histograms
		./processAllOutputs.sh ${folder}
	done
done
}



#For Bound and Free Proton PDF Merging pt hats * process sw
calcRatioAndWriteToFile(){
# Loop over (free,bound)
for mf in ${mainFolder[*]}
do
	mainFolder=${mf}
	psList=`ls ${mf}/${mf}*.root`
	for psFile in ${psList}
	do
		echo "Processing the file: " ${psFile};
		echo "------------------------------------------------"
		root -b -q "Pythia8SpectraStandalone_v3.C+(\"$psFile\")"
		echo "------------------------------------------------"
		echo ; echo ;
		
	done
done
}

#pTHatMerge
calcRatioAndWriteToFile 
