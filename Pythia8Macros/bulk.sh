#!/bin/bash

fileName=`ls Output_2016-09-16-150435/*10To20*.root | sed 's/.*\///g'`

pdfName=`echo ${fileName} | sed 's/\([a-zA-Z0-9().-]\+\)_\([a-zA-Z]\+\)_\(.*\)_.*.root/\1/p'`

#`./getData.sh -p ${pdfName} Output_2016-09-16-150435`
#./getData.sh -p CTEQ5L-LO Output_2016-09-16-150435

dirList=`ls -d Output_2016-09-16-15*`

process(){
	for dpath in ${dirList}
	do

		fileName=`ls ${dpath}/*10To20*.root | sed 's/.*\///g'`
		pdfName=`echo ${fileName} | sed 's/\([a-zA-Z0-9().-]\+\)_\([a-zA-Z]\+\)_\(.*\)_.*.root/\1/g'`
		#echo "getData.sh -p ${pdfName} ${dpath}"
		echo ${pdfName} ${dpath}
		echo "--------------------------------"
		./getData.sh "-p" ${pdfName} ${dpath} | sed 's/^fileS/fileS1/g' | sed 's/^Val/Val1/g' > PDFStudyData.sh
		echo nFiles=1 >> PDFStudyData.sh
		#cat PDFStudyData.sh
		./processAllOutputs.sh ${dpath}
		echo ;
	done
}

test(){
	for dpath in ${dirList}
	do
		cp ${dpath}/*Combined*.root .
	done
}



MRST(){
	for dpath in Output_2016-09-16-150456 Output_2016-09-16-150518 Output_2016-09-16-150717 Output_2016-09-16-150733 Output_2016-09-16-150903 Output_2016-09-16-150918
	do
		fileName=`ls ${dpath}/*10To20*.root | sed 's/.*\///g'`
		pdfName=`echo ${fileName} | sed 's/\([a-zA-Z0-9().-]\+\)_\([a-zA-Z]\+\)_\(.*\)_.*.root/\1/g'`
		#./getData.sh "-p" ${pdfName} ${dpath} | sed 's/^fileS/fileS1/g' | sed 's/^Val/Val1/g' | sed 's/O(//g'
		#echo $pdfName | sed 's/O(l/O\\(/g; s/r)/r\\)/g'
		
		./getData.sh "-p" ${pdfName} ${dpath} | sed 's/^fileS/fileS1/g' | sed 's/^Val/Val1/g' | sed 's/O(/O\\(/g; s/r)/r\\)/g' > PDFStudyData.sh
		#cat PDFStudyData.sh
		echo nFiles=1 >> PDFStudyData.sh
		./processAllOutputs.sh ${dpath}
	done
}

#test

ls *2016-09-16-15*.root | sed -n -r 's/^(.*)$/\"\1\",/p'


