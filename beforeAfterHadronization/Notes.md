### July 8, 2017

# ```oneByone.sh```
This is the main file that reads the stuff from say *FreeProton_CT14nlo_2017-06-08-155841/{.root, Cross-Section.txt, Timing.txt}* and combines them with information in the Cross-Section.txt. It requires the *getData.sh*, *makeCombinedPtHatSample.C* as well. 

The idea is, information from Cross-Section.txt is extracted and written into a file, which is used by *processAllOutputs.sh*. Beyond this step, a *Combined.root* file is generated and written into the same directory containing the pt hat files

The script can also fetch the files from ACCRE, the hadding steps happens at ACCRE, based on another script. 

Note : Since the *Combined.root* files are written into the parent directory, they should be deleted, for which a function is written in the *oneByone.sh*, read the script carefully and process it. 

# ```partonsBeforeHadronsAfter.C```
This is the script that consumes the *Combined*.root files and draws the required histograms. The information from the processed root files has the results based on HepMC class, where the partons before hadronization and hadrons after hadronization, with positive and negative charged particles separated for both the stages. To study the source of neg/pos difference, possibly at hadronization stage, because bound, free pdf sets are expected to show changes at that point. 


Note that the rest of the histograms are all double counted, like nchspectrum, pchspectrum. So, any estimate of them would be wrong.

THe collection <GenParticles> actually has partons, it is just that they are avoided, it is not clear, where is the source of this parton i.e. can we trace back a hadron from the parton, etc 
