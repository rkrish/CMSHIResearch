#!/bin/bash

###  Put a condition here for variable number ###

if [[ $# != 1 ]]
then 
	exit 1

elif [[ $# == 1 ]] 
then

### Remove the first argument ###

case $1 in
	process)

		### Change the query here to get the list of files ###
		query=*NNLO*30to50*
		echo $query

		### Add quotes to each file and remove trailing comma, removing comma at the last file name ###
		list=`ls ${query} | awk '{print "\""$1"\","}' | sed '$s/,$//g' | sed 's/\"/\\\"/g'`

		echo "After adding the quotes"
		echo -e "========================================================="
		echo ${list}

		### Replace the variable in the file ###
		perl -p -e  "s/replaceVar1/$list/g" listOfFilesTemplate.C > listOfFiles.C

		echo -e "========================================================="
		echo -e "\n\n\nCheck the file for the result : listOfFiles.C"
		;;

	*)
esac
fi
