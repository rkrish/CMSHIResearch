# November 13, 2017
-

Macros to compare the nPDF and fPDF, n:nuclear, f:free for thesis purposes

Use the following query to get the results from the Cross-Sections file 

cat ../xVSQ2/BoundAndFreeProton_EPS09_CT10nlo_2017-08-/Cross-Section.txt | grep "^[0-9]" | awk '{print $3}' | sed 's/^/\"/g' | sed 's/$/\"/g' | tr "\n" ","
