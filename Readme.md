# Code Bases

Most of the codes in this directory are written in C++, a [ROOT framework](https://root.cern.ch/) that is used to do number crunching and visualization. Apart from that there are various GUI Codes that were developed to easen the development process. For example, saving files, processing the information in the `root` format, which is created at CERN, and widely used by experimental physicists. 
